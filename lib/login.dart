import 'dart:math';
import 'package:flutter/material.dart';
import 'dart:convert';
import 'dart:io';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
// dio lib
import 'package:dio/dio.dart';
import 'package:dio/adapter.dart';

import './navTab.dart';
import './drawer.dart';

void main() {
  runApp(MaterialApp(
    theme: ThemeData(
      primarySwatch: Colors.pink,
    ),
    home: Scaffold(
        // appBar: AppBar(
        //   title: Text('Demo'),
        // ),
        body: LoginSignupPage()),
  ));
}

abstract class BaseAuth {
  Future<Response<dynamic>> login(String email, String password);
  Future<Response<dynamic>> signup(
      String email, String firstName, String lastName, String password);
}

class Auth implements BaseAuth {
  Future<Response<dynamic>> login(String email, String password) async {
    final url = "https://10.6.88.55:8084/api/sys/jwt_token/create";
    final requestBody = {"userId": email, "password": password};
// ========= dart:io:HttpClient ================
    // HttpClient httpClient = new HttpClient()
    //   ..badCertificateCallback =
    //       ((X509Certificate cert, String host, int port) => true);
    // HttpClientRequest request = await httpClient.postUrl(Uri.parse(url));

    // request.headers.set(HttpHeaders.contentTypeHeader, "application/json");
    // request.write(jsonEncode(requestBody));
    // HttpClientResponse response = await request.close();
    // String reply = await response.transform(utf8.decoder).join();
    // final body = json.decode(reply);
    // var token = body["token"];
    // print(response);

// =========   DIO lib =====================
    print("Start to run dio");
    Response response;
    final Dio dio = Dio();
    (dio.httpClientAdapter as DefaultHttpClientAdapter).onHttpClientCreate =
        (HttpClient client) {
      client.badCertificateCallback =
          (X509Certificate cert, String host, int port) => true;
      return client;
    };
    response = await dio.post(url, data: jsonEncode(requestBody));
    print("res - login api/sys/jwt_token/create");
    print(response.data);
    String token = response.data["token"];
// ========== DIO lib ================

    // Create storage
    final storage = new FlutterSecureStorage();
    // Write token into storage
    await storage.write(key: 'token', value: "Bearer " + token);
    var newToken = parseJwt(token);
    await storage.write(key: 'token_sub', value: newToken["sub"]);
    // if (newToken['sub'].length > 1) {
    //   return response;
    // } else
    //   return '';
    return response;
  }

  Future<Response<dynamic>> signup(
      String email, String firstName, String lastName, String password) async {
    final url = "https://10.6.88.55:8084/api/user/sign_up";
    final requestBody = {
      "userId": email,
      "firstName": firstName,
      "lastName": lastName,
      "password": password
    };
    // =========   DIO lib =====================
    print("Start to run dio");
    Response response;
    final Dio dio = Dio();
    (dio.httpClientAdapter as DefaultHttpClientAdapter).onHttpClientCreate =
        (HttpClient client) {
      client.badCertificateCallback =
          (X509Certificate cert, String host, int port) => true;
      return client;
    };
    response = await dio.post(url, data: jsonEncode(requestBody));
    print("res - signup api/user/sign_up");
    print(response);
    // String token = response.data["token"];
    return response;
  }
}

String _decodeBase64(String str) {
  String output = str.replaceAll('-', '+').replaceAll('_', '/');

  switch (output.length % 4) {
    case 0:
      break;
    case 2:
      output += '==';
      break;
    case 3:
      output += '=';
      break;
    default:
      throw Exception('Illegal base64url string!"');
  }

  return utf8.decode(base64Url.decode(output));
}

Map<String, dynamic> parseJwt(String token) {
  final parts = token.split('.');
  if (parts.length != 3) {
    throw Exception('invalid token');
  }

  final payload = _decodeBase64(parts[1]);
  final payloadMap = json.decode(payload);
  if (payloadMap is! Map<String, dynamic>) {
    throw Exception('invalid payload');
  }

  return payloadMap;
}

class LoginSignupPage extends StatefulWidget {
  LoginSignupPage({Key key}) : super(key: key);

  @override
  _LoginSignupPageState createState() => _LoginSignupPageState();
}

class _LoginSignupPageState extends State<LoginSignupPage>
    with TickerProviderStateMixin {
  PageController pageController;
  PageView pageView;
  int currentPage = 0;
  Color left = Colors.black;
  Color right = Colors.white;
  bool mainPage = true;
  bool signUp = true;

  AnimationController _controller;
  Animation<Offset> _offsetAnimation;

  @override
  void initState() {
    super.initState();
    _controller = AnimationController(
      duration: const Duration(milliseconds: 1500),
      vsync: this,
    )..forward();
    _offsetAnimation = Tween<Offset>(
      // begin: Offset.zero,
      // begin: const Offset(0.1, 1.0),
      end: const Offset(0, 0),
      begin: const Offset(1, 0),
      // end: Offset.zero,
    ).animate(_controller);
    // CurvedAnimation(
    //   parent: _controller,
    //   curve: Curves.elasticIn,
    // )
  }

  @override
  void dispose() {
    super.dispose();
    _controller.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
        height: MediaQuery.of(context).size.height,
        width: MediaQuery.of(context).size.width,
        decoration: BoxDecoration(
          gradient: LinearGradient(
            // colors: const [Colors.grey, Colors.lightBlueAccent],
            // colors: [Colors.grey[200], Colors.pink[100]],
            colors: const [Color(0xffD8D8D8), Color(0xffFE93BB)],
            stops: const [0.2, 1.0],
            begin: Alignment.topCenter,
            end: Alignment.bottomCenter,
          ),
        ),
        child: mainPage
            ? Column(
                mainAxisSize: MainAxisSize.max,
                children: <Widget>[
                  SizedBox(height: 100),
                  Image(
                      width: 250,
                      height: 300,
                      image: AssetImage('images/bg2.png')),
                  SizedBox(height: 80),
                  Container(
                    height: 50.0,
                    child: RaisedButton(
                      onPressed: () {
                        setState(() {
                          mainPage = false;
                          signUp = true;
                        });
                      },
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(80.0)),
                      padding: EdgeInsets.all(0.0),
                      child: Ink(
                        decoration: BoxDecoration(
                            gradient: LinearGradient(
                              colors: [Color(0xFF9583ED), Color(0xFFC66FD9)],
                              begin: Alignment.topLeft,
                              end: Alignment.bottomRight,
                              stops: const [0.0, 1.0],
                            ),
                            borderRadius: BorderRadius.circular(10.0)),
                        child: Container(
                          constraints:
                              BoxConstraints(maxWidth: 300.0, minHeight: 50.0),
                          alignment: Alignment.center,
                          child: Text(
                            "Sign up",
                            textAlign: TextAlign.center,
                            style:
                                TextStyle(color: Colors.white, fontSize: 18.0),
                          ),
                        ),
                      ),
                    ),
                  ),
                  SizedBox(height: 30),
                  Container(
                    height: 50.0,
                    child: RaisedButton(
                      onPressed: () {
                        setState(() {
                          mainPage = false;
                          signUp = false;
                        });
                      },
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(80.0)),
                      padding: EdgeInsets.all(0.0),
                      child: Ink(
                        decoration: BoxDecoration(
                            gradient: LinearGradient(
                              colors: [Color(0xFF9583ED), Color(0xFFC66FD9)],
                              begin: Alignment.topLeft,
                              end: Alignment.bottomRight,
                              stops: const [0.0, 1.0],
                            ),
                            borderRadius: BorderRadius.circular(10.0)),
                        child: Container(
                          constraints:
                              BoxConstraints(maxWidth: 300.0, minHeight: 50.0),
                          alignment: Alignment.center,
                          child: Text(
                            "Log in",
                            textAlign: TextAlign.center,
                            style:
                                TextStyle(color: Colors.white, fontSize: 18.0),
                          ),
                        ),
                      ),
                    ),
                  ),
                ],
              )
            : SlideTransition(
                position: _offsetAnimation,
                // alignment: ,
                child: Column(
                  mainAxisSize: MainAxisSize.max,
                  children: <Widget>[
                    SizedBox(height: 60),
                    Image(
                        width: 250,
                        height: 100,
                        image: AssetImage('images/logo.png')),
                    Expanded(
                        child: Container(
                      padding: EdgeInsets.only(left: 40, right: 40),
                      child: signUp
                          ? SignUpWidget(toChange: () {
                              setState(() {
                                signUp = false;
                              });
                            })
                          : LoginWidget(toChange: () {
                              setState(() {
                                signUp = true;
                              });
                            }),
                    )),
                  ],
                ),
              ));
  }
}

class TabIndicationPainter extends CustomPainter {
  Paint painter;
  final double dxTarget;
  final double dxEntry;
  final double radius;
  final double dy;

  final PageController pageController;

  TabIndicationPainter(
      {this.dxTarget = 125.0,
      this.dxEntry = 25.0,
      this.radius = 21.0,
      this.dy = 25.0,
      this.pageController})
      : super(repaint: pageController) {
    painter = Paint()
      ..color = Color(0xFFFFFFFF)
      ..style = PaintingStyle.fill;
  }

  @override
  void paint(Canvas canvas, Size size) {
    final pos = pageController.position;
    double fullExtent =
        (pos.maxScrollExtent - pos.minScrollExtent + pos.viewportDimension);

    double pageOffset = pos.extentBefore / fullExtent;

    bool left2right = dxEntry < dxTarget;
    Offset entry = Offset(left2right ? dxEntry : dxTarget, dy);
    Offset target = Offset(left2right ? dxTarget : dxEntry, dy);

    Path path = Path();
    path.addArc(
        Rect.fromCircle(center: entry, radius: radius), 0.5 * pi, 1 * pi);
    path.addRect(Rect.fromLTRB(entry.dx, dy - radius, target.dx, dy + radius));
    path.addArc(
        Rect.fromCircle(center: target, radius: radius), 1.5 * pi, 1 * pi);

    canvas.translate(size.width * pageOffset, 0.0);
    canvas.drawShadow(path, Color(0xFFfbab66), 3.0, true);
    canvas.drawPath(path, painter);
  }

  @override
  bool shouldRepaint(TabIndicationPainter oldDelegate) => true;
}

class SignUpWidget extends StatefulWidget {
  final VoidCallback toChange;
  // const SignUpWidget({Key key, this.toChange}) : super(key: key);
  SignUpWidget({Key key, this.toChange}) : super(key: key);

  final BaseAuth auth = new Auth();

  @override
  _SignUpWidgetState createState() => _SignUpWidgetState();
}

class _SignUpWidgetState extends State<SignUpWidget> {
  final formKey = GlobalKey<FormState>();
  bool isAutoValidate = false;
  // bool isAutoValidate = true;
  bool _showPassword = false;
  bool _showConfirmPassword = false;

  var email;
  var firstName;
  var lastName;
  var password;

  @override
  Widget build(BuildContext context) {
    return ListView(
      children: <Widget>[
        // MainTitleWidget(''),
        // Form(
        //   onWillPop: willPop,
        //   child: Column(
        //     crossAxisAlignment: CrossAxisAlignment.start,
        //     children: <Widget>[
        //       TextFormField(),
        //       Container(
        //         padding: const EdgeInsets.symmetric(vertical: 16.0),
        //         child: RaisedButton(
        //           onPressed: () {},
        //           child: Text('Submit'),
        //         ),
        //       ),
        //     ],
        //   ),
        // ),
        // MainTitleWidget('Form AutoValidate'),
        new Theme(
            data: new ThemeData(
              hintColor: Colors.grey,
              // primaryColorDark: Colors.red,
              // inputDecorationTheme: InputDecorationTheme(
              // labelStyle: TextStyle(
              //   color: Colors.yellow,
              //   fontSize: 24.0
              // ),
              // ),
            ),
            child: new Form(
              key: formKey,
              autovalidate: isAutoValidate,
              child: Column(
                children: <Widget>[
                  Container(
                    padding: EdgeInsets.only(bottom: 60.0),
                    child: Text(
                      "Sign up",
                      textAlign: TextAlign.center,
                      style: TextStyle(color: Colors.black, fontSize: 30.0),
                    ),
                  ),

                  Container(
                    padding: EdgeInsets.only(bottom: 10.0),
                    child: TextFormField(
                      // obscureText: !this._showPassword,
                      decoration: InputDecoration(
                          border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(10.0),
                              borderSide: BorderSide(
                                  width: 2, color: Colors.yellowAccent)),
                          focusedBorder: OutlineInputBorder(
                            borderRadius:
                                BorderRadius.all(Radius.circular(10.0)),
                            borderSide:
                                BorderSide(width: 2, color: Colors.pink[200]),
                          ),
                          // borderSide: BorderSide(width: 2,color: Colors.yellowAccent),
                          fillColor: Colors.white,
                          filled: true,
                          prefixIcon:
                              const Icon(Icons.email, color: Colors.grey),
                          // prefix: Text('Prefix'),
                          // suffix: Text('Suffix'),
                          hintText: 'Email address'),
                      keyboardType: TextInputType.text,
                      // validator: (value) {
                      //   if (value.isEmpty)
                      //     return 'Please enter some text';
                      //   }
                      //   return null;
                      // },

                      validator: (value) {
                        Pattern pattern =
                            r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
                        RegExp regex = RegExp(pattern);
                        if (!regex.hasMatch(value))
                          return 'Invalid Email address';
                        else
                          return null;
                      },
                      onSaved: (value) => email = value,
                      // onSaved: (value) {
                      //   validateInputs
                      //   name = value;
                      // },
                    ),
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Container(
                        padding: EdgeInsets.only(bottom: 10.0),
                        width: 150,
                        child: TextFormField(
                          decoration: InputDecoration(
                              border: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(10.0),
                                  borderSide: BorderSide(
                                      width: 2, color: Colors.yellowAccent)),
                              focusedBorder: OutlineInputBorder(
                                borderRadius:
                                    BorderRadius.all(Radius.circular(10.0)),
                                borderSide: BorderSide(
                                    width: 2, color: Colors.pink[200]),
                              ),
                              // borderSide: BorderSide(width: 2,color: Colors.yellowAccent),
                              fillColor: Colors.white,
                              filled: true,
                              // prefixIcon: const Icon(
                              //   Icons.person,
                              //   color: Colors.grey,
                              // ),
                              // prefix: Text('Prefix'),
                              // suffix: Text('Suffix'),
                              hintText: 'First name'),
                          keyboardType: TextInputType.text,
                          validator: (value) {
                            if (value.isEmpty) {
                              return 'Please enter first name';
                            }
                            return null;
                          },
                          onSaved: (value) => firstName = value,
                          // onSaved: (value) {
                          //   validateInputs
                          //   name = value;
                          // },
                        ),
                      ),
                      Container(
                        padding: EdgeInsets.only(bottom: 10.0),
                        width: 120,
                        child: TextFormField(
                          decoration: InputDecoration(
                              border: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(10.0),
                                  borderSide: BorderSide(
                                      width: 2, color: Colors.yellowAccent)),
                              focusedBorder: OutlineInputBorder(
                                borderRadius:
                                    BorderRadius.all(Radius.circular(10.0)),
                                borderSide: BorderSide(
                                    width: 2, color: Colors.pink[200]),
                              ),
                              // borderSide: BorderSide(width: 2,color: Colors.yellowAccent),
                              fillColor: Colors.white,
                              filled: true,
                              // prefixIcon: const Icon(
                              //   Icons.person,
                              //   color: Colors.grey,
                              // ),
                              // prefix: Text('Prefix'),
                              // suffix: Text('Suffix'),
                              hintText: 'Last name'),
                          keyboardType: TextInputType.text,
                          validator: (value) {
                            if (value.isEmpty) {
                              return 'Please enter last name';
                            }
                            return null;
                          },
                          onSaved: (value) => lastName = value,
                          // onSaved: (value) {
                          //   validateInputs
                          //   name = value;
                          // },
                        ),
                      ),
                    ],
                  ),
                  Container(
                    padding: EdgeInsets.only(bottom: 10.0),
                    child: TextFormField(
                      obscureText: !this._showPassword,
                      decoration: InputDecoration(
                          border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(10.0),
                              borderSide: BorderSide(
                                  width: 2, color: Colors.yellowAccent)),
                          focusedBorder: OutlineInputBorder(
                            borderRadius:
                                BorderRadius.all(Radius.circular(10.0)),
                            borderSide:
                                BorderSide(width: 2, color: Colors.pink[200]),
                          ),
                          // borderSide: BorderSide(width: 2,color: Colors.yellowAccent),
                          fillColor: Colors.white,
                          filled: true,
                          prefixIcon: const Icon(
                            Icons.lock,
                            color: Colors.grey,
                          ),
                          suffixIcon: IconButton(
                            icon: Icon(
                              Icons.remove_red_eye,
                              color: this._showPassword
                                  ? Colors.pink[300]
                                  : Colors.grey,
                            ),
                            onPressed: () {
                              setState(() =>
                                  this._showPassword = !this._showPassword);
                            },
                          ),
                          // prefix: Text('Prefix'),
                          // suffix: Text('Suffix'),
                          hintText: 'Password'),
                      keyboardType: TextInputType.text,
                      validator: (value) {
                        RegExp regex = RegExp(
                            "^(?=.*?[a-zA-Z])(?=.*?[0-9])(?=.*?[#!\$%^&*()_+|~=`{}\\[\\]:\";'<>?,./@-]).{8,16}\$");
                        if (!regex.hasMatch(value)) {
                          return 'Invalid password';
                        }
                        return null;
                      },
                      onSaved: (value) => password = value,
                      onChanged: (value) => password = value,
                      // onSaved: (value) {
                      //   validateInputs
                      //   name = value;
                      // },
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.only(bottom: 10.0),
                    child: TextFormField(
                      enableInteractiveSelection: false,
                      obscureText: !this._showConfirmPassword,
                      decoration: InputDecoration(
                          border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(10.0),
                              borderSide: BorderSide(
                                  width: 2, color: Colors.yellowAccent)),
                          focusedBorder: OutlineInputBorder(
                            borderRadius:
                                BorderRadius.all(Radius.circular(10.0)),
                            borderSide:
                                BorderSide(width: 2, color: Colors.pink[200]),
                          ),
                          // borderSide: BorderSide(width: 2,color: Colors.yellowAccent),
                          fillColor: Colors.white,
                          filled: true,
                          prefixIcon: const Icon(
                            Icons.lock,
                            color: Colors.grey,
                          ),
                          suffixIcon: IconButton(
                            icon: Icon(
                              Icons.remove_red_eye,
                              color: this._showConfirmPassword
                                  ? Colors.pink[300]
                                  : Colors.grey,
                            ),
                            onPressed: () {
                              setState(() => this._showConfirmPassword =
                                  !this._showConfirmPassword);
                            },
                          ),
                          // prefix: Text('Prefix'),
                          // suffix: Text('Suffix'),
                          hintText: 'Confirm password'),
                      keyboardType: TextInputType.text,
                      validator: (value) {
                        // print(this.password);
                        // print(password);
                        if (value != this.password) {
                          return 'Please confirm the password';
                        }
                        return null;
                        // RegExp regex = RegExp(
                        //   "^(?=.*?[a-zA-Z])(?=.*?[0-9])(?=.*?[#!\$%^&*()_+|~=`{}\\[\\]:\";'<>?,./@-]).{8,16}\$"
                        // );
                        // if (!regex.hasMatch(value)) {
                        //   return 'Please confirm the password';
                        // }
                        // return null;
                      },
                      onSaved: (value) => password = value,
                      // onSaved: (value) {
                      //   validateInputs
                      //   name = value;
                      // },
                    ),
                  ),

                  // TextFormField(
                  //   // decoration: const InputDecoration(labelText: 'Tel'),
                  //   decoration: const InputDecoration(hintText: 'Tel'),
                  //   keyboardType: TextInputType.phone,
                  //   validator: (value) {
                  //     if (value.length != 11)
                  //       return 'Tel Number must be 11 digit';
                  //     else
                  //       return null;
                  //   },
                  //   onSaved: (value) => tel = value,
                  // ),
                  // TextFormField(
                  //   decoration: const InputDecoration(labelText: 'Email'),
                  //   keyboardType: TextInputType.emailAddress,
                  //   validator: (value) {
                  //     Pattern pattern =
                  //         r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
                  //     RegExp regex = RegExp(pattern);
                  //     if (!regex.hasMatch(value))
                  //       return 'Not Valid Email';
                  //     else
                  //       return null;
                  //   },
                  //   onSaved: (value) => email = value,
                  // ),
                  SizedBox(height: 10),
                  // RaisedButton(
                  //   onPressed: () {
                  //     if(validateInputs()){
                  //       // willPop();
                  //       signUp();
                  //     }
                  //   },
                  //   child: Text('Validate & Submit'),
                  // ),
                  // ButtonTheme(
                  //   minWidth: 290.0,
                  //   // height: 100.0,
                  //   child: FlatButton(
                  //     color: Colors.purpleAccent,
                  //     textColor: Colors.white,
                  //     disabledColor: Colors.grey,
                  //     disabledTextColor: Colors.black,
                  //     padding: EdgeInsets.only(top:12.0, bottom: 12.0),
                  //     splashColor: Colors.blueAccent,
                  //     shape: new RoundedRectangleBorder(borderRadius: new BorderRadius.circular(10.0)),
                  //     onPressed: () {
                  //       if(validateInputs()){
                  //         signUp();
                  //       }
                  //     },
                  //     child: Text(
                  //       "Sign up",
                  //       style: TextStyle(fontSize: 18.0),
                  //     ),
                  //   ),
                  // ),
                  Container(
                    height: 50.0,
                    child: RaisedButton(
                      onPressed: () {
                        if (validateInputs()) {
                          signUp();
                        }
                      },
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(80.0)),
                      padding: EdgeInsets.all(0.0),
                      child: Ink(
                        decoration: BoxDecoration(
                            gradient: LinearGradient(
                              colors: [Color(0xFF9583ED), Color(0xFFC66FD9)],
                              begin: Alignment.topLeft,
                              end: Alignment.bottomRight,
                              stops: const [0.0, 1.0],
                            ),
                            borderRadius: BorderRadius.circular(10.0)),
                        child: Container(
                          constraints:
                              BoxConstraints(maxWidth: 300.0, minHeight: 50.0),
                          alignment: Alignment.center,
                          child: Text(
                            "Sign up",
                            textAlign: TextAlign.center,
                            style:
                                TextStyle(color: Colors.white, fontSize: 18.0),
                          ),
                        ),
                      ),
                    ),
                  ),
                  // new Text(
                  //   "Don't have an account? Sign Up",
                  //   textAlign: TextAlign.center,
                  //   overflow: TextOverflow.ellipsis,
                  //   softWrap: true,
                  //   style: new TextStyle(
                  //       fontWeight: FontWeight.w300,
                  //       letterSpacing: 0.5,
                  //       color: Colors.black,
                  //       fontSize: 12.0),
                  // ),
                  Container(
                      padding: EdgeInsets.only(top: 10),
                      child: Row(
                        children: <Widget>[
                          Text('I already have an account, login'),
                          Container(
                              constraints: BoxConstraints(maxWidth: 40),
                              alignment: Alignment.center,
                              child: FlatButton(
                                materialTapTargetSize:
                                    MaterialTapTargetSize.shrinkWrap,
                                padding: EdgeInsets.all(0),
                                textColor: Colors.blue,
                                child: Text(
                                  'here',
                                  // style: TextStyle(fontSize: 20),
                                ),
                                onPressed: () {
                                  widget.toChange();
                                  // signUp = false;
                                  // setState(() => signUp = true);
                                  //signup screen
                                },
                              ))
                        ],
                        mainAxisAlignment: MainAxisAlignment.center,
                      ))
                ],
              ),
            )),
      ],
    );
  }

  // void validateInputs() {
  //   if (formKey.currentState.validate()) {
  //     formKey.currentState.save();
  //   } else {
  //     setState(() => isAutoValidate = true);
  //   }
  // }

  bool validateInputs() {
    bool isValid;
    if (formKey.currentState.validate()) {
      formKey.currentState.save();
      isValid = true;
    } else {
      setState(() => isAutoValidate = true);
      isValid = false;
    }
    return isValid;
  }

  void signUp() async {
    print(this.email);
    // print(firstName + lastName);
    print(this.password);
    var response =
        await widget.auth.signup(email, firstName, lastName, password);
    response.statusCode = 200;
    if (response.statusCode == 200) {
      await willPop();
      // showDialog(
      //     context: context,
      //     builder: (context) => MHAlertDialog.alert(
      //           context,
      //           title: Text('Sign up success'),
      //           content: Text("Please check the email to confirm"),
      //           actions: <Widget>[
      //             MHDialogAction(
      //               child: Text('OK'),
      //               // isDestructiveAction: true,
      //               onPressed: () {
      //                 Navigator.of(context).pop();
      //               },
      //             ),
      //           ],
      //         ));
      widget.toChange();
    }
  }

  Future<bool> willPop() {
    return showDialog(
      builder: (context) => AlertDialog(
        title: Text('Sign up success'),
        content: Text("Please check the email to confirm"),
        actions: <Widget>[
          // FlatButton(child: Text('No'), onPressed: () => null
          // Navigator.pop(context, false),
          // ),
          FlatButton(
            child: Text('OK'),
            onPressed: () =>
                // null
                Navigator.pop(context, true),
          ),
        ],
      ),
      context: context,
    );
  }
}

class LoginWidget extends StatefulWidget {
  final VoidCallback toChange;
  // const LoginWidget({Key key, this.toChange}) : super(key: key);
  LoginWidget({Key key, this.toChange}) : super(key: key);
  final BaseAuth auth = new Auth();

  @override
  _LoginWidgetState createState() => _LoginWidgetState();
}

class _LoginWidgetState extends State<LoginWidget> {
  final formKey = GlobalKey<FormState>();
  bool isAutoValidate = false;
  // bool isAutoValidate = true;
  bool _showPassword = false;
  var email;
  // var username;
  String firstName;
  String lastName;
  var password;
  bool _isLoginSuccess;
  Map<String, dynamic> userToken;

  @override
  void initState() {
    email = '';
    // username = '';
    firstName = '';
    lastName = '';
    password = '';
    _isLoginSuccess = false;
    // _userName = "";

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return ListView(
      children: <Widget>[
        // MainTitleWidget('Form基本使用'),
        // Form(
        //   onWillPop: willPop,
        //   child: Column(
        //     crossAxisAlignment: CrossAxisAlignment.start,
        //     children: <Widget>[
        //       TextFormField(),
        //       Container(
        //         padding: const EdgeInsets.symmetric(vertical: 16.0),
        //         child: RaisedButton(
        //           onPressed: () {},
        //           child: Text('Submit'),
        //         ),
        //       ),
        //     ],
        //   ),
        // ),
        // MainTitleWidget('Form AutoValidate'),
        new Theme(
            data: new ThemeData(
              hintColor: Colors.grey,
              // primaryColorDark: Colors.red,
              // inputDecorationTheme: InputDecorationTheme(
              // labelStyle: TextStyle(
              //   color: Colors.yellow,
              //   fontSize: 24.0
              // ),
              // ),
            ),
            child: new Form(
              key: formKey,
              autovalidate: isAutoValidate,
              child: Column(
                children: <Widget>[
                  Container(
                    padding: EdgeInsets.only(bottom: 60.0),
                    child: Text(
                      "Log in",
                      textAlign: TextAlign.center,
                      style: TextStyle(color: Colors.black, fontSize: 30.0),
                    ),
                  ),

                  Container(
                    padding: EdgeInsets.only(bottom: 10.0),
                    child: TextFormField(
                        // obscureText: !this._showPassword,
                        decoration: InputDecoration(
                            border: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(10.0),
                                borderSide: BorderSide(
                                    width: 2, color: Colors.yellowAccent)),
                            focusedBorder: OutlineInputBorder(
                              borderRadius:
                                  BorderRadius.all(Radius.circular(10.0)),
                              borderSide:
                                  BorderSide(width: 2, color: Colors.pink[200]),
                            ),
                            // borderSide: BorderSide(width: 2,color: Colors.yellowAccent),
                            fillColor: Colors.white,
                            filled: true,
                            prefixIcon:
                                const Icon(Icons.email, color: Colors.grey),
                            // prefix: Text('Prefix'),
                            // suffix: Text('Suffix'),
                            hintText: 'Email address'),
                        keyboardType: TextInputType.text,
                        // validator: (value) {
                        //   if (value.isEmpty)
                        //     return 'Please enter some text';
                        //   }
                        //   return null;
                        // },

                        validator: (value) {
                          Pattern pattern =
                              r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
                          RegExp regex = RegExp(pattern);
                          if (!regex.hasMatch(value))
                            return 'Invalid Email address';
                          else
                            return null;
                        },
                        onSaved: (value) => email = value,
                        onChanged: (value) => email = value
                        // onSaved: (value) {
                        //   validateInputs
                        //   name = value;
                        // },
                        ),
                  ),
                  Container(
                    padding: EdgeInsets.only(bottom: 10.0),
                    child: TextFormField(
                      obscureText: !this._showPassword,
                      decoration: InputDecoration(
                          border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(10.0),
                              borderSide: BorderSide(
                                  width: 2, color: Colors.yellowAccent)),
                          focusedBorder: OutlineInputBorder(
                            borderRadius:
                                BorderRadius.all(Radius.circular(10.0)),
                            borderSide:
                                BorderSide(width: 2, color: Colors.pink[200]),
                          ),
                          // borderSide: BorderSide(width: 2,color: Colors.yellowAccent),
                          fillColor: Colors.white,
                          filled: true,
                          prefixIcon: const Icon(
                            Icons.lock,
                            color: Colors.grey,
                          ),
                          suffixIcon: IconButton(
                            icon: Icon(
                              Icons.remove_red_eye,
                              color: this._showPassword
                                  ? Colors.pink[300]
                                  : Colors.grey,
                            ),
                            onPressed: () {
                              setState(() =>
                                  this._showPassword = !this._showPassword);
                            },
                          ),
                          // prefix: Text('Prefix'),
                          // suffix: Text('Suffix'),
                          hintText: 'Password'),
                      keyboardType: TextInputType.text,
                      validator: (value) {
                        if (value.isEmpty) {
                          return 'Please enter password';
                        }
                        return null;
                      },
                      onSaved: (value) => password = value,
                      onChanged: (value) => password = value,
                      // onSaved: (value) {
                      //   validateInputs
                      //   name = value;
                      // },
                    ),
                  ),
                  // TextFormField(
                  //   // decoration: const InputDecoration(labelText: 'Tel'),
                  //   decoration: const InputDecoration(hintText: 'Tel'),
                  //   keyboardType: TextInputType.phone,
                  //   validator: (value) {
                  //     if (value.length != 11)
                  //       return 'Tel Number must be 11 digit';
                  //     else
                  //       return null;
                  //   },
                  //   onSaved: (value) => tel = value,
                  // ),
                  // TextFormField(
                  //   decoration: const InputDecoration(labelText: 'Email'),
                  //   keyboardType: TextInputType.emailAddress,
                  //   validator: (value) {
                  //     Pattern pattern =
                  //         r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
                  //     RegExp regex = RegExp(pattern);
                  //     if (!regex.hasMatch(value))
                  //       return 'Not Valid Email';
                  //     else
                  //       return null;
                  //   },
                  //   onSaved: (value) => email = value,
                  // ),
                  SizedBox(height: 10),
                  // RaisedButton(
                  //   onPressed: () {
                  //     if(validateInputs()){
                  //       // willPop();
                  //       signUp();
                  //     }
                  //   },
                  //   child: Text('Validate & Submit'),
                  // ),
                  // ButtonTheme(
                  //   minWidth: 290.0,
                  //   // height: 100.0,
                  //   child: FlatButton(
                  //     color: Colors.purpleAccent,
                  //     textColor: Colors.white,
                  //     disabledColor: Colors.grey,
                  //     disabledTextColor: Colors.black,
                  //     padding: EdgeInsets.only(top:12.0, bottom: 12.0),
                  //     splashColor: Colors.blueAccent,
                  //     shape: new RoundedRectangleBorder(borderRadius: new BorderRadius.circular(10.0)),
                  //     onPressed: () {
                  //       if(validateInputs()){
                  //         signUp();
                  //       }
                  //     },
                  //     child: Text(
                  //       "Sign up",
                  //       style: TextStyle(fontSize: 18.0),
                  //     ),
                  //   ),
                  // ),
                  Align(
                    alignment: Alignment.centerRight,
                    child: Container(
                      padding: EdgeInsets.only(bottom: 52.0),
                      child: Text(
                        "Forget password?",
                        style: TextStyle(
                          color: Colors.black,
                          fontSize: 14.0,
                          fontStyle: FontStyle.italic,
                        ),
                      ),
                    ),
                  ),
                  Container(
                    height: 50.0,
                    child: RaisedButton(
                      onPressed: () {
                        if (validateInputs()) {
                          login();
                        }
                      },
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(80.0)),
                      padding: EdgeInsets.all(0.0),
                      child: Ink(
                        decoration: BoxDecoration(
                            gradient: LinearGradient(
                              colors: [Color(0xFF9583ED), Color(0xFFC66FD9)],
                              begin: Alignment.topLeft,
                              end: Alignment.bottomRight,
                              stops: const [0.0, 1.0],
                            ),
                            borderRadius: BorderRadius.circular(10.0)),
                        child: Container(
                          constraints:
                              BoxConstraints(maxWidth: 300.0, minHeight: 50.0),
                          alignment: Alignment.center,
                          child: Text(
                            "Log in",
                            textAlign: TextAlign.center,
                            style:
                                TextStyle(color: Colors.white, fontSize: 18.0),
                          ),
                        ),
                      ),
                    ),
                  ),
                  // new Text(
                  //   "Don't have an account? Sign Up",
                  //   textAlign: TextAlign.center,
                  //   overflow: TextOverflow.ellipsis,
                  //   softWrap: true,
                  //   style: new TextStyle(
                  //       fontWeight: FontWeight.w300,
                  //       letterSpacing: 0.5,
                  //       color: Colors.black,
                  //       fontSize: 12.0),
                  // ),
                  Container(
                      padding: EdgeInsets.only(top: 10),
                      child: Row(
                        children: <Widget>[
                          Text("I don't have an account, signup"),
                          Container(
                              constraints: BoxConstraints(maxWidth: 40),
                              alignment: Alignment.center,
                              child: FlatButton(
                                materialTapTargetSize:
                                    MaterialTapTargetSize.shrinkWrap,
                                padding: EdgeInsets.all(0),
                                textColor: Colors.blue,
                                child: Text(
                                  'here',
                                  // style: TextStyle(fontSize: 20),
                                ),
                                onPressed: () {
                                  widget.toChange();
                                  // signUp = false;
                                  // setState(() => signUp = true);
                                  //signup screen
                                },
                              ))
                        ],
                        mainAxisAlignment: MainAxisAlignment.center,
                      )),
                  // _isLoginSuccess?
                  //   showLoginSuccess()
                  //   :
                  //   Container()
                ],
              ),
            )),
      ],
    );
  }

  Widget showLoginSuccess() {
    return new Container(
        child: Center(
      child: Column(
        children: <Widget>[
          // Container(padding: Container(all: 100),)
          Icon(
            Icons.check_circle,
            color: Colors.green,
          ),
          Text(userToken["sub"]),
          Text("Login Successfully"),
        ],
      ),
      // heightFactor: 8,
      // widthFactor: 20
    ));
  }

  bool validateInputs() {
    bool isValid;
    if (formKey.currentState.validate()) {
      formKey.currentState.save();
      isValid = true;
    } else {
      setState(() => isAutoValidate = true);
      isValid = false;
    }
    return isValid;
  }

  void login() async {
    print(email);
    // print(username);
    print(password);
    // email = "zxgao_test1";
    // password = "%Tr@in349";

    String token = '';
    var response = await widget.auth.login(email, password);
    print(response.statusCode);
    if (response.statusCode == 200) {
      // userToken = jsonDecode(token);
      setState(() {
        _isLoginSuccess = true;
        // userToken = jsonDecode(token);
      });

      // final result = await Navigator.push(context, MaterialPageRoute(
      Navigator.pushAndRemoveUntil(
          context,
          MaterialPageRoute(
              // builder: (context) => LoginSuccessPage(
              //     userToken: UserToken(userToken["sub"], userToken["role"],
              //         userToken["exp"], userToken["iat"]))
              builder: (context) => DrawerWidget()),
          (route) => route == null);
    }
  }
}

// class UserToken {
//   final String sub;
//   final String role;
//   final int expTime;
//   final int issueTime;
//   UserToken(this.sub, this.role, this.expTime, this.issueTime);
// }

// class LoginSuccessPage extends StatelessWidget {
//   final UserToken userToken;
//   LoginSuccessPage({Key key, @required this.userToken}) : super(key: key);

//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       appBar: AppBar(
//           // title: Text("title"),
//           ),
//       body: Container(
//         padding: EdgeInsets.all(16.0),
//         child: showLoginSuccess(),
//       ),
//     );
//   }

//   Widget showLoginSuccess() {
//     print(userToken.expTime);
//     var expTime =
//         new DateTime.fromMillisecondsSinceEpoch(userToken.expTime * 1000)
//             .toString();
//     var issueTime =
//         new DateTime.fromMillisecondsSinceEpoch(userToken.issueTime * 1000)
//             .toString();
//     return new Container(
//         child: Center(
//       child: Column(
//         children: <Widget>[
//           Container(
//             padding: EdgeInsets.all(100),
//           ),
//           Icon(
//             Icons.check_circle,
//             color: Colors.green,
//           ),
//           Text("Login Successfully"),
//           Align(
//             // alignment: Alignment.centerLeft,
//             child: Container(
//               padding: EdgeInsets.only(top: 30.0),
//               child: Column(
//                 // padding: EdgeInsets.all(100),
//                 children: <Widget>[
//                   Text("Subject: " + userToken.sub, textAlign: TextAlign.left),
//                   Text("Role: " + userToken.role, textAlign: TextAlign.left),
//                   Text("Expire Time: " + expTime, textAlign: TextAlign.left),
//                   Text("IssueTime: " + issueTime, textAlign: TextAlign.left),
//                 ],
//               ),
//             ),
//           ),
//         ],
//       ),
//       // heightFactor: 8,
//       // widthFactor: 20
//     ));
//   }
// }
