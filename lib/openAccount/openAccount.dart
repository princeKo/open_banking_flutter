import 'package:flutter/material.dart';
import 'dart:io';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'dart:convert';
import 'openAccountWebView.dart';

void main() {
  runApp(MaterialApp(
      home: Scaffold(
    body: OpenAccountPage(),
  )));
}

class OpenAccountPage extends StatefulWidget {
  @override
  _OpenAccountPagePageState createState() => _OpenAccountPagePageState();
}

class _OpenAccountPagePageState extends State {
  final storage = FlutterSecureStorage();

  Map<String, dynamic> allBanks = {};
  String selectedBank;

  @override
  void initState() {
    super.initState();
    _getAllBanks();
  }

  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    print("this.bankItems");
    print(this.allBanks);

    return Scaffold(
        body: new ListView(
            padding: const EdgeInsets.only(left: 20, right: 20),
            children: <Widget>[
          Container(
              margin: const EdgeInsets.only(bottom: 20),
              child: Text(
                "Open account",
                textAlign: TextAlign.left,
                style: new TextStyle(
                    fontWeight: FontWeight.w400,
                    letterSpacing: 0.5,
                    color: Colors.black,
                    fontSize: 26.0),
              )),
          Container(
              height: 600,
              color: Colors.white,
              child: Column(children: <Widget>[
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: <Widget>[
                    Expanded(
                        child: Container(
                            padding: const EdgeInsets.only(
                                top: 25.0, bottom: 25.0, left: 20),
                            child: Text(
                              "Bank Name",
                              style: new TextStyle(
                                  fontWeight: FontWeight.w500,
                                  letterSpacing: 0.5,
                                  color: Colors.black,
                                  fontSize: 17.0),
                            ))),
                    Expanded(
                        child: Container(
                      padding: const EdgeInsets.only(
                          top: 25.0, bottom: 25.0, left: 50),
                      child: Text(
                        "Bank Website",
                        style: new TextStyle(
                            fontWeight: FontWeight.w500,
                            letterSpacing: 0.5,
                            color: Colors.black,
                            fontSize: 17.0),
                      ),
                    )),
                  ],
                ),
                Divider(color: Colors.black54, height: 0),
                // Container(
                //     color: Colors.green,
                //     alignment: Alignment.centerLeft,
                //     padding: const EdgeInsets.only(left: 50),
                //     height: 300,
                //     child: Image.network(
                //       // "https://10.6.88.55:8080/static/img/bankbicon.png",
                //       // 'https://openbankportal:8080/static/img/bankaicon.png',
                //       // 'https://flutter.io/images/catalog-widget-placeholder.png',
                //       // 'https://10.6.88.55:8080/static/img/test1icon.svg',
                //       // 'https://images.unsplash.com/photo-1547721064-da6cfb341d50',
                //       // "https://googleflutter.com/sample_image.jpg",
                //       // "https://images.pexels.com/photos/462118/pexels-photo-462118.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500",
                //       // "https://srv4.imgonline.com.ua/result_img/imgonline-com-ua-Resize-inch-mm-cm-s5qYrJUSScpxc9.png",
                //       "https://srv2.imgonline.com.ua/result_img/imgonline-com-ua-Resize-inch-mm-cm-92lryJe6ugk.png",
                //       width: 30,
                //       // scale: 0.15,
                //       // color: Colors.green,
                //     )),
                ..._buildBankList(),
                //   Row(
                //     mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                //     children: <Widget>[
                //       Expanded(
                //           child: Container(
                //         padding: const EdgeInsets.only(left: 20),
                //         alignment: Alignment.centerLeft,
                //         height: 50,
                //         child: Text("Bank A"),
                //       )),
                //       Expanded(
                //           child: Container(
                //         alignment: Alignment.centerLeft,
                //         padding: const EdgeInsets.only(left: 50),
                //         height: 50,
                //         child: IconButton(
                //             icon: Image(
                //               image: AssetImage('images/BankA.png'),
                //               height: 35,
                //               width: 35,
                //             ),
                //             onPressed: () => {
                //                   Navigator.push(
                //                       context,
                //                       MaterialPageRoute(
                //                           builder: (context) =>
                //                               InAppWebViewExampleScreen(
                //                                 service: new Service(
                //                                   "Open Account",
                //                                   // url,
                //                                   // "http://openbank:8180",
                //                                   "http://10.6.126.139:8180",
                //                                   {
                //                                     "token": {
                //                                       "url": "https://10.6.88.55",
                //                                       "name": "token",
                //                                       "value": "token"
                //                                     },
                //                                   },
                //                                 ),
                //                               )))
                //                 }),
                //       ))
                //     ],
                //   ),
                //   Divider(color: Colors.black54, height: 0),
                //   Row(
                //     mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                //     children: <Widget>[
                //       Expanded(
                //           child: Container(
                //         padding: const EdgeInsets.only(left: 20),
                //         alignment: Alignment.centerLeft,
                //         height: 50,
                //         child: Text("Bank B"),
                //       )),
                //       Expanded(
                //           child: Container(
                //         padding: const EdgeInsets.only(left: 50),
                //         alignment: Alignment.centerLeft,
                //         height: 50,
                //         child: IconButton(
                //             icon: Image(
                //               image: AssetImage('images/BankB.png'),
                //               height: 35,
                //               width: 35,
                //             ),
                //             onPressed: null),
                //       ))
                //     ],
                //   ),
                //   Divider(color: Colors.black54, height: 0),
                //   Row(
                //     mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                //     children: <Widget>[
                //       Expanded(
                //           child: Container(
                //         padding: const EdgeInsets.only(left: 20),
                //         alignment: Alignment.centerLeft,
                //         height: 50,
                //         child: Text("Bank C"),
                //       )),
                //       Expanded(
                //           child: Container(
                //         alignment: Alignment.centerLeft,
                //         padding: const EdgeInsets.only(left: 50),
                //         height: 50,
                //         child: IconButton(
                //             icon: Image(
                //               image: AssetImage('images/BankC.png'),
                //               height: 35,
                //               width: 35,
                //             ),
                //             onPressed: null),
                //       )),
                //     ],
                //   ),
                //   Divider(color: Colors.black54, height: 0),
              ]))
        ]));
  }

  List _buildBankList() {
    var banks = this.allBanks.keys.toList();
    banks.sort();
    List<Column> list = banks.map((bankId) {
      return Column(children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: <Widget>[
            Expanded(
                child: Container(
              padding: const EdgeInsets.only(left: 20),
              alignment: Alignment.centerLeft,
              height: 50,
              child: Text(this.allBanks[bankId]["bankName"]),
            )),
            Expanded(
                child: Container(
              alignment: Alignment.centerLeft,
              padding: const EdgeInsets.only(left: 50),
              height: 50,
              child: IconButton(
                  // icon: ImageIcon(image:
                  // Image.network(
                  //   this.allBanks[bankId]["iconUrl"],
                  //   height: 35,
                  //   width: 35,
                  // )),
                  // icon: ImageIcon(
                  //   NetworkImage(
                  //       // 'https://flutter.io/images/catalog-widget-placeholder.png',
                  //       'https://10.6.88.55:8080/static/img/bankbicon.png',
                  //       scale: 100),
                  //   size: 50,
                  //   color: Colors.green,
                  // ),

                  icon: Image(
                    image: AssetImage(bankId == "banka"
                        ? 'images/BankA.png'
                        : 'images/BankB.png'),
                    height: 35,
                    width: 35,
                  ),
                  onPressed: () => {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => InAppWebViewExampleScreen(
                                      service: new Service(
                                        "Open Account",
                                        // url,
                                        this.allBanks[bankId]["url"],
                                        // "http://openbank:8180",
                                        // "http://10.6.126.139:8180",
                                        {
                                          // "token": {
                                          //   "url": "https://10.6.88.55",
                                          //   "name": "token",
                                          //   "value": "token"
                                          // },
                                        },
                                      ),
                                    )))
                      }),
            )),
          ],
        ),
        Divider(color: Colors.black54, height: 0),
      ]);
    }).toList();
    return list;
  }

  Future<Null> _getAllBanks() async {
    String token = await storage.read(key: "token");
    var email = await storage.read(key: "token_sub");

    // print(token);
    var url = "https://10.6.88.55:8084/api/sys/bankId_enquiry";
    var requestBody = {};
    HttpClient httpClient = new HttpClient()
      ..badCertificateCallback =
          ((X509Certificate cert, String host, int port) => true);
    HttpClientRequest request = await httpClient.postUrl(Uri.parse(url));
    request.headers.set('Authorization', token);
    request.headers.set(HttpHeaders.contentTypeHeader, "application/json");
    request.write(jsonEncode(requestBody));
    HttpClientResponse response = await request.close();
    String reply = await response.transform(utf8.decoder).join();
    var body = json.decode(reply);
    print("res - _getAllBanks api/sys/bankId_enquiry");
    print(body);
    // List filteredData =
    //     body.keys.where((key) => body[key]["moduleType"] != "AP").toList();
    // print(filteredData);

    setState(() {
      allBanks = body["bankIds"];
    });
  }
}
