import 'package:flutter/material.dart';
import 'dart:io';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'dart:convert';

void main() {
  runApp(MaterialApp(
      home: Scaffold(
    // appBar: AppBar(
    //   title: Text('Demo'),
    // ),
    body: OpenAccountPage(),
  )));
}

class OpenAccountPage extends StatefulWidget {
  // final Map<String, dynamic> allBanks;

  // OpenAccountPage({Key key, @required this.allBanks}) : super(key: key);

  @override
  _OpenAccountPagePageState createState() => _OpenAccountPagePageState();
}

class _OpenAccountPagePageState extends State {
  final storage = FlutterSecureStorage();

  Map<String, dynamic> allBanks = {};
  String selectedBank;
  String selectedAccount;
  String authUrl;

  String email = '';
  String username = '';
  String password = '';
  final _emailController = new TextEditingController();
  final _usernameController = new TextEditingController();
  final _passwordController = new TextEditingController();

  @override
  void initState() {
    super.initState();
    _getAllBanks();
    //  Converte value into lowercase; and disable copy and paste
    // _emailController.addListener(() {
    //   final text = _emailController.text.toLowerCase();
    //   _emailController.value = _emailController.value.copyWith(
    //     text: text,
    //     selection:
    //     TextSelection(baseOffset: text.length, extentOffset: text.length),
    //     composing: TextRange.empty,
    //   );
    // });
  }

  void dispose() {
    _emailController.dispose();
    _usernameController.dispose();
    _passwordController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    print("this.bankItems");
    print(this.allBanks);
    print(email);
    print(username);
    print(password);

    return Scaffold(
        body: new ListView(
            padding: const EdgeInsets.only(left: 20, right: 20),
            children: <Widget>[
          Container(
              margin: const EdgeInsets.only(bottom: 20),
              child: Text(
                "Open account",
                textAlign: TextAlign.left,
                style: new TextStyle(
                    fontWeight: FontWeight.w400,
                    letterSpacing: 0.5,
                    color: Colors.black,
                    fontSize: 26.0),
              )),
          Container(
              // padding: EdgeInsets.all(20),
              height: 600,
              child: Container(
                  color: Colors.white,
                  child: Column(children: <Widget>[
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Container(
                            padding: const EdgeInsets.only(top: 10.0),
                            child: FlatButton(
                              onPressed: () {
                                // Navigator.of(context).pop();
                                print("Reset is clicked");
                                setState(() {
                                  // selectedBank = "";
                                  // email = '';
                                  // username = '';
                                  // password = '';
                                  _emailController.clear();
                                  _usernameController.clear();
                                  _passwordController.clear();
                                });
                              },
                              child: Text(
                                "Reset",
                              ),
                            )),
                        Container(
                          padding: const EdgeInsets.only(top: 10.0),
                          // child: Text(
                          //   "Edit",
                          // ),
                          child: FlatButton(
                            onPressed: () async {
                              print("Hello ! =======");
                              print(this.selectedBank);
                              print(this.selectedAccount);
                              if (this.selectedAccount != null &&
                                  this.selectedAccount != null) {
                                bool result = await showAddNewDialog(context);
                                if (result == true) {
                                  print("selected Confirm");
                                  await _addAssociate(
                                      this.selectedBank, this.selectedAccount);
                                } else {
                                  print("selected Later");
                                }
                              } else {
                                print(
                                    "Please select bank name and bank username");
                              }
                              // print(this.module.settings);
                              // print(this.module.associatedBanks);
                              // setState(() {
                              //   isEditing = !isEditing;
                              // });
                              // _editModule(this.module);
                            },
                            child: Text("Confirm"),
                          ),
                        ),
                      ],
                    ),
                    Divider(color: Colors.black54, height: 0),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Container(
                          margin: const EdgeInsets.only(left: 10),
                          child: Text("Bank's name"),
                        ),
                        Container(
                          margin: const EdgeInsets.only(right: 10),
                          width: 200,
                          child:
                              //     ButtonBar(
                              //   alignment: MainAxisAlignment.center,
                              //   children: <Widget>[
                              //     DropdownButton(
                              //       value: selectedBank,
                              //       items: generateItemList(),
                              //       hint: Text("choose a bank"),
                              //       onChanged: (value) {
                              //         setState(() => selectedBank = value);
                              //       },
                              //       onTap: () {
                              //         print("onTap");
                              //       },
                              //     )
                              //   ],
                              // )
                              DropdownButtonHideUnderline(
                            child: DropdownButton(
                              items:
                                  generateItemList(this.allBanks.keys.toList()),
                              hint: Text("choose a bank"),
                              value: this.selectedBank,
                              onChanged: (value) {
                                setState(() {
                                  selectedBank = value;
                                  // Navigator.of(context).pop();
                                  // showEnrollNewDialog(context);
                                });
                              },
                              isExpanded: true,
                            ),
                          ),
                        )
                      ],
                    ),
                    Divider(color: Colors.black54, height: 0),
                    Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Container(
                            margin: const EdgeInsets.only(left: 10),
                            child: Text("Email"),
                          ),
                          Container(
                            margin: const EdgeInsets.only(right: 10),
                            width: 200,
                            child: TextFormField(
                                controller: _emailController,
                                autocorrect: true,
                                decoration: new InputDecoration(
                                  border: InputBorder.none,
                                  focusedBorder: InputBorder.none,
                                  enabledBorder: InputBorder.none,
                                  errorBorder: InputBorder.none,
                                  disabledBorder: InputBorder.none,
                                  // contentPadding: EdgeInsets.only(
                                  //     left: 15, bottom: 11, top: 11, right: 15),
                                  // hintText: "sLabel",
                                ),

                                // borderSide: BorderSide(width: 2,color: Colors.yellowAccent),
                                //     fillColor: Colors.white,
                                //     filled: true,
                                //     prefixIcon: const Icon(Icons.email, color: Colors.grey),
                                //     // prefix: Text('Prefix'),
                                //     // suffix: Text('Suffix'),
                                //     hintText: 'Email address'),
                                // keyboardType: TextInputType.text,
                                // // validator: (value) {
                                // //   if (value.isEmpty)
                                // //     return 'Please enter some text';
                                // //   }
                                // //   return null;
                                // // },

                                // validator: (value) {
                                //   Pattern pattern =
                                //       r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
                                //   RegExp regex = RegExp(pattern);
                                //   if (!regex.hasMatch(value))
                                //     return 'Invalid Email address';
                                //   else
                                //     return null;
                                // },
                                onSaved: (value) {
                                  setState(() {
                                    email = value;
                                  });
                                },
                                onChanged: (value) {
                                  setState(() {
                                    email = value;
                                  });
                                }
                                // // onSaved: (value) {
                                // //   validateInputs
                                // //   name = value;
                                // // },
                                ),
                          )
                        ]),
                    Divider(color: Colors.black54, height: 0),
                    Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Container(
                            margin: const EdgeInsets.only(left: 10),
                            child: Text("Username"),
                          ),
                          Container(
                            margin: const EdgeInsets.only(right: 10),
                            width: 200,
                            child: TextFormField(
                                controller: _usernameController,
                                decoration: new InputDecoration(
                                  border: InputBorder.none,
                                  focusedBorder: InputBorder.none,
                                  enabledBorder: InputBorder.none,
                                  errorBorder: InputBorder.none,
                                  disabledBorder: InputBorder.none,
                                  // hintText: "sLabel",
                                ),
                                onChanged: (value) {
                                  setState(() {
                                    username = value;
                                  });
                                }),
                          )
                        ]),
                    Divider(color: Colors.black54, height: 0),
                    Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Container(
                            margin: const EdgeInsets.only(left: 10),
                            child: Text("Password"),
                          ),
                          Container(
                            margin: const EdgeInsets.only(right: 10),
                            width: 200,
                            child: TextFormField(
                                controller: _passwordController,
                                decoration: new InputDecoration(
                                  border: InputBorder.none,
                                  focusedBorder: InputBorder.none,
                                  enabledBorder: InputBorder.none,
                                  errorBorder: InputBorder.none,
                                  disabledBorder: InputBorder.none,
                                  // hintText: "sLabel",
                                ),
                                onChanged: (value) {
                                  setState(() {
                                    password = value;
                                  });
                                }),
                          )
                        ]),
                    Divider(color: Colors.black54, height: 0),
                  ])))
        ]));
  }

  Future<Null> _getAllBanks() async {
    String token = await storage.read(key: "token");
    var email = await storage.read(key: "token_sub");

    // print(token);
    var url = "https://10.6.88.55:8084/api/sys/bankId_enquiry";
    var requestBody = {};
    HttpClient httpClient = new HttpClient()
      ..badCertificateCallback =
          ((X509Certificate cert, String host, int port) => true);
    HttpClientRequest request = await httpClient.postUrl(Uri.parse(url));
    request.headers.set('Authorization', token);
    request.headers.set(HttpHeaders.contentTypeHeader, "application/json");
    request.write(jsonEncode(requestBody));
    HttpClientResponse response = await request.close();
    String reply = await response.transform(utf8.decoder).join();
    var body = json.decode(reply);
    print(body.runtimeType);
    print(body);
    // List filteredData =
    //     body.keys.where((key) => body[key]["moduleType"] != "AP").toList();
    // print(filteredData);

    setState(() {
      allBanks = body["bankIds"];
    });
  }

  List<DropdownMenuItem> generateItemList(List items) {
    List<DropdownMenuItem> dropdownItems = items
        .map((item) => DropdownMenuItem(
              value: item,
              child: Text(item),
              // onTap: () {
              //   print("DropdownMenuItem is onTap");
              // }
            ))
        .toList();
    // print(dropdownItems);
    return dropdownItems;
  }

  Future<void> _addAssociate(String bankId, String bankUserName) async {
    final storage = FlutterSecureStorage();
    String token = await storage.read(key: "token");
    // print(token);
    var url = "https://10.6.88.55:8084/api/sys/oauth2/bank";
    var requestBody = {
      "bankId": bankId,
      "bankUserName": bankUserName,
    };
    print("requestBody");
    print(requestBody);
    HttpClient httpClient = new HttpClient()
      ..badCertificateCallback =
          ((X509Certificate cert, String host, int port) => true);
    HttpClientRequest request = await httpClient.postUrl(Uri.parse(url));
    request.headers.set('Authorization', token);
    request.headers.set(HttpHeaders.contentTypeHeader, "application/json");
    request.write(jsonEncode(requestBody));
    HttpClientResponse response = await request.close();
    String reply = await response.transform(utf8.decoder).join();
    var body = json.decode(reply);
    // print(body.runtimeType);
    print(body);
    print("res - _addAssociate api/sys/oauth2/bank");
    print(reply);
    setState(() {
      authUrl = body["authUrl"];
    });
  }

  Future<bool> showAddNewDialog(BuildContext context) {
    return showDialog<bool>(
        context: context,
        builder: (context) {
          return AlertDialog(
            // title: Text("Associate with new bank"),
            content: Text("Associate with this bank?"),
            actions: <Widget>[
              FlatButton(
                child: Text("Confirm"),
                onPressed: () => Navigator.of(context).pop(true),
              ),
              FlatButton(
                child: Text("Later"),
                onPressed: () {
                  Navigator.of(context).pop(false);
                },
              ),
            ],
          );
        });
  }
}
