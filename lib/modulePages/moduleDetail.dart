import 'package:flutter/material.dart';
import 'dart:io';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'dart:convert';

void main() {
  runApp(MaterialApp(
    home: Scaffold(
        // appBar: AppBar(
        //   title: Text('Demo'),
        // ),
        body: ModuleDetail(
      module: Module(
          moduleName: "test1",
          settings: {"dummy": true},
          associatedBanks: {"bank1": false}),
    )),
  ));
}

class ModuleDetail extends StatefulWidget {
  final Module module;
  final VoidCallback toUpdateDrawer;

  ModuleDetail({Key key, @required this.module, this.toUpdateDrawer})
      : super(key: key);

  @override
  _ModuleDetailState createState() => _ModuleDetailState(module: module);
}

class _ModuleDetailState extends State<ModuleDetail> {
  Module module;
  bool isEditing = false;
  _ModuleDetailState({Key key, @required this.module}) : super();

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    // throw UnimplementedError();
    return new Scaffold(
        body: new ListView(
            // padding: EdgeInsets.only(left: 20, right: 20),
            children: <Widget>[
          Container(
              padding: const EdgeInsets.all(20),
              child: Text(
                module.moduleName,
                textAlign: TextAlign.left,
                style: new TextStyle(
                    fontWeight: FontWeight.w400,
                    letterSpacing: 0.5,
                    color: Colors.black,
                    fontSize: 26.0),
              )),
          Container(
            margin: EdgeInsets.only(left: 20, right: 20),
            height: 600,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(12.0),
              color: Colors.white,
            ),
            child: _buildListView(),
          )
        ]));
  }

  ListView _buildListView() {
    print("run _buildListView");
    // print(this.module.settings);
    // print(this.module.associatedBanks);
    var settingKeys = module.settings.keys.toList();
    settingKeys.sort();
    var bankKeys = [];
    if (module.associatedBanks != null) {
      bankKeys = module.associatedBanks.keys.toList();
      bankKeys.sort();
    }

    return new ListView(children: <Widget>[
      Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Container(
              padding: EdgeInsets.only(top: 10.0),
              child: FlatButton(
                onPressed: () => Navigator.of(context).pop(),
                child: Text(
                  "<  Cancel",
                ),
              )),
          Container(
            padding: EdgeInsets.only(top: 10.0),
            // child: Text(
            //   "Edit",
            // ),
            child: FlatButton(
              onPressed: () {
                print("Hello ! =======");
                // print(this.module.settings);
                // print(this.module.associatedBanks);
                setState(() {
                  isEditing = !isEditing;
                });
                _editModule(this.module);
              },
              child: Text(
                isEditing ? "Save" : "Edit",
              ),
            ),
          ),
        ],
      ),
      Container(
          padding: EdgeInsets.only(top: 40, bottom: 10, left: 10),
          child: Text(
            "Setting",
            style: new TextStyle(
                fontWeight: FontWeight.w400,
                letterSpacing: 0.5,
                color: Colors.black,
                fontSize: 20.0),
          )),
      Divider(color: Colors.black54, height: 0),
      ...settingKeys.map((e) {
        // print(e == module.settings.keys.toList()[0]);
        return Column(
          children: <Widget>[
            ListTile(
                title: Text(e),
                trailing: _buildSwitchArea(
                    "setting", e, module.settings[e]["enabled"])),
            Divider(color: Colors.black54, height: 0),
          ],
        );
      }).toList(),
      Container(
          padding: EdgeInsets.only(top: 40, bottom: 10, left: 10),
          child: Text(
            "Banks",
            style: new TextStyle(
                fontWeight: FontWeight.w400,
                letterSpacing: 0.5,
                color: Colors.black,
                fontSize: 20.0),
          )),
      Divider(color: Colors.black54, height: 0),
      ...module.associatedBanks != null
          ? bankKeys.map((e) {
              // print(e == module.settings.keys.toList()[0]);
              return Column(
                children: <Widget>[
                  ListTile(
                      title: Text(e),
                      trailing: _buildSwitchArea(
                          "banks", e, module.associatedBanks[e])),
                  Divider(color: Colors.black54, height: 0),
                ],
              );
            }).toList()
          : [Container()],
      Container(
        margin: EdgeInsets.only(top: 20, bottom: 20),
        height: 50.0,
        child: RaisedButton(
          color: Colors.red,
          onPressed: () async {
            bool enroll = await showEnrollNewDialog(context);
            if (enroll == true) {
              print("selected Confirm");
              // var a = await _enrollNewModule(this.selectedValue);
              // print(a);
              // await _getMyModules();
              await _deleteModule(this.module);
              Navigator.of(context).pop(true);
            } else {
              print("selected Later");
            }
          },

          // await _deleteModule(this.module);
          // Navigator.of(context).pop(true);
          // setState(() {
          //   mainPage = false;
          //   signUp = false;
          // });
          // },
          shape:
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(10.0)),
          child: Container(
            alignment: Alignment.center,
            child: Text(
              "Delete",
              textAlign: TextAlign.center,
              style: TextStyle(color: Colors.white, fontSize: 18.0),
            ),
          ),
          // ),
        ),
      ),
    ]);
  }

  Widget _buildSwitchArea(type, id, switchValue) {
    // print(id);
    if (this.isEditing && type == "setting") {
      return Switch(
        value: switchValue,
        onChanged: (value) {
          setState(() {
            // isTrue = value;
            type == "setting"
                ? this.module.settings[id]["enabled"] = value
                : this.module.associatedBanks[id]["enabled"] = value;
            // print(switchValue);
          });
        },
        // activeTrackColor: Colors.lightGreenAccent,
        // activeColor: Colors.green,
      );
    } else {
      if (switchValue) {
        return type == "setting" ? Text("On") : Text("Associated");
      } else
        return type == "setting" ? Text("Off") : Text("Not associated");
    }
  }

  Future<void> _editModule(Module module) async {
    final storage = FlutterSecureStorage();
    String token = await storage.read(key: "token");
    // print(token);
    var url = "https://10.6.88.55:8084/api/user/account/edit";
    var requestBody = {
      "modules": {
        module.moduleName: {
          "settings": module.settings,
          // {
          // "dummy": {"enabled": false},
          // "moneyTransfer": {"enabled": false},
          // "recommendCreditCard": {"enabled": false},
          // "productEnquiry": {"enabled": false},
          // "creditScoringEngine": {"enabled": false}

          // },
          // "enabled": true
        }
      }
    };
    print("requestBody");
    print(requestBody);
    HttpClient httpClient = new HttpClient()
      ..badCertificateCallback =
          ((X509Certificate cert, String host, int port) => true);
    HttpClientRequest request = await httpClient.postUrl(Uri.parse(url));
    request.headers.set('Authorization', token);
    request.headers.set(HttpHeaders.contentTypeHeader, "application/json");
    request.write(jsonEncode(requestBody));
    HttpClientResponse response = await request.close();
    String reply = await response.transform(utf8.decoder).join();
    // var body = json.decode(reply);
    // print(body.runtimeType);
    // print(body);
    print("res - _editModule api/user/account/edit");
    print(reply);

    widget.toUpdateDrawer();
  }

  Future<void> _deleteModule(Module module) async {
    final storage = FlutterSecureStorage();
    String token = await storage.read(key: "token");
    // print(token);
    var url = "https://10.6.88.55:8084/api/sys/modules/disenroll";
    var requestBody = {module.moduleName: {}};
    print("requestBody");
    print(requestBody);
    HttpClient httpClient = new HttpClient()
      ..badCertificateCallback =
          ((X509Certificate cert, String host, int port) => true);
    HttpClientRequest request = await httpClient.deleteUrl(Uri.parse(url));
    request.headers.set('Authorization', token);
    request.headers.set(HttpHeaders.contentTypeHeader, "application/json");
    request.write(jsonEncode(requestBody));
    HttpClientResponse response = await request.close();
    String reply = await response.transform(utf8.decoder).join();
    // var body = json.decode(reply);
    // print(body.runtimeType);
    // print(body);
    print("res - _deleteModule api/sys/modules/disenroll");
    print(reply);

    widget.toUpdateDrawer();
  }

  Future<bool> showEnrollNewDialog(BuildContext context) {
    return showDialog<bool>(
        context: context,
        builder: (context) {
          return AlertDialog(
            title: Text("Delete module"),
            // content: Text("show content"),
            content: Text("Delete this module?"),
            actions: <Widget>[
              FlatButton(
                child: Text("Confirm"),
                onPressed: () => Navigator.of(context).pop(true),
              ),
              FlatButton(
                child: Text("Later"),
                onPressed: () {
                  Navigator.of(context).pop(false);
                },
              ),
            ],
          );
        });
  }
}

class Module {
  final String moduleName;
  final Map<String, dynamic> settings;
  final Map<String, dynamic> associatedBanks;
  // Module._({this.bankName, this.cardName, this.interestRate});

  Module._({this.moduleName, this.settings, this.associatedBanks});
  // Module(this.title, this.link);
  Module({this.moduleName, this.settings, this.associatedBanks});

  factory Module.fromJson(Map<String, dynamic> json) {
    return new Module._(
        moduleName: json['moduleId'],
        settings: json['settings'],
        associatedBanks: json["associatedBanks"]);
  }
}
