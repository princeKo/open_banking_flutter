import 'package:flutter/material.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'dart:io';
import 'dart:convert';

import 'moduleDetail.dart';
import './enrollModule.dart';

void main() {
  runApp(MaterialApp(
    home: Scaffold(
        // appBar: AppBar(
        //   title: Text('Demo'),
        // ),
        body: ModulePage()),
  ));
}

class ModulePage extends StatefulWidget {
  final VoidCallback toUpdateDrawer;
  ModulePage({Key key, this.toUpdateDrawer}) : super(key: key);

  @override
  _ModulePageState createState() => _ModulePageState();
}

class _ModulePageState extends State<ModulePage> {
  final storage = FlutterSecureStorage();
  String token = '';
  List allModules = [];
  Map<String, dynamic> allModules_obj;
  Map<String, dynamic> myModules = {};
  List myModulesName = [];
  List myAssociatedBankIds = [];
  bool myAssociatedBankIdsIsLoaded = false;
  var selectedValue;

  @override
  void initState() {
    super.initState();
    _getToken();
    _getMyModules();
    _getAllModules();
    _getMyAssociatedBanks();
  }

  @override
  Widget build(BuildContext context) {
    print("this.selectedValue");
    print(this.selectedValue);
    return new Scaffold(
        body: new ListView(
            padding: const EdgeInsets.only(left: 20, right: 20),
            children: <Widget>[
          Container(
              padding: const EdgeInsets.only(bottom: 20),
              child: Text(
                "Module",
                textAlign: TextAlign.left,
                style: new TextStyle(
                    fontWeight: FontWeight.w400,
                    letterSpacing: 0.5,
                    color: Colors.black,
                    fontSize: 26.0),
              )),
          Container(
              // margin: const EdgeInsets.all(20),
              height: 60.0,
              width: 140.0,
              color: Colors.white,
              child:
                  // Container(
                  //     decoration: BoxDecoration(
                  //       borderRadius: BorderRadius.circular(12.0),
                  //       // color: Colors.white,
                  //     ),
                  //     padding: EdgeInsets.only(left: 10, right: 20),
                  //     child:
                  FlatButton(
                onPressed: () {
                  print("Tapped on FlatButton");
                  // print(myModules);
                  Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => EnrollModulePage(
                                    toUpdateDrawer: widget.toUpdateDrawer,
                                  )))
                      .then((val) => val == true ? _getMyModules() : "");
                },
                // onPressed: () async {
                //   bool enroll = await showEnrollNewDialog(context);
                //   if (enroll == true && selectedValue != null) {
                //     print("selected Confirm");
                //     var a = await _enrollNewModule(selectedValue);
                //     print(a);
                //     await _getMyModules();
                //   } else {
                //     print("selected Later");
                //   }
                // },
                // onPressed: () {
                //   showEnrollNewDialog(context).then((value) => {
                //         if (value == true && selectedValue != null)
                //           {
                //             print("selected Confirm"),
                //             _enrollNewModule(selectedValue),
                //             // print(a),
                //             _getMyModules(),
                //           }
                //         else
                //           {print("selected Later")}
                //       });
                // },
                // onTap: () {
                //   showEnrollNewDialog(context);
                // },
                child: Row(
                  // mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Container(
                      margin: const EdgeInsets.only(left: 10, right: 20),
                      child: Icon(Icons.add_circle, color: Color(0xffff3783)),
                    ),
                    Text("Enroll a new module"),
                  ],
                ),
                // alignment: Alignment.center,
              )),
          Container(
            // padding: EdgeInsets.only(left: 20, right: 20),
            height: 500.0,
            // width: 140.0,
            child: _buildListView(),
            alignment: Alignment.center,
          ),
        ]));
  }

  Future<Null> _getToken() async {
    String token = await storage.read(key: "token");
    // print(token);
    setState(() {
      this.token = token;
    });
  }

  Future<Null> _getAllModules() async {
    String token = await storage.read(key: "token");
    // print(token);
    var url = "https://10.6.88.55:8084/api/sys/modules/list";
    HttpClient httpClient = new HttpClient()
      ..badCertificateCallback =
          ((X509Certificate cert, String host, int port) => true);
    HttpClientRequest request = await httpClient.postUrl(Uri.parse(url));
    request.headers.set('Authorization', token);
    request.headers.set(HttpHeaders.contentTypeHeader, "application/json");
    // request.write(jsonEncode(requestBody));
    HttpClientResponse response = await request.close();
    String reply = await response.transform(utf8.decoder).join();
    var body = json.decode(reply);
    print("res - _getAllModules api/sys/modules/list");
    print(body);
    List filteredData =
        body.keys.where((key) => body[key]["moduleType"] != "TRANS").toList();
    print(filteredData);

    setState(() {
      allModules = filteredData;
      allModules_obj = body;
    });
  }

  Future<String> _enrollNewModule(moduleName) async {
    String token = await storage.read(key: "token");
    // print(token);
    var url = "https://10.6.88.55:8084/api/sys/modules/enroll";
    var requestBody = {
      moduleName: {
        "settings": {
          "dummy": {"enabled": false},
          "moneyTransfer": {"enabled": false},
          "recommendCreditCard": {"enabled": false},
          "productEnquiry": {"enabled": false},
          "creditScoringEngine": {"enabled": false}
        },
        "enabled": true
      }
    };
    print(requestBody);
    HttpClient httpClient = new HttpClient()
      ..badCertificateCallback =
          ((X509Certificate cert, String host, int port) => true);
    HttpClientRequest request = await httpClient.postUrl(Uri.parse(url));
    request.headers.set('Authorization', token);
    request.headers.set(HttpHeaders.contentTypeHeader, "application/json");
    request.write(jsonEncode(requestBody));
    HttpClientResponse response = await request.close();
    String reply = await response.transform(utf8.decoder).join();
    // var body = json.decode(reply);
    // print(body.runtimeType);
    // print(body);
    print("res - _enrollNewModule api/sys/modules/enroll");
    print(reply);
    return "end of _enrollNewModule";
  }

  Future<Null> _getMyModules() async {
    String token = await storage.read(key: "token");
    // print(token);
    String email = await storage.read(key: "token_sub");
    var url = "https://10.6.88.55:8084/api/user/account/list";

    // var apiUrl = 'api/user/account/list';

    // var token = accessCookie("A_Token");
    // let email = getUserID(token);

    // var requestBody = {email: {}};
    var requestBody = {};
    HttpClient httpClient = new HttpClient()
      ..badCertificateCallback =
          ((X509Certificate cert, String host, int port) => true);
    HttpClientRequest request = await httpClient.postUrl(Uri.parse(url));
    request.headers.set('Authorization', token);
    request.headers.set(HttpHeaders.contentTypeHeader, "application/json");
    request.write(jsonEncode(requestBody));
    HttpClientResponse response = await request.close();
    String reply = await response.transform(utf8.decoder).join();
    var body = json.decode(reply);
    print("res - _getMyModules api/user/account/list");
    print(body);
    var modules = body[email]["modules"];
    var moduleNames;
    if (modules == null) {
      moduleNames = [];
    } else {
      modules.keys.forEach((e) {
        modules[e]["moduleId"] = e;
      });
      moduleNames = modules.keys.toList();
    }
    print(modules);
    print(moduleNames);

    setState(() {
      myModulesName = moduleNames;
      myModules = modules;
    });
  }

  Future<void> _getMyAssociatedBanks() async {
    String token = await storage.read(key: "token");
    // print(token);
    String email = await storage.read(key: "token_sub");
    var url = "https://10.6.88.55:8084/api/sys/tx/accounts/list";

    var requestBody = {};
    HttpClient httpClient = new HttpClient()
      ..badCertificateCallback =
          ((X509Certificate cert, String host, int port) => true);
    HttpClientRequest request = await httpClient.postUrl(Uri.parse(url));
    request.headers.set('Authorization', token);
    request.headers.set(HttpHeaders.contentTypeHeader, "application/json");
    request.write(jsonEncode(requestBody));
    HttpClientResponse response = await request.close();
    String reply = await response.transform(utf8.decoder).join();
    var body = json.decode(reply);
    print("res - _getMyAssociatedBanks api/sys/tx/accounts/list");
    print(body);
    var myAssociatedBankIds = body.keys.toList();
    setState(() {
      this.myAssociatedBankIds = myAssociatedBankIds;
      this.myAssociatedBankIdsIsLoaded = true;
    });

    // var modules = body[email]["modules"];
    // var moduleNames;
    // if (modules == null) {
    //   moduleNames = [];
    // } else {
    //   modules.keys.forEach((e) {
    //     modules[e]["moduleId"] = e;
    //   });
    //   moduleNames = modules.keys.toList();
    // }
    // print(modules);
    // print(moduleNames);

    // setState(() {
    //   myModulesName = moduleNames;
    //   myModules = modules;
    // });
  }

  Widget _buildListView() {
    print("myModulesName: " + this.myModulesName.toString());
    print("myAssociatedBankIds: " + this.myAssociatedBankIds.toString());
    var myAssociatedBankIds = this.myAssociatedBankIds;
    var allModules = this.allModules_obj;
    var myModules_original = this.myModules;
    var myModulesName = this.myModulesName;
    var myModules_transformed = myModules_original;
    if (myModulesName.length > 0 &&
        this.allModules_obj.keys.length > 0 &&
        this.myAssociatedBankIdsIsLoaded == true) {
      myModulesName.forEach((moduleId) {
        var associatedWithModules =
            allModules[moduleId]["associatedWith"].keys.toList();

        var associatedWithBanks = [];
        associatedWithModules.forEach((moduleId) {
          var banks = allModules[moduleId]["associatedWith"].keys.map((bankId) {
            return {bankId: allModules[moduleId]["associatedWith"][bankId]};
          });

          associatedWithBanks.addAll(banks);
        });
        associatedWithBanks.toSet().toList();

        Map<String, dynamic> isBankAssociated = {};

        associatedWithBanks.forEach((bank) {
          // print("HI!!!");
          // print(myAssociatedBankIds);
          // print(bank);
          // print((bank.keys.toList()[0]));
          // print(myAssociatedBankIds.contains((bank.keys.toList()[0])));
          if (myAssociatedBankIds.contains(bank.keys.toList()[0])) {
            isBankAssociated[bank[bank.keys.toList()[0]]["bankName"]] = true;
          } else {
            isBankAssociated[bank[bank.keys.toList()[0]]["bankName"]] = false;
          }
        });

        myModules_transformed[moduleId]["associatedBanks"] = isBankAssociated;
      });
      print("myModules_transformed");
      print(myModules_transformed);

      return new ListView(
          children:
              // myModulesName.length > 0?
              myModulesName
                  .map((e) => Container(
                      margin: const EdgeInsets.only(
                        bottom: 10,
                      ),
                      // decoration: BoxDecoration(
                      //   borderRadius: BorderRadius.circular(12.0),
                      //   color: Colors.red[50],
                      // ),
                      height: 60.0,
                      child: FlatButton(
                          shape: new RoundedRectangleBorder(
                              borderRadius: new BorderRadius.circular(10.0)),
                          color: Colors.white,
                          onPressed: () {
                            // Navigator.of(context)
                            //     .push(new MaterialPageRoute(builder: (context) {
                            //   return new Text("Hi KaMu");
                            // }));
                            print("Tapped on FlatButton");
                            // print(myModules);
                            // print(e);
                            // print(myModules[e]);
                            // print(myModules_transformed[e]);
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => ModuleDetail(
                                          module: Module.fromJson(
                                              myModules_transformed[e]),
                                          toUpdateDrawer: widget.toUpdateDrawer,
                                        ))).then(
                                (val) => val == true ? _getMyModules() : "");
                          },
                          child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: <Widget>[
                                Text(e),
                                Text(">"),
                              ]))))
                  .toList());
    } else {
      return Container();
    }

    // myModulesName = [
    //   ...myModulesName,
    //   "fake1",
    //   "fake2",
    //   "fake3",
    //   "fake4",
    //   "fake5",
    //   "fake6",
    //   "fake7",
    //   "fake8",
    //   "fake9",
    //   "fake10"
    // ];
  }

  Future<bool> showEnrollNewDialog(BuildContext context) {
    return showDialog<bool>(
        context: context,
        builder: (context) {
          return AlertDialog(
            title: Text("Enroll module"),
            // content: Text("show content"),
            content: getDropList(context),
            actions: <Widget>[
              FlatButton(
                child: Text("Confirm"),
                onPressed: () => Navigator.of(context).pop(true),
              ),
              FlatButton(
                child: Text("Later"),
                onPressed: () {
                  Navigator.of(context).pop(false);
                },
              ),
            ],
          );
        });
  }

  DropdownButton getDropList(BuildContext context) {
    final List<DropdownMenuItem> items = this
        .allModules
        .map((module) => DropdownMenuItem(
              value: module,
              child: Text(module),
            ))
        .toList();
    return DropdownButton(
      items: items,
      hint: Text("choose an item"),
      value: this.selectedValue,
      onChanged: (value) {
        print("onChanged");
        print(value);
        print(this.selectedValue);
        setState(() {
          this.selectedValue = value;
          Navigator.of(context).pop();
          showEnrollNewDialog(context);
        });
      },
      onTap: () {
        print("onTap");
      },
      // isDense: true,
      isExpanded: true,
    );
    // return items;
  }
}

// class Module {
//   final String moduleName;
//   final Map<String, bool> settings;
//   final Map<String, bool> associatedBanks;

//   Module._({this.moduleName, this.settings, this.associatedBanks});

//   factory Module.fromJson(Map<String, dynamic> json) {
//     return new Module._(
//         moduleName: json['title'],
//         settings: json['docid'],
//         associatedBanks: json["rate"]);
//   }
// }
