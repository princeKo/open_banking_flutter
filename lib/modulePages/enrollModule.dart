import 'package:flutter/material.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'dart:io';
import 'dart:convert';

// import 'moduleDetail.dart';

class EnrollModulePage extends StatefulWidget {
  final VoidCallback toUpdateDrawer;
  EnrollModulePage({Key key, this.toUpdateDrawer}) : super(key: key);

  @override
  _EnrollModulePageState createState() => _EnrollModulePageState();
}

class _EnrollModulePageState extends State<EnrollModulePage> {
  final storage = FlutterSecureStorage();
  String token = '';
  List allModules = [];
  Map<String, dynamic> myModules = {};
  List myModulesName = [];
  var selectedValue;

  @override
  void initState() {
    super.initState();
    // _getToken();
    // _getMyModules();
    _getAllModules();
  }

  @override
  Widget build(BuildContext context) {
    // print("this.selectedValue");
    // print(this.selectedValue);
    return new Scaffold(
        appBar: AppBar(
          title: Text("Avaliable modules"),
        ),
        body: SafeArea(
            child: Container(
                // height: 500,
                padding: const EdgeInsets.only(left: 20, right: 20, top: 20),
                child: _buildListView())));
  }

  Future<void> _getAllModules() async {
    String token = await storage.read(key: "token");
    // print(token);
    var url = "https://10.6.88.55:8084/api/sys/modules/list";
    HttpClient httpClient = new HttpClient()
      ..badCertificateCallback =
          ((X509Certificate cert, String host, int port) => true);
    HttpClientRequest request = await httpClient.postUrl(Uri.parse(url));
    request.headers.set('Authorization', token);
    request.headers.set(HttpHeaders.contentTypeHeader, "application/json");
    // request.write(jsonEncode(requestBody));
    HttpClientResponse response = await request.close();
    String reply = await response.transform(utf8.decoder).join();
    var body = json.decode(reply);
    print("res - _getAllModules api/sys/modules/list");
    print(body);
    List filteredData =
        body.keys.where((key) => body[key]["moduleType"] != "TRANS").toList();
    print("filteredData");
    print(filteredData);

    setState(() {
      allModules = filteredData;
    });
  }

  Future<void> _enrollNewModule(moduleName) async {
    String token = await storage.read(key: "token");
    // print(token);
    var url = "https://10.6.88.55:8084/api/sys/modules/enroll";
    var requestBody = {
      moduleName: {
        "settings": {
          "dummy": {"enabled": false},
          "moneyTransfer": {"enabled": false},
          "recommendCreditCard": {"enabled": false},
          "productEnquiry": {"enabled": false},
          "creditScoringEngine": {"enabled": false}
        },
        "enabled": true
      }
    };
    print("requestBody");
    print(requestBody);
    HttpClient httpClient = new HttpClient()
      ..badCertificateCallback =
          ((X509Certificate cert, String host, int port) => true);
    HttpClientRequest request = await httpClient.postUrl(Uri.parse(url));
    request.headers.set('Authorization', token);
    request.headers.set(HttpHeaders.contentTypeHeader, "application/json");
    request.write(jsonEncode(requestBody));
    HttpClientResponse response = await request.close();
    String reply = await response.transform(utf8.decoder).join();
    // var body = json.decode(reply);
    // print(body.runtimeType);
    // print(body);
    print("res - _enrollNewModule api/sys/modules/enroll");
    print(reply);
    // return "end of _enrollNewModule";

    widget.toUpdateDrawer();
  }

  ListView _buildListView() {
    print("myModulesName: " + this.allModules.toString());
    var allModules = this.allModules;
    // allModules = [
    //   ...allModules,
    //   "fake1",
    //   "fake2",
    //   "fake3",
    //   "fake4",
    //   "fake5",
    //   "fake6",
    //   "fake7",
    //   "fake8",
    //   "fake9",
    //   "fake10"
    // ];
    return new ListView(
        children: allModules.length > 0
            ? allModules
                .map((e) => Container(
                    margin: const EdgeInsets.only(
                      bottom: 10,
                    ),
                    // decoration: BoxDecoration(
                    //   borderRadius: BorderRadius.circular(12.0),
                    //   color: Colors.red[50],
                    // ),
                    height: 60.0,
                    child: FlatButton(
                        shape: new RoundedRectangleBorder(
                            borderRadius: new BorderRadius.circular(10.0)),
                        color: Colors.white,
                        onPressed: () async {
                          // Navigator.of(context)
                          //     .push(new MaterialPageRoute(builder: (context) {
                          //   return new Text("Hi KaMu");
                          // }));
                          print("Tapped on FlatButton");

                          var res = await showDialog<bool>(
                              context: context,
                              builder: (context) {
                                return AlertDialog(
                                  title: Text("Enroll module"),
                                  // content: Text("show content"),
                                  content: Text("Confirm to enroll " + e),
                                  actions: <Widget>[
                                    FlatButton(
                                      child: Text("Confirm"),
                                      onPressed: () =>
                                          Navigator.of(context).pop(true),
                                    ),
                                    FlatButton(
                                      child: Text("Cancel"),
                                      onPressed: () {
                                        Navigator.of(context).pop(false);
                                      },
                                    ),
                                  ],
                                );
                              });
                          if (res == true) {
                            await _enrollNewModule(e);
                            Navigator.of(context).pop(true);
                          }
                        },
                        child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              Text(e),
                              Text(">"),
                            ]))))
                .toList()
            : [
                Container(
                    // height: 300,
                    // alignment: Alignment.center,
                    // child: Text("No module found..")
                    )
              ]);
  }
}
