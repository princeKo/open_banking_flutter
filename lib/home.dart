import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'dart:convert';
import 'dart:io';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import './moduleSummaryWebView.dart';

void main() {
  runApp(MaterialApp(
    theme: ThemeData(
      primarySwatch: Colors.pink,
    ),
    home: Scaffold(
      body: HomePage(),
    ),
  ));
}

class HomePage extends StatefulWidget {
  final myModules;
  HomePage({Key key, this.myModules}) : super(key: key);

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  final storage = FlutterSecureStorage();

  Map<String, dynamic> myModules = {};
  List myModulesName = [];
  String selectedModule;
  String token;

  @override
  void initState() {
    super.initState();
    // _getToken();
    // // _getMyModules();
    // myModules = widget.myModules;
    // var moduleNames;
    // if (myModules == null) {
    //   moduleNames = [];
    // } else {
    //   myModules.keys.forEach((e) {
    //     myModules[e]["moduleId"] = e;
    //   });
    //   moduleNames = myModules.keys.toList();
    // }
    // print("myModules ==== > >  >> ");

    // print(myModules);
    // print(moduleNames);
    // var defaultModule;
    // if (moduleNames.length > 0) {
    //   defaultModule = moduleNames[0];
    // }
    // selectedModule = defaultModule;
    prepareState();
  }

  void prepareState() async {
    await _getToken();
  }

  @override
  Widget build(BuildContext context) {
    // print("this.token");
    // print(this.token);

    myModules = widget.myModules;
    var moduleNames;
    if (myModules == null) {
      moduleNames = [];
    } else {
      myModules.keys.forEach((e) {
        myModules[e]["moduleId"] = e;
      });
      moduleNames = myModules.keys.toList();
    }
    print("myModules ==== > >  >> ");
    print(myModules);
    myModulesName = moduleNames;
    print(myModulesName);
    var defaultModule;
    if (moduleNames.length > 0) {
      defaultModule = moduleNames[0];
    }
    selectedModule = defaultModule;

    var paramString = "";
    var settings;
    if (this.myModules.keys.length > 0 &&
        this.myModules[selectedModule] != null) {
      settings = this.myModules[selectedModule]["settings"];
    }
    print(settings);
    if (settings != null && settings.keys.length > 0) {
      for (var i = 0; i < settings.keys.length; i++) {
        var symbol;
        if (i == 0) {
          symbol = "?";
        } else {
          symbol = "&";
        }
        paramString = paramString +
            symbol +
            settings.keys.toList()[i] +
            "=" +
            settings[settings.keys.toList()[i]]["enabled"].toString();
      }
    }

    print("selectedModule");
    print(selectedModule);

    return Scaffold(
      body:
          // new ListView(
          //     padding: EdgeInsets.only(left: 20, right: 20),
          Container(
              padding: EdgeInsets.only(left: 20, right: 20),
              child: Column(children: <Widget>[
                Container(
                    alignment: Alignment.centerLeft,
                    margin: EdgeInsets.only(bottom: 20),
                    child: Text(
                      "Home",
                      textAlign: TextAlign.left,
                      style: new TextStyle(
                          fontWeight: FontWeight.w400,
                          letterSpacing: 0.5,
                          color: Colors.black,
                          fontSize: 30.0),
                    )),
                Container(
                  child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Container(
                          // margin: EdgeInsets.only(left: 10),
                          child: Text(
                            "Module",
                            style: new TextStyle(
                                fontWeight: FontWeight.w400,
                                letterSpacing: 0.5,
                                color: Colors.black,
                                fontSize: 22.0),
                          ),
                        ),
                        Container(
                          margin: EdgeInsets.only(right: 10),
                          width: 180,
                          child:
                              //     ButtonBar(
                              //   alignment: MainAxisAlignment.center,
                              //   children: <Widget>[
                              //     DropdownButton(
                              //       value: selectedBank,
                              //       items: generateItemList(),
                              //       hint: Text("choose a bank"),
                              //       onChanged: (value) {
                              //         setState(() => selectedBank = value);
                              //       },
                              //       onTap: () {
                              //         print("onTap");
                              //       },
                              //     )
                              //   ],
                              // )
                              DropdownButtonHideUnderline(
                            child: DropdownButton(
                              // items: generateItemList(this.allBanks.keys.toList()),
                              items: generateItemList(this.myModulesName),
                              hint: Text("choose a module"),
                              value: this.selectedModule,
                              onChanged: (value) {
                                setState(() {
                                  selectedModule = value;
                                  // Navigator.of(context).pop();
                                  // showEnrollNewDialog(context);
                                });
                              },
                              isExpanded: true,
                            ),
                          ),
                        )
                      ]),
                ),
                selectedModule != null
                    ? Expanded(
                        // height: 300,
                        child: InAppWebViewExampleScreen(
                        service: new Service(
                          // "Test1",
                          selectedModule,
                          // this.myModules[selectedModule]["url"] + "/home" + paramString,
                          "http://10.6.88.98:3001/home" + paramString,
                          // "https://10.6.88.98:3000/",

                          {
                            "A_Token": {
                              // "url": this.myModules[selectedModule]["url"] + "/home" + paramString,
                              "url":
                                  "http://10.6.88.98:3001/home" + paramString,
                              "name": "A_Token",
                              "value": this.token
                              // "value": "token"
                            },
                          },
                        ),
                      ))
                    : Container(
                        height: 200,
                        alignment: Alignment.center,
                        child: Text("No module enrolled..."),
                      ),
              ])),
    );
  }

  Future<String> _getToken() async {
    String token = await storage.read(key: "token");
    setState(() {
      this.token = token;
    });
    return token;
  }

  Future<Null> _getMyModules() async {
    String token = await storage.read(key: "token");
    // print(token);

    // setState(
    //   () {
    //     myModules = {
    //       "test1": {
    //         "settings": {
    //           "dummy": {"enabled": false},
    //           "moneyTransfer": {"enabled": true},
    //           "recommendCreditCard": {"enabled": true},
    //           "productEnquiry": {"enabled": false},
    //           "creditScoringEngine": {"enabled": true}
    //         },
    //         "url": "https://openbanktest1:8080",
    //         "enabled": true
    //       }
    //     };
    //     myModulesName = ["test1"];
    //   },
    // );

    String email = await storage.read(key: "token_sub");
    var url = "https://10.6.88.55:8084/api/user/account/list";
    var requestBody = {};
    HttpClient httpClient = new HttpClient()
      ..badCertificateCallback =
          ((X509Certificate cert, String host, int port) => true);
    HttpClientRequest request = await httpClient.postUrl(Uri.parse(url));
    request.headers.set('Authorization', token);
    request.headers.set(HttpHeaders.contentTypeHeader, "application/json");
    request.write(jsonEncode(requestBody));
    HttpClientResponse response = await request.close();
    String reply = await response.transform(utf8.decoder).join();
    var body = json.decode(reply);
    print("res - _getMyModules api/user/account/list");
    print(body);
    var modules = body[email]["modules"];
    var moduleNames;
    if (modules == null) {
      moduleNames = [];
    } else {
      modules.keys.forEach((e) {
        modules[e]["moduleId"] = e;
      });
      moduleNames = modules.keys.toList();
    }
    // print(modules);
    // print(moduleNames);
    var defaultModule;
    if (moduleNames.length > 0) {
      defaultModule = moduleNames[0];
    }

    setState(() {
      myModulesName = moduleNames;
      myModules = modules;
      // selectedModule = moduleNames[0];
      selectedModule = defaultModule;
    });
  }

  List<DropdownMenuItem> generateItemList(List items) {
    List<DropdownMenuItem> dropdownItems = items
        .map((item) => DropdownMenuItem(
              value: item,
              child: Text(item),
              // onTap: () {
              //   print("DropdownMenuItem is onTap");
              // }
            ))
        .toList();
    // print(dropdownItems);
    return dropdownItems;
  }
}
