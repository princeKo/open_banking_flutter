import 'dart:ffi';

import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'dart:convert';
import 'dart:io';
// import 'package:flutter/cupertino.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import './profile/profile.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';

void main() {
  runApp(HomePage());
}

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  final storage = FlutterSecureStorage();

  // Data _data = new Data();
  List<CardInterest> dataList = [
    // new CardInterest._(bankName: "bank2", cardName: "Card1", interestRate: 0.3),
    // new CardInterest._(bankName: "bank3", cardName: "Card1", interestRate: 0.1),
    // new CardInterest._(bankName: "bank3", cardName: "Card1", interestRate: 0.5),
    // new CardInterest._(
    //     bankName: "BoC", cardName: "Card Abc", interestRate: 0.3),
    // new CardInterest._(
    //     bankName: "HSBC", cardName: "Card wow", interestRate: 0.5),
  ];
  int updateTime;
  Map<String, dynamic> myModules = {};
  List myModulesName = [];
  String selectedModule;

  final GlobalKey _scanffoldKey = new GlobalKey();

  @override
  void initState() {
    super.initState();
    _getMyModules();
  }

  @override
  Widget build(BuildContext context) {
    // print(this.updateTime);
    return MaterialApp(
        // title: 'Flutter Demo',
        theme: ThemeData(
          primarySwatch: Colors.pink,
          visualDensity: VisualDensity.adaptivePlatformDensity,
        ),
        // home: MyHomePage(title: 'Flutter Demo Home Page'),
        // home: WebViewExample(service: new Service("ABc title", "https://www.google.com")),
        home: Builder(
            builder: (context) => Scaffold(
                  key: _scanffoldKey,
                  backgroundColor: Color(0xfff3f4f4),
                  // appBar: AppBar(
                  //   backgroundColor: Color(0xfff3f4f4),
                  //   // title: Text("title"),
                  // ),
                  drawer: Drawer(
                    // Add a ListView to the drawer. This ensures the user can scroll
                    // through the options in the drawer if there isn't enough vertical
                    // space to fit everything.
                    child: ListView(
                      // Important: Remove any padding from the ListView.
                      padding: EdgeInsets.zero,
                      children: <Widget>[
                        DrawerHeader(
                          child: Text('Drawer Header'),
                          decoration: BoxDecoration(
                            color: Colors.blue,
                          ),
                        ),
                        ListTile(
                          title: Text('Item 1'),
                          onTap: () {
                            // Update the state of the app
                            // ...
                            // Then close the drawer
                            Navigator.pop(context);
                          },
                        ),
                        ListTile(
                          title: Text('Item 2'),
                          onTap: () {
                            // Update the state of the app
                            // ...
                            // Then close the drawer
                            Navigator.pop(context);
                          },
                        ),
                      ],
                    ),
                  ),
                  body: Container(
                    padding: EdgeInsets.only(top: 50, left: 20.0, right: 20.0),
                    child: Column(mainAxisSize: MainAxisSize.max, children: <
                        Widget>[
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          // Text(
                          //   "Home",
                          //   textAlign: TextAlign.left,
                          //   style: new TextStyle(
                          //       fontWeight: FontWeight.w400,
                          //       letterSpacing: 0.5,
                          //       color: Colors.black,
                          //       fontSize: 30.0),
                          // ),
                          IconButton(
                            iconSize: 40,
                            icon: Icon(Icons.menu),
                            // Image(
                            // // width: 50,
                            // // height: 50,
                            // image: AssetImage('images/profile.png')),
                            onPressed: () => Scaffold.of(context).openDrawer(),
                            // {
                            // _buildDrawer();
                            // _scanffoldKey.op
                            // Scaffold.of(context).openDrawer();
                            // Navigator.push(
                            //     context,
                            //     MaterialPageRoute(
                            //         builder: (context) => ProfilePage()));
                            // },
                          ),
                          IconButton(
                            iconSize: 60,
                            icon: Image(
                                // width: 50,
                                // height: 50,
                                image: AssetImage('images/profile.png')),
                            onPressed: () {
                              Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) => ProfilePage()));
                            },
                          ),
                        ],
                      ),
                      Row(
                        children: <Widget>[
                          Text(
                            "Home",
                            textAlign: TextAlign.left,
                            style: new TextStyle(
                                fontWeight: FontWeight.w400,
                                letterSpacing: 0.5,
                                color: Colors.black,
                                fontSize: 30.0),
                          )
                        ],
                      ),
                      Container(
                        margin: EdgeInsets.only(top: 20),
                        child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              Container(
                                margin: EdgeInsets.only(left: 10),
                                child: Text(
                                  "Module",
                                  style: new TextStyle(
                                      fontWeight: FontWeight.w400,
                                      letterSpacing: 0.5,
                                      color: Colors.black,
                                      fontSize: 22.0),
                                ),
                              ),
                              Container(
                                margin: EdgeInsets.only(right: 10),
                                width: 180,
                                child:
                                    //     ButtonBar(
                                    //   alignment: MainAxisAlignment.center,
                                    //   children: <Widget>[
                                    //     DropdownButton(
                                    //       value: selectedBank,
                                    //       items: generateItemList(),
                                    //       hint: Text("choose a bank"),
                                    //       onChanged: (value) {
                                    //         setState(() => selectedBank = value);
                                    //       },
                                    //       onTap: () {
                                    //         print("onTap");
                                    //       },
                                    //     )
                                    //   ],
                                    // )
                                    DropdownButtonHideUnderline(
                                  child: DropdownButton(
                                    // items: generateItemList(this.allBanks.keys.toList()),
                                    items: generateItemList(this.myModulesName),
                                    hint: Text("choose a module"),
                                    value: this.selectedModule,
                                    onChanged: (value) {
                                      setState(() {
                                        selectedModule = value;
                                        // Navigator.of(context).pop();
                                        // showEnrollNewDialog(context);
                                      });
                                    },
                                    isExpanded: true,
                                  ),
                                ),
                              )
                            ]),
                      ),
                      Container(
                        // margin: EdgeInsets.only(top: 20),
                        child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              Container(
                                margin: EdgeInsets.only(left: 10),
                                child: Text(
                                  "Balances",
                                  style: new TextStyle(
                                      fontWeight: FontWeight.w400,
                                      letterSpacing: 0.5,
                                      color: Colors.black,
                                      fontSize: 18.0),
                                ),
                              ),
                              Container(
                                  margin: EdgeInsets.only(right: 10),
                                  // width: 180,
                                  // height: 100,
                                  child: Switch(
                                    value: true,
                                    onChanged: (value) {
                                      setState(() {});
                                    },
                                  ))
                              // Text("abc"))
                            ]),
                      ),
                      Container(
                        child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              Container(
                                margin: EdgeInsets.only(left: 10),
                                child: Text(
                                  "Bank's name",
                                  style: new TextStyle(
                                    fontWeight: FontWeight.w400,
                                    letterSpacing: 0.5,
                                    color: Colors.black,
                                    // fontSize: 20.0,
                                  ),
                                ),
                              ),
                              Container(
                                margin: EdgeInsets.only(right: 10),
                                width: 180,
                                child:
                                    //     ButtonBar(
                                    //   alignment: MainAxisAlignment.center,
                                    //   children: <Widget>[
                                    //     DropdownButton(
                                    //       value: selectedBank,
                                    //       items: generateItemList(),
                                    //       hint: Text("choose a bank"),
                                    //       onChanged: (value) {
                                    //         setState(() => selectedBank = value);
                                    //       },
                                    //       onTap: () {
                                    //         print("onTap");
                                    //       },
                                    //     )
                                    //   ],
                                    // )
                                    DropdownButtonHideUnderline(
                                  child: DropdownButton(
                                    // items: generateItemList(this.allBanks.keys.toList()),
                                    items: [],
                                    hint: Text("choose a bank"),
                                    // value: this.selectedBank,
                                    onChanged: (value) {
                                      setState(() {
                                        // selectedBank = value;
                                        // Navigator.of(context).pop();
                                        // showEnrollNewDialog(context);
                                      });
                                    },
                                    isExpanded: true,
                                  ),
                                ),
                              )
                            ]),
                      ),
                      Container(
                        height: 150,
                        child: ListView(
                          // This next line does the trick.
                          scrollDirection: Axis.horizontal,
                          children: <Widget>[
                            Container(
                              margin: EdgeInsets.only(left: 10, right: 10),
                              decoration: BoxDecoration(
                                border: Border.all(color: Colors.black54),
                                borderRadius: BorderRadius.circular(12.0),
                                color: Colors.white,
                              ),
                              width: 200.0,
                              // color: Colors.red,
                            ),
                            Container(
                              margin: EdgeInsets.only(left: 10, right: 10),
                              decoration: BoxDecoration(
                                border: Border.all(color: Colors.black54),
                                borderRadius: BorderRadius.circular(12.0),
                                color: Colors.white,
                              ),
                              width: 200.0,
                            ),
                            Container(
                              width: 200.0,
                              color: Colors.green,
                            ),
                          ],
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.only(top: 20),
                        child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              Container(
                                margin: EdgeInsets.only(left: 10),
                                child: Text(
                                  "Product information",
                                  style: new TextStyle(
                                      fontWeight: FontWeight.w400,
                                      letterSpacing: 0.5,
                                      color: Colors.black,
                                      fontSize: 20.0),
                                ),
                              ),
                              Container(
                                  margin: EdgeInsets.only(right: 10),
                                  // width: 180,
                                  // height: 100,
                                  child: Switch(
                                    value: true,
                                    onChanged: (value) {
                                      setState(() {});
                                    },
                                  ))
                              // Text("abc"))
                            ]),
                      ),
                      Container(
                        child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              Container(
                                margin: EdgeInsets.only(left: 10),
                                child: Text(
                                  "Bank's name",
                                  style: new TextStyle(
                                    fontWeight: FontWeight.w400,
                                    letterSpacing: 0.5,
                                    color: Colors.black,
                                    // fontSize: 20.0,
                                  ),
                                ),
                              ),
                              Container(
                                margin: EdgeInsets.only(right: 10),
                                width: 180,
                                child:
                                    //     ButtonBar(
                                    //   alignment: MainAxisAlignment.center,
                                    //   children: <Widget>[
                                    //     DropdownButton(
                                    //       value: selectedBank,
                                    //       items: generateItemList(),
                                    //       hint: Text("choose a bank"),
                                    //       onChanged: (value) {
                                    //         setState(() => selectedBank = value);
                                    //       },
                                    //       onTap: () {
                                    //         print("onTap");
                                    //       },
                                    //     )
                                    //   ],
                                    // )
                                    DropdownButtonHideUnderline(
                                  child: DropdownButton(
                                    // items: generateItemList(this.allBanks.keys.toList()),
                                    items: [],
                                    hint: Text("choose a bank"),
                                    // value: this.selectedBank,
                                    onChanged: (value) {
                                      setState(() {
                                        // selectedBank = value;
                                        // Navigator.of(context).pop();
                                        // showEnrollNewDialog(context);
                                      });
                                    },
                                    isExpanded: true,
                                  ),
                                ),
                              )
                            ]),
                      ),
                      Container(
                        height: 150,
                        child: ListView(
                          // This next line does the trick.
                          scrollDirection: Axis.horizontal,
                          children: <Widget>[
                            Container(
                              margin: EdgeInsets.only(left: 10, right: 10),
                              decoration: BoxDecoration(
                                border: Border.all(color: Colors.black54),
                                borderRadius: BorderRadius.circular(12.0),
                                color: Colors.white,
                              ),
                              width: 200.0,
                              // color: Colors.red,
                            ),
                            Container(
                              margin: EdgeInsets.only(left: 10, right: 10),
                              decoration: BoxDecoration(
                                border: Border.all(color: Colors.black54),
                                borderRadius: BorderRadius.circular(12.0),
                                color: Colors.white,
                              ),
                              width: 200.0,
                            ),
                            Container(
                              width: 200.0,
                              color: Colors.green,
                            ),
                          ],
                        ),
                      )
                      // Row(
                      //   mainAxisAlignment: MainAxisAlignment.center,
                      //   children: <Widget>[
                      //     // Text("Transfer"),
                      //     Container(
                      //       decoration: BoxDecoration(
                      //         borderRadius: BorderRadius.circular(12.0),
                      //         color: Colors.white,
                      //       ),
                      //       margin: EdgeInsets.all(10.0),
                      //       alignment: Alignment.center,
                      //       child: Column(
                      //         mainAxisAlignment: MainAxisAlignment.center,
                      //         children: <Widget>[
                      //           Icon(
                      //             IconData(59504, fontFamily: 'MaterialIcons'),
                      //             color: Color(0xffff3783),
                      //             // decoration: ,
                      //           ),
                      //           Text(
                      //             "Credit Card",
                      //             textAlign: TextAlign.center,
                      //           ),
                      //         ],
                      //       ),
                      //       height: 100.0,
                      //       width: 140.0,
                      //       // color: Colors.white,
                      //     ),
                      //     Container(
                      //       decoration: BoxDecoration(
                      //         borderRadius: BorderRadius.circular(12.0),
                      //         color: Colors.white,
                      //       ),
                      //       margin: EdgeInsets.all(10.0),
                      //       alignment: Alignment.center,
                      //       child: Column(
                      //         mainAxisAlignment: MainAxisAlignment.center,
                      //         children: <Widget>[
                      //           FaIcon(
                      //             FontAwesomeIcons.exchangeAlt,
                      //             color: Color(0xffff3783),
                      //           ),
                      //           Text("Transfer"),
                      //         ],
                      //       ),
                      //       height: 100.0,
                      //       width: 140.0,
                      //       // color: Colors.white,
                      //     ),
                      //   ],
                      // ),
                      // Row(
                      //   mainAxisAlignment: MainAxisAlignment.center,
                      //   children: <Widget>[
                      //     // Text("Transfer"),
                      //     Container(
                      //       height: 40.0,
                      //       width: 240,
                      //       margin: EdgeInsets.only(top: 10.0),
                      //       // padding: EdgeInsets.all(0.0),
                      //       // padding: const EdgeInsets.symmetric(vertical: 8.0),
                      //       alignment: Alignment.centerLeft,
                      //       child: TextFormField(
                      //         textAlignVertical: TextAlignVertical.bottom,
                      //         decoration: InputDecoration(
                      //             border: OutlineInputBorder(
                      //                 borderRadius: BorderRadius.circular(12.0),
                      //                 borderSide: BorderSide(
                      //                     width: 5, color: Colors.black)),
                      //             focusedBorder: OutlineInputBorder(
                      //               borderRadius:
                      //                   BorderRadius.all(Radius.circular(10.0)),
                      //               borderSide: BorderSide(
                      //                   width: 2, color: Colors.pink[200]),
                      //             ),
                      //             // borderSide: BorderSide(width: 2,color: Colors.yellowAccent),
                      //             fillColor: Colors.white,
                      //             filled: true,
                      //             // prefixIcon: const Icon(
                      //             //   Icons.person,
                      //             //   color: Colors.grey,
                      //             // ),
                      //             // prefix: Text('Prefix'),
                      //             // suffix: Text('Suffix'),
                      //             hintText: 'Product Enquiry'),
                      //         keyboardType: TextInputType.text,
                      //         // validator: (value) {
                      //         //   if (value.isEmpty) {
                      //         //     return 'Please enter username';
                      //         //   }
                      //         //   return null;
                      //         // },
                      //         // onSaved: (value) => username = value,
                      //         // onSaved: (value) {
                      //         //   validateInputs
                      //         //   name = value;
                      //         // },
                      //       ),
                      //       // height: 100.0,
                      //       // width: 150.0,
                      //       // color: Colors.white,
                      //       // decoration: ,
                      //     ),
                      //     Container(
                      //       height: 40,
                      //       width: 70.0,
                      //       margin: EdgeInsets.only(top: 10.0, left: 10.0),
                      //       alignment: Alignment.centerRight,
                      //       child: RaisedButton(
                      //         onPressed: () {
                      //           // setState(() {
                      //           //   mainPage = false;
                      //           //   signUp = false;
                      //           // });
                      //           _getDataList();
                      //         },
                      //         shape: RoundedRectangleBorder(
                      //             borderRadius: BorderRadius.circular(80.0)),
                      //         padding: EdgeInsets.all(0.0),
                      //         child: Ink(
                      //           decoration: BoxDecoration(
                      //               gradient: LinearGradient(
                      //                 colors: [
                      //                   Color(0xFF9583ED),
                      //                   Color(0xFFC66FD9)
                      //                 ],
                      //                 begin: Alignment.topLeft,
                      //                 end: Alignment.bottomRight,
                      //                 stops: const [0.0, 1.0],
                      //               ),
                      //               borderRadius: BorderRadius.circular(10.0)),
                      //           child: Container(
                      //             constraints: BoxConstraints(
                      //                 maxWidth: 300.0, minHeight: 50.0),
                      //             alignment: Alignment.center,
                      //             child: Text(
                      //               "Search",
                      //               textAlign: TextAlign.center,
                      //               style: TextStyle(
                      //                   color: Colors.white, fontSize: 16.0),
                      //             ),
                      //           ),
                      //         ),
                      //       ),
                      //       // height: 100.0,
                      //       // color: Colors.white,
                      //       // decoration: ,
                      //     )
                      //   ],
                      // ),
                      // Row(
                      //   mainAxisAlignment: MainAxisAlignment.center,
                      //   children: <Widget>[
                      //     // Text("Transfer"),
                      //     Container(
                      //       margin: EdgeInsets.only(top: 50.0),
                      //       // padding: EdgeInsets.all(0.0),
                      //       alignment: Alignment.centerLeft,
                      //       child: Text(
                      //         "Card interest",
                      //         textAlign: TextAlign.center,
                      //         style: TextStyle(
                      //             fontSize: 18.0, fontWeight: FontWeight.w600),
                      //       ),
                      //       // height: 100.0,
                      //       width: 120.0,
                      //       // color: Colors.white,
                      //       // decoration: ,
                      //     ),
                      //     Container(
                      //       margin: EdgeInsets.only(top: 50.0, left: 30.0),
                      //       alignment: Alignment.centerLeft,
                      //       child: this.updateTime == null
                      //           ? Text("Last updated: ")
                      //           : Text("Last updated: " +
                      //               new DateTime.fromMillisecondsSinceEpoch(
                      //                       this.updateTime)
                      //                   .toString()
                      //                   // .substring(0, 19)),
                      //                   .substring(0, 10)),
                      //       // height: 100.0,
                      //       width: 180.0,
                      //       // color: Colors.white,
                      //       // decoration: ,
                      //     )
                      //   ],
                      // ),
                      // Expanded(
                      //   child: Column(
                      //     mainAxisSize: MainAxisSize.max,
                      //     children: <Widget>[
                      //       // _getInterestList(),
                      //       Expanded(child: _getInterestList(context)),
                      //     ],
                      //   ),
                      // )
                    ]),
                  ),
                )));
  }

  Widget _buildDrawer() {
    return Drawer(
      // Add a ListView to the drawer. This ensures the user can scroll
      // through the options in the drawer if there isn't enough vertical
      // space to fit everything.
      child: ListView(
        // Important: Remove any padding from the ListView.
        padding: EdgeInsets.zero,
        children: <Widget>[
          DrawerHeader(
            child: Text('Drawer Header'),
            decoration: BoxDecoration(
              color: Colors.blue,
            ),
          ),
          ListTile(
            title: Text('Item 1'),
            onTap: () {
              // Update the state of the app
              // ...
              // Then close the drawer
              Navigator.pop(context);
            },
          ),
          ListTile(
            title: Text('Item 2'),
            onTap: () {
              // Update the state of the app
              // ...
              // Then close the drawer
              Navigator.pop(context);
            },
          ),
        ],
      ),
    );
  }

  ListView _getInterestList(BuildContext context) {
    // print("this.dataList");
    // print(this.dataList);
    // final items = [1, 2, 3, 4, 5];
    // final myItems = _getInterestData();
    final cardList = this.dataList.map((e) {
      return Container(
          margin: EdgeInsets.only(top: 10.0),
          height: 100,
          child: Card(
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(10.0),
            ),
            child: InkWell(
              child: Row(children: <Widget>[
                Container(
                  // color: Colors.blue,
                  width: 110.0,
                  padding: EdgeInsets.only(left: 20.0),
                  alignment: Alignment.centerLeft,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    // mainAxisAlignment: Alignment.center,
                    children: <Widget>[
                      Text(
                        // "${_data.getName(index)}",
                        e.bankName,
                        style: TextStyle(
                          fontWeight: FontWeight.w600,
                          fontSize: 18.0,
                          color: Colors.orange,
                        ),
                      ),
                      Text(
                        // "${_data.getName(index)}",
                        e.cardName,
                        style: TextStyle(
                          fontWeight: FontWeight.w500,
                          // color: Colors.orange,
                        ),
                      ),
                      Text(
                        // "${_data.getName(index)}",
                        (e.interestRate.toString() + "%"),
                        style: TextStyle(
                          fontWeight: FontWeight.w500,
                          // fontSize: 18.0,
                          color: Colors.grey,
                          // color: Colors.orange,
                        ),
                      ),
                    ],
                  ),
                ),
                Container(
                    width: 200.0,
                    // margin: EdgeInsets.all(10.0),
                    alignment: Alignment.centerRight,
                    child: Text(
                      ">",
                      textAlign: TextAlign.right,
                      style: TextStyle(
                        fontWeight: FontWeight.w200,
                        fontSize: 24.0,
                      ),
                    ))
              ]),
              onTap: () {
                print("Tapped on container");
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        // builder: (context) => LoginSuccessPage(
                        //     userToken: UserToken(userToken["sub"], userToken["role"],
                        //         userToken["exp"], userToken["iat"]))
                        builder: (context) => CardInterestDetailPage(
                              cardInterest: e,
                              updateTime: this.updateTime,
                            )));
              },
            ),
          ));
    });
    return ListView(children: cardList.toList());
    // ListView.builder(
    //   itemCount: items.length,
    //   itemBuilder: _itemBuilder,
    //   // itemBuilder: (context, index) {
    //   //   return ListTile(
    //   //     title: Text('${items[index]}'),
    //   //   );
    //   // },
    // );
  }

  void _getDataList() {
    final data = _getInterestData();
    // print('then => print');
    data.then((value) {
      var mydata = value;
      int updateTime;
      print(mydata);
      List<CardInterest> myDataList = [];
      mydata.forEach((key, value) {
        updateTime = mydata[key]['dateTime'];
        mydata[key]['creditCards'].forEach((card) {
          var cardinterest = new CardInterest._(
              bankName: key,
              cardName: card['name'],
              interestRate: card['interestRate']);
          // return cardinterest;
          myDataList.add(cardinterest);
        });
        // return cards;
      });
      // print(myDataList);
      print(updateTime);
      setState(() {
        this.dataList = myDataList;
        this.updateTime = updateTime;
      });
    });
  }

  Future<Map> _getInterestData() async {
    // ==== Get token first. ====== //
    String token = await _getToken();
    token = "Bearer " + token;

    var url = "https://10.6.88.55:8080/api/product_enquiry";
    HttpClient httpClient = new HttpClient()
      ..badCertificateCallback =
          ((X509Certificate cert, String host, int port) => true);
    HttpClientRequest request = await httpClient.postUrl(Uri.parse(url));

    Map<dynamic, dynamic> requestBody = {
      "bandId_BROADCAST": {"input": "all world 123"},
      "bank1": {},
      "bank2": {"input": "world 1"},
      "string": "123",
      "num": 123,
      "ary": ["df", 4]
    };
    request.headers.set('Authorization', token);
    request.headers.set(HttpHeaders.contentTypeHeader, "application/json");
    request.write(jsonEncode(requestBody));
    HttpClientResponse response = await request.close();
    // print(response);
    String reply = await response.transform(utf8.decoder).join();
    var body = json.decode(reply);
    // print(body);
    // var token = body["token"];
    // var newToken = parseJwt(token);
    // print(newToken);

    // List<int> a = [1, 2, 3];
    // print(a);
    // return a;
    return body;
  }

  Future<String> _getToken() async {
    var url = "https://10.6.88.55:8084/api/sys/jwt_token/create";
    HttpClient httpClient = new HttpClient()
      ..badCertificateCallback =
          ((X509Certificate cert, String host, int port) => true);
    HttpClientRequest request = await httpClient.postUrl(Uri.parse(url));

    // request.headers.set('Authorization', encodeNew);
    var email = "zxgao@astri.org";
    var password = "n0v@Credit";
    Map<dynamic, dynamic> requestBody = {"userId": email, "password": password};

    request.headers.set(HttpHeaders.contentTypeHeader, "application/json");
    request.write(jsonEncode(requestBody));
    HttpClientResponse response = await request.close();
    // print("Response");
    // print(response);
    // return response.toString();
    String reply = await response.transform(utf8.decoder).join();
    var body = json.decode(reply);
    var token = body["token"];
    // var newToken = parseJwt(token);
    // print(newToken);
    print(token);
    return token;
  }

  Future<Null> _getMyModules() async {
    String token = await storage.read(key: "token");
    // print(token);
    String email = await storage.read(key: "token_sub");
    var url = "https://10.6.88.55:8084/api/user/account/list";
    var requestBody = {};
    HttpClient httpClient = new HttpClient()
      ..badCertificateCallback =
          ((X509Certificate cert, String host, int port) => true);
    HttpClientRequest request = await httpClient.postUrl(Uri.parse(url));
    request.headers.set('Authorization', token);
    request.headers.set(HttpHeaders.contentTypeHeader, "application/json");
    request.write(jsonEncode(requestBody));
    HttpClientResponse response = await request.close();
    String reply = await response.transform(utf8.decoder).join();
    var body = json.decode(reply);
    print(body.runtimeType);
    print(body);
    var modules = body[email]["modules"];
    var moduleNames;
    if (modules == null) {
      moduleNames = [];
    } else {
      modules.keys.forEach((e) {
        modules[e]["moduleId"] = e;
      });
      moduleNames = modules.keys.toList();
    }
    print(modules);
    print(moduleNames);

    setState(() {
      myModulesName = moduleNames;
      myModules = modules;
    });
  }

  List<DropdownMenuItem> generateItemList(List items) {
    List<DropdownMenuItem> dropdownItems = items
        .map((item) => DropdownMenuItem(
              value: item,
              child: Text(item),
              // onTap: () {
              //   print("DropdownMenuItem is onTap");
              // }
            ))
        .toList();
    // print(dropdownItems);
    return dropdownItems;
  }
}

class CardInterest {
  final String bankName;
  final String cardName;
  final double interestRate;

  CardInterest._({this.bankName, this.cardName, this.interestRate});

  factory CardInterest.fromJson(Map<String, dynamic> json) {
    return new CardInterest._(
        bankName: json['title'],
        cardName: json['docid'],
        interestRate: json["rate"]);
  }
}

class CardInterestDetailPage extends StatelessWidget {
  final CardInterest cardInterest;
  final int updateTime;
  CardInterestDetailPage(
      {Key key, @required this.cardInterest, this.updateTime})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Interest Rate Details"),
        backgroundColor: Colors.pink[300],
      ),
      body: Padding(
        padding: EdgeInsets.all(16.0),
        child: showDetails(),
      ),
    );
  }

  Widget showDetails() {
    print(cardInterest);
    var bankName = cardInterest.bankName;
    // new DateTime.fromMillisecondsSinceEpoch(userToken.expTime * 1000)
    //     .toString();
    var cardName = cardInterest.cardName;
    // new DateTime.fromMillisecondsSinceEpoch(userToken.issueTime * 1000)
    //     .toString();
    var interestRate = cardInterest.interestRate;
    return new Container(
        child: Center(
      child: Column(
        children: <Widget>[
          Container(
            padding: EdgeInsets.all(50),
          ),
          // Icon(
          //   Icons.check_circle,
          //   color: Colors.green,
          // ),
          // Text("Login Successfully"),
          Align(
            // alignment: Alignment.centerLeft,
            child: Container(
              padding: EdgeInsets.only(left: 20),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                // padding: EdgeInsets.all(100),
                children: <Widget>[
                  Row(
                    children: <Widget>[
                      Text(
                        "Bank Name: ",
                        style: TextStyle(fontSize: 20.0),
                      ),
                      Text(bankName),
                    ],
                  ),
                  Row(
                    children: <Widget>[
                      Text(
                        "Card Name: ",
                        style: TextStyle(fontSize: 20.0),
                      ),
                      Text(cardName),
                    ],
                  ),
                  Row(
                    children: <Widget>[
                      Text(
                        "Interest Rate: ",
                        style: TextStyle(fontSize: 20.0),
                      ),
                      Text(interestRate.toString() + "%"),
                    ],
                  ),
                  Row(
                    children: <Widget>[
                      Text(
                        "Update Time: ",
                        style: TextStyle(fontSize: 20.0),
                      ),
                      Text(new DateTime.fromMillisecondsSinceEpoch(
                              this.updateTime)
                          .toString()
                          .substring(0, 19)),
                    ],
                  )
                ],
              ),
            ),
          ),
        ],
      ),
      // heightFactor: 8,
      // widthFactor: 20
    ));
  }
}

String _decodeBase64(String str) {
  String output = str.replaceAll('-', '+').replaceAll('_', '/');

  switch (output.length % 4) {
    case 0:
      break;
    case 2:
      output += '==';
      break;
    case 3:
      output += '=';
      break;
    default:
      throw Exception('Illegal base64url string!"');
  }

  return utf8.decode(base64Url.decode(output));
}

Map<String, dynamic> parseJwt(String token) {
  final parts = token.split('.');
  if (parts.length != 3) {
    throw Exception('invalid token');
  }

  final payload = _decodeBase64(parts[1]);
  final payloadMap = json.decode(payload);
  if (payloadMap is! Map<String, dynamic>) {
    throw Exception('invalid payload');
  }

  return payloadMap;
}

class Data {
  Map fetched_data = {
    "data": [
      {"id": 111, "name": "abc"},
      {"id": 222, "name": "pqr"},
      {"id": 333, "name": "abc"}
    ]
  };
  List _data;

//function to fetch the data

  Data() {
    _data = fetched_data["data"];
  }

  int getId(int index) {
    return _data[index]["id"];
  }

  String getName(int index) {
    return _data[index]["name"];
  }

  int getLength() {
    return _data.length;
  }
}
