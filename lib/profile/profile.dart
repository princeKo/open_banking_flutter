import 'package:flutter/material.dart';
import 'package:open_banking/profile/changePassword.dart';

class ProfilePage extends StatefulWidget {
  // BankAssociationPage({Key key}) : super(key: key);

  @override
  _ProfilePageState createState() => _ProfilePageState();
}

class _ProfilePageState extends State {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: new ListView(
            // padding: EdgeInsets.only(left: 20, right: 20),
            children: <Widget>[
          Container(
              padding: const EdgeInsets.all(20),
              child: Text(
                "Profile",
                textAlign: TextAlign.left,
                style: new TextStyle(
                    fontWeight: FontWeight.w400,
                    letterSpacing: 0.5,
                    color: Colors.black,
                    fontSize: 26.0),
              )),
          Container(
            // margin: const EdgeInsets.all(20),
            height: 60.0,
            // color: Colors.white,
            child: Container(
                alignment: Alignment.centerLeft,
                child: FlatButton(
                  onPressed: () => Navigator.of(context).pop(),
                  child: Text(
                    "<  Back",
                  ),
                )),
          ),
          Container(
              margin: EdgeInsets.only(bottom: 10, left: 20, right: 20),
              // decoration: BoxDecoration(
              //   borderRadius: BorderRadius.circular(12.0),
              //   color: Colors.white,
              // ),
              child: Ink(
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(12.0),
                    color: Colors.white,
                  ),
                  child: ListTile(
                    onTap: () {
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => ChangePasswordPage()));
                    },
                    leading: Text("Change Password"),
                    trailing: new Text(">"),
                  ))),
          Container(
              margin: EdgeInsets.only(bottom: 10, left: 20, right: 20),
              child: Ink(
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(12.0),
                    color: Colors.white,
                  ),
                  child: ListTile(
                    onTap: () {},
                    leading: Text("Log out"),
                    trailing: new Text(">"),
                  ))),
        ]));
  }
}
