import 'package:flutter/material.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';

class ChangePasswordPage extends StatefulWidget {
  // final Map<String, dynamic> allBanks;

  // OpenAccountPage({Key key, @required this.allBanks}) : super(key: key);

  @override
  _ChangePasswordPageState createState() => _ChangePasswordPageState();
}

class _ChangePasswordPageState extends State {
  final storage = FlutterSecureStorage();

  String oldPassword = '';
  String newPassword = '';
  String confirmPassword = '';

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        resizeToAvoidBottomInset: false,
        body: new ListView(
            // padding: EdgeInsets.only(left: 20, right: 20),
            children: <Widget>[
              Container(
                  padding: const EdgeInsets.all(20),
                  child: Text(
                    "Change password",
                    textAlign: TextAlign.left,
                    style: new TextStyle(
                        fontWeight: FontWeight.w400,
                        letterSpacing: 0.5,
                        color: Colors.black,
                        fontSize: 26.0),
                  )),
              Container(
                  padding: EdgeInsets.all(20),
                  height: 600,
                  child: Container(
                      color: Colors.white,
                      child: Column(children: <Widget>[
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            Container(
                                padding: EdgeInsets.only(top: 10.0),
                                child: FlatButton(
                                  onPressed: () {
                                    Navigator.of(context).pop();
                                  },
                                  child: Text(
                                    "< Cancel",
                                  ),
                                )),
                            Container(
                              padding: EdgeInsets.only(top: 10.0),
                              // child: Text(
                              //   "Edit",
                              // ),
                              child: FlatButton(
                                onPressed: () {},
                                child: Text("Save"),
                              ),
                            ),
                          ],
                        ),
                        Divider(color: Colors.black54, height: 0),
                        Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              Container(
                                margin: EdgeInsets.only(left: 10),
                                child: Text("Old password"),
                              ),
                              Container(
                                margin: EdgeInsets.only(right: 10),
                                width: 180,
                                child: TextFormField(
                                    decoration: new InputDecoration(
                                      border: InputBorder.none,
                                      focusedBorder: InputBorder.none,
                                      enabledBorder: InputBorder.none,
                                      errorBorder: InputBorder.none,
                                      disabledBorder: InputBorder.none,
                                      // contentPadding: EdgeInsets.only(
                                      //     left: 15, bottom: 11, top: 11, right: 15),
                                      // hintText: "sLabel",
                                    ),
                                    onSaved: (value) {
                                      setState(() {
                                        oldPassword = value;
                                      });
                                    },
                                    onChanged: (value) {
                                      setState(() {
                                        oldPassword = value;
                                      });
                                    }
                                    // // onSaved: (value) {
                                    // //   validateInputs
                                    // //   name = value;
                                    // // },
                                    ),
                              )
                            ]),
                        Divider(color: Colors.black54, height: 0),
                        Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              Container(
                                margin: EdgeInsets.only(left: 10),
                                child: Text("New password"),
                              ),
                              Container(
                                margin: EdgeInsets.only(right: 10),
                                width: 180,
                                child: TextFormField(
                                    decoration: new InputDecoration(
                                      border: InputBorder.none,
                                      focusedBorder: InputBorder.none,
                                      enabledBorder: InputBorder.none,
                                      errorBorder: InputBorder.none,
                                      disabledBorder: InputBorder.none,
                                      // hintText: "sLabel",
                                    ),
                                    onChanged: (value) {
                                      setState(() {
                                        newPassword = value;
                                      });
                                    }),
                              )
                            ]),
                        Divider(color: Colors.black54, height: 0),
                        Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              Container(
                                margin: EdgeInsets.only(left: 10),
                                child: Text("Confirm password"),
                              ),
                              Container(
                                margin: EdgeInsets.only(right: 10),
                                width: 180,
                                child: TextFormField(
                                    decoration: new InputDecoration(
                                      border: InputBorder.none,
                                      focusedBorder: InputBorder.none,
                                      enabledBorder: InputBorder.none,
                                      errorBorder: InputBorder.none,
                                      disabledBorder: InputBorder.none,
                                      // hintText: "sLabel",
                                    ),
                                    onChanged: (value) {
                                      setState(() {
                                        confirmPassword = value;
                                      });
                                    }),
                              )
                            ]),
                        Divider(color: Colors.black54, height: 0),
                      ])))
            ]));
  }
}
