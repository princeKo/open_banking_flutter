import 'package:flutter/material.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'dart:io';
import 'dart:convert';

import 'associateBank.dart';
import 'dart:math';

void main() {
  runApp(MaterialApp(
    home: Scaffold(
        // appBar: AppBar(
        //   title: Text('Demo'),
        // ),
        body: BankAssociationPage()),
  ));
}

class BankAssociationPage extends StatefulWidget {
  BankAssociationPage({Key key}) : super(key: key);

  @override
  _BankAssociationPageState createState() => _BankAssociationPageState();
}

class _BankAssociationPageState extends State {
  final storage = FlutterSecureStorage();
  String token = '';
  Map<String, dynamic> allBanks = {};
  Map<String, dynamic> associatedBanks = {};
  var selectedBank;
  var selectedAccount;

  @override
  void initState() {
    super.initState();
    _getToken();
    _getAssociatedBanks();
    _getAllBanks();
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
        body: new ListView(
            padding: const EdgeInsets.only(left: 20, right: 20),
            children: <Widget>[
          Container(
              padding: const EdgeInsets.only(bottom: 20),
              child: Text(
                "Bank association",
                textAlign: TextAlign.left,
                style: new TextStyle(
                    fontWeight: FontWeight.w400,
                    letterSpacing: 0.5,
                    color: Colors.black,
                    fontSize: 26.0),
              )),
          Container(
              // margin: const EdgeInsets.all(20),
              height: 60.0,
              width: 140.0,
              color: Colors.white,
              child:
                  // Container(
                  //     decoration: BoxDecoration(
                  //       borderRadius: BorderRadius.circular(12.0),
                  //       // color: Colors.white,
                  //     ),
                  //     padding: EdgeInsets.only(left: 10, right: 20),
                  //     child:
                  FlatButton(
                onPressed: () async {
                  // bool enroll = await showEnrollNewDialog(context);
                  // if (enroll == true) {
                  //   print("selected Confirm");
                  //   var a = await _enrollNewModule(selectedValue);
                  //   print(a);
                  //   await _getAssociatedBanks();
                  // } else {
                  //   print("selected Later");
                  // }

                  showToAssociateBankPage(context);
                },
                // onTap: () {
                //   showEnrollNewDialog(context);
                // },
                child: Row(
                  // mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Container(
                      margin: const EdgeInsets.only(left: 10, right: 20),
                      child: Icon(Icons.add_circle, color: Color(0xffff3783)),
                    ),
                    Text("Associate with a new bank"),
                  ],
                ),
                // alignment: Alignment.center,
              )),
          Container(
            // margin: EdgeInsets.only(left: 20, right: 20),
            height: 500.0,
            // width: 140.0,
            child: _buildListView(),
            alignment: Alignment.center,
          ),
        ]));
  }

  Future<Null> _getToken() async {
    String token = await storage.read(key: "token");
    // print(token);
    setState(() {
      this.token = token;
    });
  }

  Future<Null> _getAllBanks() async {
    String token = await storage.read(key: "token");
    var email = await storage.read(key: "token_sub");

    // print(token);
    var url = "https://10.6.88.55:8084/api/sys/bankId_enquiry";
    var requestBody = {};
    HttpClient httpClient = new HttpClient()
      ..badCertificateCallback =
          ((X509Certificate cert, String host, int port) => true);
    HttpClientRequest request = await httpClient.postUrl(Uri.parse(url));
    request.headers.set('Authorization', token);
    request.headers.set(HttpHeaders.contentTypeHeader, "application/json");
    request.write(jsonEncode(requestBody));
    HttpClientResponse response = await request.close();
    String reply = await response.transform(utf8.decoder).join();
    var body = json.decode(reply);
    print("res - _getAllBanks  api/sys/bankId_enquiry");
    print(body);
    // List filteredData =
    //     body.keys.where((key) => body[key]["moduleType"] != "AP").toList();
    // print(filteredData);

    setState(() {
      allBanks = body["bankIds"];
    });
  }

  Future<Null> _getAssociatedBanks() async {
    String token = await storage.read(key: "token");
    // print(token);
    String email = await storage.read(key: "token_sub");
    // print(email);
    var url = "https://10.6.88.55:8084/api/sys/tx/accounts/list";

    // var apiUrl = 'api/user/account/list';

    // var token = accessCookie("A_Token");
    // let email = getUserID(token);

    // var requestBody = {email: {}};
    var requestBody = {};
    HttpClient httpClient = new HttpClient()
      ..badCertificateCallback =
          ((X509Certificate cert, String host, int port) => true);
    HttpClientRequest request = await httpClient.postUrl(Uri.parse(url));
    request.headers.set('Authorization', token);
    request.headers.set(HttpHeaders.contentTypeHeader, "application/json");
    request.write(jsonEncode(requestBody));
    HttpClientResponse response = await request.close();
    String reply = await response.transform(utf8.decoder).join();
    var body = json.decode(reply);
    print("res - _getAssociatedBanks - api/sys/tx/accounts/list");
    print(body);
    var banks = body;

    // var filteredBankIds;
    // if (banks == null) {
    //   filteredBankIds = [];
    // } else {
    //   var bankIds = banks.keys;
    //   filteredBankIds = bankIds.where((bankId) {
    //     var accounts = banks[bankId];
    //     var bankUserNames = accounts.keys;
    //     // bool isHasToken = bankUserNames.any((bankUserName) {
    //     //   if (accounts[bankUserName]["accessToken"] &&
    //     //       accounts[bankUserName]["accessToken"].length > 0) {
    //     //     return true;
    //     //   } else {
    //     //     return false;
    //     //   }
    //     // });
    //     bool isHasToken = bankUserNames.any((bankUserName) =>
    //         (accounts[bankUserName]["accessToken"] != null &&
    //             accounts[bankUserName]["accessToken"].length > 0));

    //     // print("isHasToken");
    //     // print(isHasToken);
    //     if (isHasToken) {
    //       return true;
    //     } else {
    //       return false;
    //     }
    //   });
    // print("filteredBankIds");
    // print(filteredBankIds);

    // print(banks);
    setState(() {
      // AllBanks = banks;
      associatedBanks = banks;
    });
    // }
  }

  ListView _buildListView() {
    // print("associatedBanks: " + this.associatedBanks.toString());
    var sortedBanks = [];
    if (this.associatedBanks != null) {
      sortedBanks = this.associatedBanks.keys.toList();
      sortedBanks.sort();
    }

    // associatedBanks = [
    //   ...associatedBanks,
    //   "fake1",
    //   "fake2",
    //   "fake3",
    //   "fake4",
    //   "fake5",
    //   "fake6",
    //   "fake7",
    //   "fake8",
    //   "fake9",
    //   "fake10"
    // ];
    return new ListView(
        children: sortedBanks.length > 0
            ? sortedBanks.map((bankId) {
                List associateDates;
                List accountIds = this.associatedBanks[bankId].keys.toList();
                associateDates = accountIds
                    .map((accountId) =>
                        this.associatedBanks[bankId][accountId]["createdAt"])
                    .toList();
                associateDates.sort();
                print(associateDates);
                String earlistDate =
                    DateTime.fromMillisecondsSinceEpoch(associateDates[0])
                        .toString()
                        .substring(0, 19);
                return Container(
                    margin: const EdgeInsets.only(bottom: 10),
                    // decoration: BoxDecoration(
                    //   borderRadius: BorderRadius.circular(12.0),
                    //   color: Colors.red[50],
                    // ),
                    height: 60.0,
                    child: FlatButton(
                        shape: new RoundedRectangleBorder(
                            borderRadius: new BorderRadius.circular(10.0)),
                        color: Colors.white,
                        onPressed: () {
                          print("Tapped on FlatButton");
                        },
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Text(
                                  bankId,
                                  style: TextStyle(fontSize: 16),
                                ),
                                Text(
                                  "Associated from " + earlistDate,
                                  style: TextStyle(
                                      fontSize: 12, color: Colors.grey),
                                )
                              ],
                            ),
                            Text(">")
                          ],
                        )));
              }).toList()
            : [Container()]);
  }

  void showToAssociateBankPage(context) async {
    final result = await Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => AssociateBankPage(
                  allBanks: this.allBanks,
                  // accountItems: this.allMyBanks[this.selectedBank],
                )));
  }
}

// class CardInterestDetailPage extends StatelessWidget {
//   final CardInterest cardInterest;
//   final int updateTime;
//   CardInterestDetailPage(
//       {Key key, @required this.cardInterest, this.updateTime})
//       : super(key: key);
