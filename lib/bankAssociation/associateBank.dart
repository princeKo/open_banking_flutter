import 'package:flutter/material.dart';
import 'dart:io';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'dart:convert';
import './inAppsWebView.dart';
import 'package:url_launcher/url_launcher.dart';

void main() {
  runApp(MaterialApp(
      home: Scaffold(
    // appBar: AppBar(
    //   title: Text('Demo'),
    // ),
    body: AssociateBankPage(allBanks: {
      "banka": {"zxg2": {}, "zxg1": {}, "zxg4": {}, "zxg3": {}}
    }),
  )));
}

class AssociateBankPage extends StatefulWidget {
  final Map<String, dynamic> allBanks;

  AssociateBankPage({Key key, @required this.allBanks}) : super(key: key);

  @override
  _AssociateBankPageState createState() =>
      _AssociateBankPageState(allBanks: allBanks);
}

class _AssociateBankPageState extends State {
  final Map<String, dynamic> allBanks;
  String selectedBank;
  String selectedAccount;
  String authUrl;

  _AssociateBankPageState({Key key, @required this.allBanks}) : super();

  @override
  Widget build(BuildContext context) {
    // print("this.bankItems");
    // print(this.allBanks);

    // List accountItems = [];
    // if (this.allBanks[this.selectedBank] != null) {
    //   accountItems = this.allBanks[this.selectedBank].keys.toList();
    // }

    return Scaffold(
        body: new ListView(
            // padding: EdgeInsets.only(left: 20, right: 20),
            children: <Widget>[
          Container(
              padding: const EdgeInsets.all(20),
              child: Text(
                "Associate Bank",
                textAlign: TextAlign.left,
                style: new TextStyle(
                    fontWeight: FontWeight.w400,
                    letterSpacing: 0.5,
                    color: Colors.black,
                    fontSize: 26.0),
              )),
          Container(
              color: Colors.white,
              margin: EdgeInsets.all(20),
              height: 500,
              child: Column(children: <Widget>[
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Container(
                        padding: EdgeInsets.only(top: 10.0),
                        child: FlatButton(
                          onPressed: () => Navigator.of(context).pop(),
                          child: Text(
                            "<  Cancel",
                          ),
                        )),
                    Container(
                      padding: EdgeInsets.only(top: 10.0),
                      // child: Text(
                      //   "Edit",
                      // ),
                      child: FlatButton(
                        onPressed: () async {
                          // print("Hello ! =======");
                          // print(this.selectedBank);
                          // print(this.selectedAccount);
                          if (this.selectedBank != null) {
                            bool result = await showAddNewDialog(context);
                            if (result == true) {
                              print("selected Confirm");
                              await _addAssociate(
                                  this.selectedBank, this.selectedAccount);
                            } else {
                              print("selected Later");
                            }
                          } else {
                            print("Please select a bank name");
                          }
                          // print(this.module.settings);
                          // print(this.module.associatedBanks);
                          // setState(() {
                          //   isEditing = !isEditing;
                          // });
                          // _editModule(this.module);
                        },
                        child: Text("Add"),
                      ),
                    ),
                  ],
                ),
                Divider(color: Colors.black54, height: 0),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Container(
                      margin: EdgeInsets.only(left: 10),
                      child: Text("Bank's name"),
                    ),
                    Container(
                        // margin: EdgeInsets.only(left: 10),
                        width: 180,
                        child: DropdownButtonHideUnderline(
                            child: ButtonTheme(
                          alignedDropdown: true,
                          child: DropdownButton(
                            // style: TextStyle(color: Colors.red),
                            items:
                                generateItemList(this.allBanks.keys.toList()),
                            hint: Text("choose a bank"),
                            value: this.selectedBank,
                            onChanged: (value) {
                              setState(() {
                                selectedBank = value;
                                // Navigator.of(context).pop();
                                // showEnrollNewDialog(context);
                              });
                            },
                            // isExpanded: true,
                          ),
                        ))),
                  ],
                ),
                Divider(color: Colors.black54, height: 0),
                // Row(
                //     mainAxisAlignment: MainAxisAlignment.spaceBetween,
                //     children: <Widget>[
                //       Container(
                //         margin: EdgeInsets.only(left: 10),
                //         child: Text("Bank's username"),
                //       ),
                //       Container(
                //         // margin: EdgeInsets.only(left: 10),
                //         padding: EdgeInsets.only(left: 16),
                //         width: 180,
                //         child:
                //             // DropdownButtonHideUnderline(
                //             //     child: ButtonTheme(
                //             //   minWidth: 100,
                //             //   alignedDropdown: true,
                //             //   child: DropdownButton(
                //             //     items: generateItemList(accountItems),
                //             //     hint: Text("choose username"),
                //             //     value: this.selectedAccount,
                //             //     onChanged: (value) {
                //             //       setState(() {
                //             //         selectedAccount = value;
                //             //         // Navigator.of(context).pop();
                //             //         // showEnrollNewDialog(context);
                //             //       });
                //             //     },
                //             //     onTap: () {
                //             //       print("onTap");
                //             //     },
                //             //     // isDense: true,
                //             //     isExpanded: true,
                //             //   ),
                //             TextFormField(
                //                 // controller: _usernameController,
                //                 decoration: new InputDecoration(
                //                   border: InputBorder.none,
                //                   focusedBorder: InputBorder.none,
                //                   enabledBorder: InputBorder.none,
                //                   errorBorder: InputBorder.none,
                //                   disabledBorder: InputBorder.none,
                //                   // hintText: "sLabel",
                //                 ),
                //                 onChanged: (value) {
                //                   setState(() {
                //                     selectedAccount = value;
                //                   });
                //                 }),
                //       )
                //     ]),
                // Divider(color: Colors.black54, height: 0),
              ]))
        ]));
  }

  List<DropdownMenuItem> generateItemList(List items) {
    items.sort();
    List<DropdownMenuItem> dropdownItems = items
        .map((item) => DropdownMenuItem(
              value: item,
              child: Text(item),
              // onTap: () {
              //   print("DropdownMenuItem is onTap");
              // }
            ))
        .toList();
    // print(dropdownItems);
    return dropdownItems;
  }

  Future<void> _addAssociate(String bankId, String bankUserName) async {
    final storage = FlutterSecureStorage();
    String token = await storage.read(key: "token");
    // print(token);
    var url = "https://10.6.88.55:8084/api/sys/oauth2/bank";
    var requestBody = {
      "bankId": bankId,
      "bankUserName": bankUserName,
    };
    print("requestBody");
    print(requestBody);
    HttpClient httpClient = new HttpClient()
      ..badCertificateCallback =
          ((X509Certificate cert, String host, int port) => true);
    HttpClientRequest request = await httpClient.postUrl(Uri.parse(url));
    request.headers.set('Authorization', token);
    request.headers.set(HttpHeaders.contentTypeHeader, "application/json");
    request.write(jsonEncode(requestBody));
    HttpClientResponse response = await request.close();
    String reply = await response.transform(utf8.decoder).join();
    var body = json.decode(reply);
    // print(body.runtimeType);
    // print(body);
    print("reply - _addAssociate");
    print(reply);
    setState(() {
      authUrl = body["authUrl"];
    });
    _openWebView(bankId, bankUserName, body["authUrl"], "cookies");
    // launchURL() {
    //   launch('https://www.himmy.cn/');
    // }
  }

  Future<void> _openWebView(bankId, bankUserName, url, cookiesTobeSet) async {
    // var a = new InternetAddress("10.6.88.55");
    // print("a.host");
    // print(a.host);

    final storage = FlutterSecureStorage();
    String aToken = await storage.read(key: "token");
    String token = aToken.split(" ")[1];
    // print(b[1]);

    final result = await Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => InAppWebViewExampleScreen(
                  service: new Service(
                    "Bank Association",
                    url,
                    // "https://www.google.com",
                    // "http://10.6.126.139:8080/openid-connect-server-webapp/authorize?response_type=code&client_id=d13ad77f-7d2c-4503-a0dd-be9ea6af0414&redirect_uri=https://10.6.88.98:3000/auth&scope=openid%20profile%20email%20address%20phone%20read:account%20write:account%20read:card%20write:card", // "http://10.6.88.98:3000/auth",
                    // "http://10.6.88.98:3000",
                    // "http://10.6.126.139:8080/openid-connect-server-webapp/authorize?response_type=code&client_id=e53cf3c1-a082-4075-be12-9b9b9c5e57c0&redirect_uri=https://10.6.88.55:8084/auth&scope=openid%20profile%20email%20address%20phone",
                    // "http://oauth2:8080/openid-connect-server-webapp/authorize?response_type=code&client_id=e53cf3c1-a082-4075-be12-9b9b9c5e57c0&redirect_uri=https://openbankportal:8084/auth&scope=openid%20profile%20email%20address%20phone",
                    {
                      "token": {
                        "url": "https://10.6.88.55",
                        "name": "token",
                        "value": token
                      },
                      "bankId": {
                        "url": "https://10.6.88.55",
                        "name": "bankName",
                        "value": bankId
                      },
                      "A_Token": {
                        "url": "https://10.6.88.55",
                        "name": "token",
                        "value": aToken
                      },
                      // "bankUserName": {
                      //   "url": "https://10.6.88.55",
                      //   "name": "bankUserName",
                      //   "value": bankUserName
                      // }
                    },
                  ),
                  // accountItems: this.allBanks[this.selectedBank],
                )));
  }

  Future<bool> showAddNewDialog(BuildContext context) {
    return showDialog<bool>(
        context: context,
        builder: (context) {
          return AlertDialog(
            // title: Text("Associate with new bank"),
            content: Text("Associate with this bank?"),
            actions: <Widget>[
              FlatButton(
                child: Text("Confirm"),
                onPressed: () => Navigator.of(context).pop(true),
              ),
              FlatButton(
                child: Text("Later"),
                onPressed: () {
                  Navigator.of(context).pop(false);
                },
              ),
            ],
          );
        });
  }
}
