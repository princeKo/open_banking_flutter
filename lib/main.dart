import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';

// import './webView.dart';
// import './webView2.dart';

import 'bankAssociation/inAppsWebView.dart';
import './login.dart';

// void main() {
//   runApp(MyApp());
// }

void main() {
  runApp(MaterialApp(
    theme: ThemeData(
      primarySwatch: Colors.pink,
    ),
    home: Scaffold(
        // resizeToAvoidBottomPadding: false,
        // resizeToAvoidBottomInset: false,
        // appBar: AppBar(
        //   title: Text('Demo'),
        // ),
        body: LoginSignupPage()),
  ));
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  // final myTabs = [Tab(key: Key("a"), text: "a"), Tab(key: Key("b"), text: "b"),];
  // final myTabs = [
  // TabItem("Page 1", Icon(CupertinoIcons.home), Icon(CupertinoIcons.home)),
  // TabItem("Page 2", Icon(CupertinoIcons.conversation_bubble), Icon(CupertinoIcons.conversation_bubble)),
  // TabItem("Page 3", Icon(CupertinoIcons.group), Icon(CupertinoIcons.group_solid)),
  // ];
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        // This is the theme of your application.
        //
        // Try running your application with "flutter run". You'll see the
        // application has a blue toolbar. Then, without quitting the app, try
        // changing the primarySwatch below to Colors.green and then invoke
        // "hot reload" (press "r" in the console where you ran "flutter run",
        // or simply save your changes to "hot reload" in a Flutter IDE).
        // Notice that the counter didn't reset back to zero; the application
        // is not restarted.
        primarySwatch: Colors.blue,
        // This makes the visual density adapt to the platform that you run
        // the app on. For desktop platforms, the controls will be smaller and
        // closer together (more dense) than on mobile platforms.
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      // home: MyHomePage(title: 'Flutter Demo Home Page'),
      // home: WebViewExample(service: new Service("ABc title", "https://www.google.com")),
      home: TabView(),
      // CupertinoTabScaffold(
      //     tabBar: CupertinoTabBar(
      //       // backgroundColor: Colors.blue,
      //       items: _getBottomNavigationBarItem(myTabs),
      //     ),
      //     tabBuilder: (BuildContext context, int index) {
      //       print(index);

      //       return CupertinoTabView(
      //         builder: (BuildContext context) {
      //           const titles = [ "Page1", "Page2", "Page3"];
      //           const links = [ "http://10.6.88.98:3000", "http://10.6.88.98:3001", "https://www.youtube.com"];

      //           return CupertinoPageScaffold(
      //             // navigationBar: CupertinoNavigationBar(
      //             //   middle: Text('Page 1 of tab $index'),
      //             // ),
      //             child: Center(
      //               child: WebViewExample(service: new Service(titles[index], links[index])),
      //             ),
      //           );
      //         },
      //       );
      //     },
      //   )
    );
  }

  // List _getBottomNavigationBarItem(List<TabItem> tabItems){
  //   final barItemList = tabItems.map((TabItem item) {
  //     return  BottomNavigationBarItem(
  //       // backgroundColor:Colors.white,
  //         icon: item.icon,
  //         activeIcon: item.activeIcon,
  //         title: Text(
  //           item.title,
  //           style: TextStyle(
  //             fontSize: 10, // Default: 10
  //             fontWeight: FontWeight.w500,
  //             // color: Colors.white,
  //           ),
  //         ),
  //       );
  //   });

  //   return barItemList.toList();
  // }
}

class TabItem {
  final String title;
  final Icon icon;
  final Icon activeIcon;
  final String url;

  TabItem(this.title, this.icon, this.activeIcon, this.url);
}

class TabView extends StatefulWidget {
  @override
  _TabViewState createState() => _TabViewState();
}

class _TabViewState extends State<TabView> {
  int _counter = 3;
  List<TabItem> myTabs = [
    TabItem("Page 1", Icon(CupertinoIcons.home), Icon(CupertinoIcons.home),
        "https://10.6.88.98:3000"),
    TabItem(
        "Page 2",
        Icon(CupertinoIcons.conversation_bubble),
        Icon(CupertinoIcons.conversation_bubble),
        "https://10.6.88.98:3000/table_demo"),
    TabItem("Page 3", Icon(CupertinoIcons.group),
        Icon(CupertinoIcons.group_solid), "https://m.youtube.com"),
  ];

  void _addServiceBtnClick() {
    _counter++;
    myTabs.add(TabItem("Page " + _counter.toString(), Icon(CupertinoIcons.home),
        Icon(CupertinoIcons.home), "https://www.google.com"));
    setState(() {
      _counter = _counter;
      myTabs = myTabs;
    });
  }

  List _getBottomNavigationBarItem(List<TabItem> tabItems) {
    final barItemList = tabItems.map((TabItem item) {
      return BottomNavigationBarItem(
        // backgroundColor:Colors.white,
        icon: item.icon,
        activeIcon: item.activeIcon,
        title: Text(
          item.title,
          style: TextStyle(
            fontSize: 10, // Default: 10
            fontWeight: FontWeight.w500,
            // color: Colors.white,
          ),
        ),
      );
    });

    return barItemList.toList();
  }

  CupertinoTabView _getWebViewPage(List<TabItem> tabItems, int index) {
    return CupertinoTabView(
      // key: Key(index.toString()),
      builder: (BuildContext context) {
        return CupertinoPageScaffold(
            child: Center(
                // child: WebViewExample(service: new Service(tabItems[index].title, tabItems[index].url)),
                child: InAppWebViewExampleScreen(
                    service: new Service(
                        tabItems[index].title, tabItems[index].url, {}))));
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: CupertinoTabScaffold(
          tabBar: CupertinoTabBar(
            // backgroundColor: Colors.blue,
            items: _getBottomNavigationBarItem(myTabs),
          ),
          tabBuilder: (BuildContext context, int index) {
            print(index);
            return _getWebViewPage(myTabs, index);

            // return CupertinoTabView(
            //   // key: Key(index.toString()),
            //   builder: (BuildContext context) {
            //     const titles = [ "Page1", "Page2", "Page3"];
            //     const links = [ "http://10.6.88.98:3000", "http://10.6.88.98:3001", "https://www.youtube.com"];

            //     return CupertinoPageScaffold(
            //       // navigationBar: CupertinoNavigationBar(
            //       //   middle: Text('Page 1 of tab $index'),
            //       // ),
            //       child: Center(
            //         child: WebViewExample(service: new Service(titles[index], links[index])),
            //       ),
            //     );
            //   },
            // );
          },
        ),
        floatingActionButton: Padding(
          padding: const EdgeInsets.only(bottom: 70.0),
          child: FloatingActionButton(
            onPressed: _addServiceBtnClick,
            tooltip: 'Add service',
            child: Icon(Icons.add),
          ),
        ),
        // floatingActionButtonLocation:centerDocked ,
        floatingActionButtonLocation: FloatingActionButtonLocation.endFloat);
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  // This widget is the home page of your application. It is stateful, meaning
  // that it has a State object (defined below) that contains fields that affect
  // how it looks.

  // This class is the configuration for the state. It holds the values (in this
  // case the title) provided by the parent (in this case the App widget) and
  // used by the build method of the State. Fields in a Widget subclass are
  // always marked "final".

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  int _counter = 0;

  void _incrementCounter() {
    setState(() {
      // This call to setState tells the Flutter framework that something has
      // changed in this State, which causes it to rerun the build method below
      // so that the display can reflect the updated values. If we changed
      // _counter without calling setState(), then the build method would not be
      // called again, and so nothing would appear to happen.
      _counter++;
    });
  }

  @override
  Widget build(BuildContext context) {
    // This method is rerun every time setState is called, for instance as done
    // by the _incrementCounter method above.
    //
    // The Flutter framework has been optimized to make rerunning build methods
    // fast, so that you can just rebuild anything that needs updating rather
    // than having to individually change instances of widgets.
    return Scaffold(
      appBar: AppBar(
        // Here we take the value from the MyHomePage object that was created by
        // the App.build method, and use it to set our appbar title.
        title: Text(widget.title),
      ),
      body: Center(
        // Center is a layout widget. It takes a single child and positions it
        // in the middle of the parent.
        child: Column(
          // Column is also a layout widget. It takes a list of children and
          // arranges them vertically. By default, it sizes itself to fit its
          // children horizontally, and tries to be as tall as its parent.
          //
          // Invoke "debug painting" (press "p" in the console, choose the
          // "Toggle Debug Paint" action from the Flutter Inspector in Android
          // Studio, or the "Toggle Debug Paint" command in Visual Studio Code)
          // to see the wireframe for each widget.
          //
          // Column has various properties to control how it sizes itself and
          // how it positions its children. Here we use mainAxisAlignment to
          // center the children vertically; the main axis here is the vertical
          // axis because Columns are vertical (the cross axis would be
          // horizontal).
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text(
              'You have pushed the button this many times:',
            ),
            Text(
              '$_counter',
              style: Theme.of(context).textTheme.headline4,
            ),
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: _incrementCounter,
        tooltip: 'Increment',
        child: Icon(Icons.add),
      ), // This trailing comma makes auto-formatting nicer for build methods.
    );
  }
}
