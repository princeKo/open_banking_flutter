import 'dart:io';
import 'package:flutter/material.dart';
import 'package:flutter_inappwebview/flutter_inappwebview.dart';
import 'package:url_launcher/url_launcher.dart';

// import '../ios/Runner/main.dart';
class Service {
  final String title;
  final String link;
  final Map<String, dynamic> cookiesToBeSet;

  Service(this.title, this.link, this.cookiesToBeSet);
}

// void main() => runApp(MaterialApp(
//     home: InAppWebViewExampleScreen(
//         service: new Service(
//             "Bank Association",
//             // url,
//             "http://10.6.88.98:3001/test",
//             // "http://10.6.126.139:8080/openid-connect-server-webapp/authorize?response_type=code&client_id=e53cf3c1-a082-4075-be12-9b9b9c5e57c0&redirect_uri=https://10.6.88.55:8084/auth&scope=openid%20profile%20email%20address%20phone",
//             // "http://oauth2:8080/openid-connect-server-webapp/authorize?response_type=code&client_id=e53cf3c1-a082-4075-be12-9b9b9c5e57c0&redirect_uri=https://openbankportal:8084/auth&scope=openid%20profile%20email%20address%20phone",
//             {}))));

class InAppWebViewExampleScreen extends StatefulWidget {
  final Service service;

  const InAppWebViewExampleScreen({Key key, this.service}) : super(key: key);

  @override
  _InAppWebViewExampleScreenState createState() =>
      new _InAppWebViewExampleScreenState(service: this.service);
}

class _InAppWebViewExampleScreenState extends State<InAppWebViewExampleScreen> {
  final Service service;
  _InAppWebViewExampleScreenState({Key key, this.service}) : super();

  InAppWebViewController webView;
  ContextMenu contextMenu;
  String url = "";
  double progress = 0;
  CookieManager _cookieManager = CookieManager.instance();

  @override
  void initState() {
    super.initState();

    contextMenu = ContextMenu(
        menuItems: [
          ContextMenuItem(
              androidId: 1,
              iosId: "1",
              title: "Special",
              action: () async {
                print("Menu item Special clicked!");
                print(await webView.getSelectedText());
                await webView.clearFocus();
              })
        ],
        options: ContextMenuOptions(hideDefaultSystemContextMenuItems: true),
        onCreateContextMenu: (hitTestResult) async {
          print("onCreateContextMenu");
          print(hitTestResult.extra);
          print(await webView.getSelectedText());
        },
        onHideContextMenu: () {
          print("onHideContextMenu");
        },
        onContextMenuActionItemClicked: (contextMenuItemClicked) async {
          var id = (Platform.isAndroid)
              ? contextMenuItemClicked.androidId
              : contextMenuItemClicked.iosId;
          print("onContextMenuActionItemClicked: " +
              id.toString() +
              " " +
              contextMenuItemClicked.title);
        });
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.transparent,
        // appBar: AppBar(
        //     // elevation: 0,
        //     backgroundColor: Colors.pink,
        //     // backgroundColor: Color(0x44000000),
        //     // title: Text("InAppWebView"),
        //     title: Text(this.service.title),
        //     actions: <Widget>[
        //       ButtonBar(
        //         alignment: MainAxisAlignment.center,
        //         children: <Widget>[
        //           // IconButton(
        //           //   icon: Icon(Icons.arrow_back),
        //           //   onPressed: () {
        //           //     if (webView != null) {
        //           //       webView.goBack();
        //           //     }
        //           //   },
        //           // ),
        //           // IconButton(
        //           //   icon: Icon(Icons.arrow_forward),
        //           //   onPressed: () {
        //           //     if (webView != null) {
        //           //       webView.goForward();
        //           //     }
        //           //   },
        //           // ),
        //           IconButton(
        //             // color: Colors.transparent,
        //             icon: Icon(Icons.refresh),
        //             onPressed: () {
        //               if (webView != null) {
        //                 webView.reload();
        //               }
        //             },
        //           ),
        //           IconButton(
        //             icon: Icon(Icons.info_outline),
        //             onPressed: () {
        //               showcookie();
        //             },
        //           ),
        //         ],
        //       ),
        //     ]),
        // drawer: myDrawer(context: context),
        body: SafeArea(
            top: false,
            // bottom: false,
            child: Column(children: <Widget>[
              // Container(
              //   padding: EdgeInsets.all(20.0),
              //   child: Text(
              //       "CURRENT URL\n${(url.length > 50) ? url.substring(0, 50) + "..." : url}"),
              // ),
              // Container(
              //     // padding: EdgeInsets.all(10.0),
              //     height: 3,
              //     child: progress < 1.0
              //         ? LinearProgressIndicator(
              //             value: progress,
              //             valueColor:
              //                 new AlwaysStoppedAnimation<Color>(Colors.pink),
              //             backgroundColor: Colors.transparent,
              //           )
              //         : Container()),
              // Container(
              //     alignment: Alignment.centerLeft,
              //     margin: EdgeInsets.only(bottom: 20, left: 20),
              //     child: Text(
              //       this.service.title,
              //       textAlign: TextAlign.left,
              //       style: new TextStyle(
              //           fontWeight: FontWeight.w400,
              //           letterSpacing: 0.5,
              //           color: Colors.black,
              //           fontSize: 30.0),
              //     )),
              Expanded(
                child: Container(
                  // margin: const EdgeInsets.all(10.0),
                  // decoration: BoxDecoration(border: Border.all(color: Colors.blueAccent)),
                  child: InAppWebView(
                    contextMenu: contextMenu,
                    initialUrl: this.service.link,
                    // initialUrl: "https://10.6.88.98:3000",
                    // initialUrl: "https://kreatryx.com",
                    // initialFile: "assets/index.html",
                    initialHeaders: {},
                    initialOptions: InAppWebViewGroupOptions(
                      crossPlatform: InAppWebViewOptions(
                        debuggingEnabled: true,
                        useShouldOverrideUrlLoading: true,
                        // contentBlockers: ContentBlocker(trigger: null, action: null)
                      ),
                    ),
                    onLoadError: (controller, url, code, message) {
                      print(url);
                      print(message);
                    },
                    onJsAlert: (controller, jsAlertRequest) {
                      // return await jsAlertRequest();
                      print("jsAlertRequest.messag==========>");
                      print(jsAlertRequest.message);

                      if (jsAlertRequest.message.startsWith("Authorize")) {
                        // Navigator.of(context).pop(true);
                      }
                      // Navigator.of(context).pop(true);
                      // return  JsAlertResponse({message:});
                    },
                    onWebViewCreated:
                        (InAppWebViewController controller) async {
                      // var a = await controller.evaluateJavascript(
                      //     source: 'document.cookie');
                      // print(a);
                      // controller.evaluateJavascript(
                      //   source:
                      //       'document.cookie = "mytoken=this_is_token" + "; expires="',
                      // );
                      // _cookieManager.setCookie(
                      //   url: "http://10.6.88.98:3000",
                      //   name: "token",
                      //   value: "banka",
                      //   domain: "openbankportal",
                      // );
                      // _cookieManager.setCookie(
                      //     url: "https://openbankportal",
                      //     name: "myToken",
                      //     value: "123abbbbbddd");
                      // _cookieManager.setCookie(
                      //     url: "10.6.88.55", name: "bankName", value: "banka");
                      // _cookieManager.setCookie(
                      //     url: "openbankportal",
                      //     name: "bankUserName",
                      //     value: "zxg1");
                      print(this.service.cookiesToBeSet.keys);
                      this.service.cookiesToBeSet.keys.forEach((e) => {
                            print("Setting cookie for " +
                                this.service.cookiesToBeSet[e].toString()),
                            _cookieManager.setCookie(
                                url: this.service.cookiesToBeSet[e]["url"],
                                // domain: this.service.cookiesToBeSet[e]["url"],
                                name: e,
                                value: this.service.cookiesToBeSet[e]["value"])
                          });
                      webView = controller;
                      print("onWebViewCreated");
                    },
                    onLoadStart:
                        (InAppWebViewController controller, String url) {
                      print("onLoadStart $url");
                      if (url
                          .startsWith("https://openbankportal:8084/portal")) {
                        print("==========!! this url");
                        // Navigator.of(context).pop(true);
                      }
                      setState(() {
                        this.url = url;
                      });
                    },
                    shouldOverrideUrlLoading:
                        (controller, shouldOverrideUrlLoadingRequest) async {
                      var url = shouldOverrideUrlLoadingRequest.url;
                      var uri = Uri.parse(url);

                      if (![
                        "http",
                        "https",
                        "file",
                        "chrome",
                        "data",
                        "javascript",
                        "about"
                      ].contains(uri.scheme)) {
                        if (await canLaunch(url)) {
                          // Launch the App
                          await launch(
                            url,
                          );
                          // and cancel the request
                          return ShouldOverrideUrlLoadingAction.CANCEL;
                        }
                      }

                      return ShouldOverrideUrlLoadingAction.ALLOW;
                    },
                    onLoadStop:
                        (InAppWebViewController controller, String url) async {
                      print("onLoadStop $url");
                      setState(() {
                        this.url = url;
                      });
                    },
                    onProgressChanged:
                        (InAppWebViewController controller, int progress) {
                      setState(() {
                        this.progress = progress / 100;
                      });
                    },
                    onUpdateVisitedHistory: (InAppWebViewController controller,
                        String url, bool androidIsReload) {
                      print("onUpdateVisitedHistory $url");
                      setState(() {
                        this.url = url;
                      });
                    },
                    onReceivedServerTrustAuthRequest:
                        (InAppWebViewController controller,
                            ServerTrustChallenge challenge) async {
                      return ServerTrustAuthResponse(
                          action: ServerTrustAuthResponseAction.PROCEED);
                    },
                    onReceivedClientCertRequest:
                        (InAppWebViewController controller,
                            ClientCertChallenge challenge) async {
                      return ClientCertResponse(
                          action: ClientCertResponseAction.PROCEED);
                    },
                    onReceivedHttpAuthRequest:
                        (InAppWebViewController controller,
                            HttpAuthChallenge challenge) async {
                      return HttpAuthResponse(
                          action: HttpAuthResponseAction.PROCEED);
                    },
                  ),
                ),
              ),
              // ButtonBar(
              //   alignment: MainAxisAlignment.center,
              //   children: <Widget>[
              //     RaisedButton(
              //       child: Icon(Icons.arrow_back),
              //       onPressed: () {
              //         if (webView != null) {
              //           webView.goBack();
              //         }
              //       },
              //     ),
              //     RaisedButton(
              //       child: Icon(Icons.arrow_forward),
              //       onPressed: () {
              //         if (webView != null) {
              //           webView.goForward();
              //         }
              //       },
              //     ),
              //     RaisedButton(
              //       child: Icon(Icons.refresh),
              //       onPressed: () {
              //         if (webView != null) {
              //           webView.reload();
              //         }
              //       },
              //     ),
              //   ],
              // ),
            ])));
  }

  void showcookie() async {
    var a = await _cookieManager.getCookies(url: "http://openbankportal");
    print(a);
  }
}
