import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';

// import './webView.dart';
// import './webView2.dart';

import './home.dart';
import 'modulePages/modulePage.dart';
import 'bankAssociation/bankAssociationListPage.dart';
import 'openAccount/openAccount.dart';

void main() {
  runApp(NavBar());
}

class NavBar extends StatelessWidget {
  // This widget is the root of your application.
  // final myTabs = [Tab(key: Key("a"), text: "a"), Tab(key: Key("b"), text: "b"),];
  // final myTabs = [
  // TabItem("Page 1", Icon(CupertinoIcons.home), Icon(CupertinoIcons.home)),
  // TabItem("Page 2", Icon(CupertinoIcons.conversation_bubble), Icon(CupertinoIcons.conversation_bubble)),
  // TabItem("Page 3", Icon(CupertinoIcons.group), Icon(CupertinoIcons.group_solid)),
  // ];
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        // This is the theme of your application.
        //
        // Try running your application with "flutter run". You'll see the
        // application has a blue toolbar. Then, without quitting the app, try
        // changing the primarySwatch below to Colors.green and then invoke
        // "hot reload" (press "r" in the console where you ran "flutter run",
        // or simply save your changes to "hot reload" in a Flutter IDE).
        // Notice that the counter didn't reset back to zero; the application
        // is not restarted.
        primarySwatch: Colors.blue,
        // This makes the visual density adapt to the platform that you run
        // the app on. For desktop platforms, the controls will be smaller and
        // closer together (more dense) than on mobile platforms.
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      // home: MyHomePage(title: 'Flutter Demo Home Page'),
      // home: WebViewExample(service: new Service("ABc title", "https://www.google.com")),
      home: TabView(),
      // CupertinoTabScaffold(
      //     tabBar: CupertinoTabBar(
      //       // backgroundColor: Colors.blue,
      //       items: _getBottomNavigationBarItem(myTabs),
      //     ),
      //     tabBuilder: (BuildContext context, int index) {
      //       print(index);

      //       return CupertinoTabView(
      //         builder: (BuildContext context) {
      //           const titles = [ "Page1", "Page2", "Page3"];
      //           const links = [ "http://10.6.88.98:3000", "http://10.6.88.98:3001", "https://www.youtube.com"];

      //           return CupertinoPageScaffold(
      //             // navigationBar: CupertinoNavigationBar(
      //             //   middle: Text('Page 1 of tab $index'),
      //             // ),
      //             child: Center(
      //               child: WebViewExample(service: new Service(titles[index], links[index])),
      //             ),
      //           );
      //         },
      //       );
      //     },
      //   )
    );
  }

  // List _getBottomNavigationBarItem(List<TabItem> tabItems){
  //   final barItemList = tabItems.map((TabItem item) {
  //     return  BottomNavigationBarItem(
  //       // backgroundColor:Colors.white,
  //         icon: item.icon,
  //         activeIcon: item.activeIcon,
  //         title: Text(
  //           item.title,
  //           style: TextStyle(
  //             fontSize: 10, // Default: 10
  //             fontWeight: FontWeight.w500,
  //             // color: Colors.white,
  //           ),
  //         ),
  //       );
  //   });

  //   return barItemList.toList();
  // }
}

class TabItem {
  final String title;
  final Image icon;
  final Image activeIcon;
  final String url;

  TabItem(this.title, this.icon, this.activeIcon, this.url);
}

class TabView extends StatefulWidget {
  @override
  _TabViewState createState() => _TabViewState();
}

class _TabViewState extends State<TabView> {
  // static const _kFontFam = 'MyFlutterApp';
  // static const _kFontPkg = null;

  // static const IconData exchange = IconData(0xe800, fontPackage: _kFontPkg);

  int _counter = 4;
  List<TabItem> myTabs = [
    TabItem(
        "Home",
        // Icon(IconData(59530, fontFamily: 'MaterialIcons')),
        // Icon(CupertinoIcons.home
        // Icon(IconData(59530, fontFamily: 'MaterialIcons')),
        Image(width: 50, height: 50, image: AssetImage('images/home.png')),
        Image(width: 50, height: 50, image: AssetImage('images/home.png')),
        "https://10.6.88.98:3000"),
    TabItem(
        "Open account",
        // Icon(IconData(59504, fontFamily: 'MaterialIcons')),
        // Icon(FontAwesomeIcons.ccVisa),
        // Icon(IconData(59504, fontFamily: 'MaterialIcons')),
        Image(
            width: 50,
            height: 50,
            image: AssetImage('images/open_account.png')),
        Image(
            width: 50,
            height: 50,
            image: AssetImage('images/open_account.png')),
        "https://10.6.88.98:3000/table_demo"),
    TabItem(
      "Bank association",
      // Icon(FontAwesomeIcons.exchangeAlt),
      // Icon(FontAwesomeIcons.exchangeAlt),
      Image(width: 50, height: 50, image: AssetImage('images/bank.png')),
      Image(width: 50, height: 50, image: AssetImage('images/bank.png')),
      "https://10.6.88.98:3000",
    ),
    TabItem(
        "Module",
        // Icon(CupertinoIcons.person),
        // Icon(CupertinoIcons.person_solid),
        // Icon(FontAwesomeIcons.userCircle),
        // Icon(FontAwesomeIcons.userCircle),
        Image(width: 250, height: 300, image: AssetImage('images/module.png')),
        Image(width: 250, height: 300, image: AssetImage('images/module.png')),
        "https://m.youtube.com"),
  ];

  void _addServiceBtnClick() {
    _counter++;
    myTabs.add(TabItem(
      "Page " + _counter.toString(),
      // Icon(CupertinoIcons.home),
      // Icon(CupertinoIcons.home),
      Image(width: 50, height: 50, image: AssetImage('images/info.png')),
      Image(width: 50, height: 50, image: AssetImage('images/info.png')),

      "https://www.google.com",
    ));
    setState(() {
      _counter = _counter;
      myTabs = myTabs;
    });
  }

  List<Widget> myTabViews = [
    HomePage(),
    // Text("This is page2"),
    // Text("This is page3"),
    // Text("This is page4"),
    OpenAccountPage(),
    BankAssociationPage(),
    ModulePage()
  ];

  List _getBottomNavigationBarItem(List<TabItem> tabItems) {
    final barItemList = tabItems.map((TabItem item) {
      return BottomNavigationBarItem(
        // backgroundColor:Colors.white,
        icon: item.icon,
        activeIcon: item.activeIcon,
        title: Text(
          item.title,
          style: TextStyle(
            fontSize: 10, // Default: 10
            fontWeight: FontWeight.w500,
            // color: Colors.white,
          ),
        ),
      );
    });

    return barItemList.toList();
  }

  CupertinoTabView _getWebViewPage(List<TabItem> tabItems, int index) {
    return CupertinoTabView(
      // key: Key(index.toString()),
      builder: (BuildContext context) {
        return CupertinoPageScaffold(
            child: Center(
          // child: WebViewExample(service: new Service(tabItems[index].title, tabItems[index].url)),
          // child: Text("hello")
          child: myTabViews[index],
        ));
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        // To avoid resize when keyboad is turned on
        resizeToAvoidBottomPadding: false,
        resizeToAvoidBottomInset: false,
        body: CupertinoTabScaffold(
          tabBar: CupertinoTabBar(
            // backgroundColor: Colors.blue,
            inactiveColor: Colors.black54,
            activeColor: Color(0xffff3783),
            items: _getBottomNavigationBarItem(myTabs),
          ),
          tabBuilder: (BuildContext context, int index) {
            print(index);
            return _getWebViewPage(myTabs, index);
            // return myTabViews[index];

            // return CupertinoTabView(
            //   // key: Key(index.toString()),
            //   builder: (BuildContext context) {
            //     const titles = [ "Page1", "Page2", "Page3"];
            //     const links = [ "http://10.6.88.98:3000", "http://10.6.88.98:3001", "https://www.youtube.com"];

            //     return CupertinoPageScaffold(
            //       // navigationBar: CupertinoNavigationBar(
            //       //   middle: Text('Page 1 of tab $index'),
            //       // ),
            //       child: Center(
            //         child: WebViewExample(service: new Service(titles[index], links[index])),
            //       ),
            //     );
            //   },
            // );
          },
        ),
        floatingActionButton: Padding(
          padding: const EdgeInsets.only(bottom: 70.0),
          child: FloatingActionButton(
            shape: CircleBorder(
                side: BorderSide(color: Color(0xffe7e8f9), width: 2.0)),
            backgroundColor: Color(0xffff3783),
            onPressed: _addServiceBtnClick,
            tooltip: 'Add service',
            child: Icon(Icons.add),
          ),
        ),
        floatingActionButtonLocation:
            // FloatingActionButtonLocation.centerDocked);
            FloatingActionButtonLocation.centerDocked);
  }
}
