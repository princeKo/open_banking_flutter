import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';

import './profile/profile.dart';
import './home.dart';
import 'modulePages/modulePage.dart';
import 'bankAssociation/bankAssociationListPage.dart';
import 'openAccount/openAccount.dart';
import './moduleWebView.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'dart:io';
import 'dart:convert';

void main() {
  runApp(MaterialApp(
    theme: ThemeData(
      primarySwatch: Colors.pink,
    ),
    home: Scaffold(
        // appBar: AppBar(
        //   title: Text('Demo'),
        // ),
        body: DrawerWidget()),
  ));
}

class DrawerWidget extends StatefulWidget {
  @override
  _DrawerWidgetState createState() => _DrawerWidgetState();
}

class _DrawerWidgetState extends State<DrawerWidget> {
  int index = 0;
  TabController _tabController;

  final storage = FlutterSecureStorage();
  String token;

  List initialPages = [];

  final initialTabs = [
    {
      "title": "Home",
      "icon": 'images/home.png',
    },
    {
      "title": "Open user account",
      "icon": 'images/open_account.png',
    },
    {
      "title": "Bank association",
      "icon": 'images/bank.png',
    },
    {
      "title": "Module",
      "icon": 'images/modules.png',
    },
  ];

  Map<String, dynamic> myModules = {};
  List myModulesName = [];

  @override
  void initState() {
    super.initState();
    // _tabController = TabController(length: 4, vsync: this);
    _getMyModules();
    initialPages = [
      // HomePage(myModules: this.myModules),
      OpenAccountPage(),
      BankAssociationPage(),
      ModulePage(
        toUpdateDrawer: updateDrawer,
      )
    ];

    _getToken();
    // .then((value) => {
    //       pages = [
    //         HomePage(),
    //         OpenAccountPage(),
    //         BankAssociationPage(),
    //         ModulePage(),
    //         InAppWebViewExampleScreen(
    //           service: new Service(
    //             "Test1",
    //             // url,
    //             "http://10.6.88.98:3001",
    //             {
    //               "A_Token": {
    //                 // "url": "https://openbankportal",
    //                 "url": "http://10.6.88.98:3001",
    //                 "name": "A_Token",
    //                 "value": value
    //               },
    //             },
    //           ),
    //         )
    //       ]
    //     });

    // accountItems: this.allMyBanks[this.selectedBank],
  }

  void updateDrawer() {
    setState(() {
      this.myModules = {};
    });
    _getMyModules();
    print("updateDrawer is running in drawer.dart ");
  }

  Future<String> _getToken() async {
    String token = await storage.read(key: "token");
    setState(() {
      this.token = token;
    });
    return token;
  }

  @override
  Widget build(BuildContext context) {
    print("Building!!");
    print(this.myModules);
    List<Widget> allPages = [];
    allPages = [...initialPages];
    // HomePage needs to re-render when mymodule is chagned
    allPages.insert(0, HomePage(myModules: this.myModules));
    List allTabs = [...initialTabs];

    if (this.myModules != null) {
      print("Hello");
      // allPages.removeAt(0);
      // allPages.insert(0, HomePage(myModules: myModules));

      this.myModules.keys.forEach((moduleId) {
        allTabs.add({
          "title": moduleId,
          "icon": "images/Test1.png",
        });

        var settings = myModules[moduleId]["settings"];
        print(settings);
        var paramString = "";
        if (settings != null && settings.keys.length > 0) {
          for (var i = 0; i < settings.keys.length; i++) {
            var symbol;
            if (i == 0) {
              symbol = "?";
            } else {
              symbol = "&";
            }
            paramString = paramString +
                symbol +
                settings.keys.toList()[i] +
                "=" +
                settings[settings.keys.toList()[i]]["enabled"].toString();
          }
        }

        // print("http://10.6.88.98:3001" + paramString);

        allPages.add(InAppWebViewExampleScreen(
          service: new Service(
            // "Test1",
            moduleId,
            this.myModules[moduleId]["url"] + paramString,
            // "https://10.6.88.55:8080" + paramString,
            // "http://10.6.88.98:3001" + paramString,
            {
              "A_Token": {
                "url": this.myModules[moduleId]["url"],
                // "url": "https://10.6.88.55:8080",
                // "url": "http://10.6.88.98:3001",
                "name": "A_Token",
                "value": this.token
              },
            },
          ),
        ));
      });
    }

    return Scaffold(
        resizeToAvoidBottomPadding: false,
        // body: _buildPage(context, index),
        drawer: _buildDrawer(context, allTabs),
        body: Column(children: <Widget>[
          _buildAppBar(),
          // pages != null
          //     ? Expanded(
          //         child: IndexedStack(
          //         index: index,
          //         children: pages,
          //       ))
          //     : Container()
          Expanded(
              child: IndexedStack(
            index: index,
            children: allPages,
          ))
        ]));
  }

  Future<void> _getMyModules() async {
    String token = await storage.read(key: "token");
    // print(token);

    // setState(() {
    //   myModules = {
    //     "test1": {
    //       "settings": {
    //         "dummy": {"enabled": false},
    //         "moneyTransfer": {"enabled": true},
    //         "recommendCreditCard": {"enabled": true},
    //         "productEnquiry": {"enabled": false},
    //         "creditScoringEngine": {"enabled": true}
    //       },
    //       "url": "https://openbanktest1:8080",
    //       "enabled": true
    //     }
    //   };
    // });

    String email = await storage.read(key: "token_sub");
    var url = "https://10.6.88.55:8084/api/user/account/list";

    // var apiUrl = 'api/user/account/list';

    // var token = accessCookie("A_Token");
    // let email = getUserID(token);

    // var requestBody = {email: {}};
    var requestBody = {};
    HttpClient httpClient = new HttpClient()
      ..badCertificateCallback =
          ((X509Certificate cert, String host, int port) => true);
    HttpClientRequest request = await httpClient.postUrl(Uri.parse(url));
    request.headers.set('Authorization', token);
    request.headers.set(HttpHeaders.contentTypeHeader, "application/json");
    request.write(jsonEncode(requestBody));
    HttpClientResponse response = await request.close();
    String reply = await response.transform(utf8.decoder).join();
    var body = json.decode(reply);
    print("res - _getMyModules api/user/account/list");
    print(body);
    var modules = body[email]["modules"];
    print(modules);
    // var moduleNames;
    // if (modules == null) {
    //   moduleNames = [];
    // } else {
    //   modules.keys.forEach((e) {
    //     modules[e]["moduleId"] = e;
    //   });
    //   moduleNames = modules.keys.toList();
    // }
    // print(modules);
    // print(moduleNames);

    setState(() {
      // myModulesName = moduleNames;
      this.myModules = modules;
    });
  }

  Widget _buildAppBar() {
    return Container(
      height: 80,
      // color: Colors.green,
      margin: EdgeInsets.only(top: 40, left: 10, right: 10),
      // padding: EdgeInsets.only(bottom: 0),
      child: Builder(
          builder: (context) => Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  IconButton(
                    iconSize: 40,
                    icon: Icon(Icons.menu),
                    onPressed: () => Scaffold.of(context).openDrawer(),
                  ),
                  index == 0
                      ? IconButton(
                          iconSize: 60,
                          icon: Image(image: AssetImage('images/profile.png')),
                          onPressed: () {
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => ProfilePage()));
                          },
                        )
                      : Container()
                ],
              )),
    );
  }

  Widget _buildPage(BuildContext context, int index) {
    List<Widget> pages = [
      HomePage(),
      OpenAccountPage(),
      BankAssociationPage(),
      ModulePage(),
    ];

    return Builder(
        builder: (context) => SafeArea(
                child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      IconButton(
                        iconSize: 40,
                        icon: Icon(Icons.menu),
                        onPressed: () => Scaffold.of(context).openDrawer(),
                      ),
                      index == 0
                          ? IconButton(
                              iconSize: 60,
                              icon: Image(
                                  image: AssetImage('images/profile.png')),
                              onPressed: () {
                                Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                        builder: (context) => ProfilePage()));
                              },
                            )
                          : Container()
                    ],
                  ),
                  Expanded(child: pages[index]),
                ])));
  }

  Widget _buildDrawer(BuildContext context, List tabs) {
    List<Widget> list = [];
    for (var i = 0; i < tabs.length; i++) {
      list.add(Container(
          height: 70,
          child: ListTile(
            title: Text(tabs[i]["title"]),
            leading: Image(
                width: 35, height: 35, image: AssetImage(tabs[i]["icon"])),
            onTap: () {
              // _getMyModules();
              print("hi");
              setState(() {
                index = i;
              });
              Navigator.pop(context);
            },
          )));
    }

    return Drawer(
        child: SafeArea(
            child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
          Container(
            padding: const EdgeInsets.only(left: 20, top: 20, bottom: 30),
            child: IconButton(
              iconSize: 30,
              icon: Icon(Icons.close, color: Colors.black),
              onPressed: () => Navigator.pop(context),
            ),
          ),
          Expanded(
              child: ListView(
            padding: const EdgeInsets.only(left: 20),
            children: list,
            // children: <Widget>[
            //   ListTile(
            //     title: Text("Home"),
            //     leading: Image(
            //         width: 40,
            //         height: 40,
            //         image: AssetImage('images/home.png')),
            //     onTap: () {
            //       print("hi");
            //       setState(() {
            //         index = 0;
            //       });
            //       Navigator.pop(context);
            //     },
            //   ),
            //   ListTile(
            //     title: Text("Open account"),
            //     leading: Image(
            //         width: 40,
            //         height: 40,
            //         image: AssetImage('images/open_account.png')),
            //     onTap: () {
            //       print("hi");
            //       setState(() {
            //         index = 1;
            //       });
            //       Navigator.pop(context);
            //     },
            //   ),
            //   ListTile(
            //     title: Text("Bank association"),
            //     leading: Image(
            //         width: 40,
            //         height: 40,
            //         image: AssetImage('images/bank.png')),
            //     onTap: () {
            //       print("hi");
            //       setState(() {
            //         index = 2;
            //       });
            //       Navigator.pop(context);
            //     },
            //   ),
            //   ListTile(
            //     title: Text("Module"),
            //     leading: Image(
            //         width: 40,
            //         height: 40,
            //         image: AssetImage('images/module.png')),
            //     onTap: () {
            //       print("hi");
            //       setState(() {
            //         index = 3;
            //       });
            //       Navigator.pop(context);
            //     },
            //   ),
            //   ListTile(
            //     title: Text("Test1"),
            //     leading: Image(
            //         width: 40,
            //         height: 40,
            //         image: AssetImage('images/info.png')),
            //     onTap: () {
            //       print("hi");
            //       setState(() {
            //         index = 4;
            //       });
            //       Navigator.pop(context);
            //     },
            //   ),
            // ],
          )),
        ])));
  }
}
