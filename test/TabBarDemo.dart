import 'dart:typed_data';

import 'package:flutter/material.dart';
// import 'package:transparent_image/transparent_image.dart';

void main() => runApp(TabBarDemo());

class TabBarDemo extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    launchWebView();
    print("ABCDDDD");
    TestPrint.foo();

    final longText = Text(
        "EAEFA AAAAAAAAAAAAAA a A#@    dfssssssssssssssssssssssssssssssssssssssddddddddwswswswswswswswswswswswswswswswswswswswswswswswswswswswswswswswswswswswswswswswswswswswswswswswswswswswswswswswswswswswswswswswswswswswswswswsw        sddddddddddddddddddddddddweeeeeeeeeeeee wwwwwwwwwwwwwwwwwwwwww         22222222222223333333333333333333333333344444444444444444444444444444444444444444444444444444444 swswswswswswswswswswswswswswswswswswswswswswswswswswsssssssssssssssssssss    sfddd                fffffffffffffffffffffffffffffffffffffffffffffffffffff dfsssssssssssssssssssfdsfsfsffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff eeeeeee2222222222222222222222222222222222222222222222222222222222222222222 erwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwww weeeeeeeeeeeetgggggggggggggggggggggggggggggggggggggggggggggggggggggggggggg eeeeeeeeeeeeeeeeeeeeeeeerrrrrrrrrrrrrrrrrrrrrrrrgf aaaaaaaaaaAAAAAAAAAAAAAAAAAAAAA AAAAAAAAAAAAAAAAAAAAA AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA AAAAAAAAAAAAEFFFFFFFFFFFFFFFFFFFFFF FWAGGG ggggggggggggggggg       greewaTEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEE324让r arrrrrrrrarerawR   Arerawer 热waa  awrererwrrrrawerrr");
    final longText2 = new Expanded(
      flex: 1,
      child: Scrollbar(
        child: new SingleChildScrollView(
          child: new Text(
            "1 Description that is too long in text format(Here Data is coming from API) jdlksaf j klkjjflkdsjfkddfdfsdfds " +
                "2 Description that is too long in text format(Here Data is coming from API) d fsdfdsfsdfd dfdsfdsf sdfdsfsd d " +
                "3 Description that is too long in text format(Here Data is coming from API)  adfsfdsfdfsdfdsf   dsf dfd fds fs" +
                "4 Description that is too long in text format(Here Data is coming from API) dsaf dsafdfdfsd dfdsfsda fdas dsad" +
                "5 Description that is too long in text format(Here Data is coming from API) dsfdsfd fdsfds fds fdsf dsfds fds " +
                "6 Description that is too long in text format(Here Data is coming from API) asdfsdfdsf fsdf sdfsdfdsf sd dfdsf" +
                "7 Description that is too long in text format(Here Data is coming from API) df dsfdsfdsfdsfds df dsfds fds fsd" +
                "8 Description that is too long in text format(Here Data is coming from API)" +
                "8 Description that is too long in text format(Here Data is coming from API)" +
                "8 Description that is too long in text format(Here Data is coming from API)" +
                "9 Description that is too long in text format(Here Data is coming from API)" +
                "8 Description that is too long in text format(Here Data is coming from API)" +
                "8 Description that is too long in text format(Here Data is coming from API)" +
                "8 Description that is too long in text format(Here Data is coming from API)" +
                "9 Description that is too long in text format(Here Data is coming from API)" +
                "8 Description that is too long in text format(Here Data is coming from API)" +
                "8 Description that is too long in text format(Here Data is coming from API)" +
                "8 Description that is too long in text format(Here Data is coming from API)" +
                "9 Description that is too long in text format(Here Data is coming from API)" +
                "10 Description that is too long in text format(Here Data is coming from API)",
            style: new TextStyle(
              fontSize: 16.0,
              color: Colors.black45,
            ),
          ),
        ),
      ),
    );
    return MaterialApp(
      home: DefaultTabController(
        length: 4,
        child: Scaffold(
          appBar: AppBar(
            bottom: TabBar(
              tabs: [
                Tab(
                  icon: Icon(Icons.directions_car),
                  text: "Taxi",
                  // child: Text("Hey hey I am a child"), // child cannot be used in combination with text
                ),
                Tab(
                  icon: Icon(Icons.directions_transit),
                  text: "Railway",
                ),
                Tab(
                  icon: Icon(Icons.directions_bike),
                  text: "Bike",
                ),
                Tab(icon: Icon(Icons.directions_walk), text: "Walk"),
              ],
            ),
            title: Text('This is a Tabs Demo'),
          ),
          body: TabBarView(
            children: [
              // Icon(Icons.directions_car),
              Scaffold(
                appBar: AppBar(
                  title: Center(child: Text("HIhihi")),
                  backgroundColor: Colors.blueGrey,
                ),
                body: Center(
                  key: Key("This is a key"),
                  // child:Icon(Icons.directions_car),
                  child: longText2,
                ),
                backgroundColor: Colors.purple[50],
              ),
              Icon(Icons.directions_transit),
              // Center(
              //   child: FadeInImage.assetNetwork(
              //     placeholder: 'https://picsum.photos/250?image=5',
              //     image: 'https://picsum.photos/250?image=9',
              //   ),
              // ),
              // Icon(Icons.directions_bike),

              Stack(
                children: <Widget>[
                  Center(child: CircularProgressIndicator()),
                  Center(
                    child: FadeInImage.memoryNetwork(
                      // placeholder: kTransparentImage,
                      placeholder: Uint8List(10),
                      image:
                          'https://github.com/flutter/plugins/raw/master/packages/video_player/video_player/doc/demo_ipod.gif?raw=true',
                    ),
                  ),
                ],
              ),

              // Image.network(
              //   'https://github.com/flutter/plugins/raw/master/packages/video_player/video_player/doc/demo_ipod.gif?raw=true',
              // ),
              // Icon(Icons.directions_walk),
              // Image.network(
              //   'https://picsum.photos/250?image=9',
              // )
              Stack(
                children: <Widget>[
                  Center(child: CircularProgressIndicator()),
                  Center(
                    child: FadeInImage.memoryNetwork(
                      // placeholder: kTransparentImage,
                      placeholder: Uint8List(10),
                      image: 'https://picsum.photos/250?image=5',
                    ),
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}

// Function launchWebView() {
//   print("Something like 1234");
//   return  ()=>{};
// }

void launchWebView() {
  print("Something like 1234");
}

class TestPrint {
  static void foo() {
    print("This is a static method");
  }
}
