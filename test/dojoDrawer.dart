import 'package:flutter/material.dart';

void main() {
  runApp(MaterialApp(
    home: Scaffold(
        // appBar: AppBar(
        //   title: Text('Demo'),
        // ),
        body: DrawerWidget()),
  ));
}

class DrawerWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Body(),
      drawer: MyDrawer(),
    );
  }
}

class Body extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(16.0),
      child: ListView(
        children: <Widget>[
          Title(color: Colors.amber, child: Text("Drawer titile")),
          IconButton(
            icon: Icon(Icons.menu),
            onPressed: () => Scaffold.of(context).openDrawer(),
            // child: Text("open drawer"),
          ),
          // RaisedButton(
          //   onPressed: () => Scaffold.of(context).openDrawer(),
          //   child: Text("open drawer"),
          // )
        ],
      ),
    );
  }
}

class MyDrawer extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: ListView(
        children: <Widget>[
          DrawerHeader(
            child: Text("Header!"),
            // margin: const EdgeInsets.all(16),
          ),
          ListTile(
            title: Text("Title1"),
          ),
          ListTile(title: Text("Title12")),
        ],
      ),
    );

    // ListView(
    //   // Important: Remove any padding from the ListView.
    //   padding: EdgeInsets.zero,
    //   children: <Widget>[
    //     DrawerHeader(
    //       child: Text('Drawer Header'),
    //       decoration: BoxDecoration(
    //         color: Colors.blue,
    //       ),
    //     ),
    //     ListTile(
    //       title: Text('Item 1'),
    //       onTap: () {
    //         // Update the state of the app
    //         // ...
    //         // Then close the drawer
    //         Navigator.pop(context);
    //       },
    //     ),
    //     ListTile(
    //       title: Text('Item 2'),
    //       onTap: () {
    //         // Update the state of the app
    //         // ...
    //         // Then close the drawer
    //         Navigator.pop(context);
    //       },
    //     ),
    //   ],
    // ),
  }
}
