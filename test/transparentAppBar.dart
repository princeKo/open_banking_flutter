import 'package:flutter/material.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: MyHomePage(title: 'Flutter Demo Home Page'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);
  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  @override
  Widget build(BuildContext context) {
    // return Container(
    //     decoration: BoxDecoration(
    //         image: DecorationImage(
    //       image: NetworkImage(
    //           'https://img.zcool.cn/community/0372d195ac1cd55a8012062e3b16810.jpg'),
    //       fit: BoxFit.cover,
    //     )),
    //     child: Scaffold(
    //         backgroundColor: Colors.transparent, //把scaffold的背景色改成透明
    //         appBar: AppBar(
    //           backgroundColor: Colors.transparent, //把appbar的背景色改成透明
    //           // elevation: 0,//appbar的阴影
    //           title: Text(widget.title),
    //         ),
    //         body:
    //             // Center(
    //             //   child: Text('Hello World'),
    //             // )
    //             ListView(
    //           children: <Widget>[
    //             Container(
    //               color: Colors.greenAccent,
    //               height: 300,
    //             ),
    //             Container(
    //               color: Colors.red,
    //               height: 300,
    //             ),
    //             Container(
    //               color: Colors.yellowAccent,
    //               height: 300,
    //             )
    //           ],
    //         )));
    // return Scaffold(
    //     backgroundColor: Colors.transparent,
    //     body: Stack(overflow: Overflow.visible, children: <Widget>[
    //       ListView(
    //         children: <Widget>[
    //           Container(
    //             color: Colors.greenAccent,
    //             height: 300,
    //           ),
    //         ],
    //       ),
    //       Container(
    //           height: 100,
    //           width: double.infinity,
    //           child: AppBar(
    //             title: Text('分享管理'),
    //             elevation: 0,
    //             backgroundColor: Colors.transparent,
    //           )),
    //     ]));
    return Stack(
      children: <Widget>[
        Container(
          //My container or any other widget
          color: Colors.blue,
          child: ListView(
            children: <Widget>[
              Container(
                margin: EdgeInsets.only(top: 50),
                color: Colors.greenAccent,
                height: 300,
              ),
              Container(
                color: Colors.yellowAccent,
                height: 400,
              ),
              Container(
                color: Colors.red,
                height: 300,
              ),
            ],
          ),
        ),
        new Positioned(
          //Place it at the top, and not use the entire screen
          top: 0.0,
          left: 0.0,
          right: 0.0,
          child: AppBar(
            title: Text('Hello world'),
            backgroundColor: Colors.transparent, //No more green
            elevation: 0.0, //Shadow gone
          ),
        ),
      ],
    );

    // return new Scaffold(
    //   body: new Stack(
    //     children: <Widget>[
    //       new Container(
    //         color: Colors.blue.shade200,
    //       ),
    //       new Container(
    //         // top: 0.0,
    //         // left: 0.0,
    //         // bottom: 0.0,
    //         // right: 0.0,
    //         //here the body
    //         child: ListView(
    //           children: <Widget>[
    //             Container(
    //               height: 200,
    //               color: Colors.grey,
    //             ),
    //             Container(
    //               height: 300,
    //               color: Colors.red,
    //             ),
    //             Container(
    //               height: 600,
    //               color: Colors.yellow,
    //             )
    //           ],
    //         ),
    //       ),
    //       new Positioned(
    //         top: 0,
    //         left: 0,
    //         right: 0,
    //         child: AppBar(
    //           title: new Text("App bar"),
    //           backgroundColor: Colors.transparent,
    //           elevation: 0.0,
    //         ),
    //       )
    //     ],
    //   ),
    // );
  }
}
