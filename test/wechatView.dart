import 'package:flutter/material.dart';

void main() {
  runApp(MaterialApp(
    home: Scaffold(
        // appBar: AppBar(
        //   title: Text('Demo'),
        // ),
        body: App()),
  ));
}

class App extends StatefulWidget {
  @override
  MainState createState() => MainState();
}

class MainState extends State<App> {
  var _currentIndex = 0;

  _popupMenuItem(String title, {String imagePath, IconData icon}) {
    return PopupMenuItem(
      child: Row(
        children: <Widget>[
          imagePath != null
              ? Image.asset(
                  imagePath,
                  width: 32.0,
                  height: 32.0,
                )
              : SizedBox(
                  width: 32.0,
                  height: 32.0,
                  child: Icon(
                    icon,
                    color: Colors.white,
                  ),
                ),
          Container(
            padding: const EdgeInsets.only(left: 20),
            child: Text(
              title,
              style: TextStyle(color: Colors.black),
            ),
          ),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.black87,
        title: Text('微信'),
        actions: <Widget>[
          GestureDetector(
            onTap: () {
              // Navigator.pushNamed(context, 'search');
              Navigator.push(
                  context,
                  MaterialPageRoute(
                      // builder: (context) => LoginSuccessPage(
                      //     userToken: UserToken(userToken["sub"], userToken["role"],
                      //         userToken["exp"], userToken["iat"]))
                      builder: (context) => MaterialApp(
                              home: Scaffold(
                            appBar: AppBar(
                              title: Text('Search'),
                            ),
                            body: Text("Search"),
                          ))));
              // ));
            },
            child: Icon(
              Icons.search,
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(left: 20.0, right: 20.0),
            child: GestureDetector(
              onTap: () {
                showMenu(
                  context: context,
                  position: RelativeRect.fromLTRB(500.0, 86.0, 25.0, 0.0),
                  items: <PopupMenuEntry>[
                    _popupMenuItem(
                      '发起群聊',
                      // imagePath: 'images/2x/wechat@2x.png'
                    ),
                    _popupMenuItem('扫一扫')
                  ],
                );
              },
              child: Icon(Icons.add_a_photo),
            ),
          )
        ],
      ),
      bottomNavigationBar: new BottomNavigationBar(
        type: BottomNavigationBarType.fixed,
        currentIndex: _currentIndex,
        onTap: ((index) {
          setState(() {
            _currentIndex = index;
          });
        }),
        items: [
          new BottomNavigationBarItem(
              title: new Text(
                '聊天',
                style: TextStyle(
                    color: _currentIndex == 0
                        ? Color(0xFF03c40e)
                        : Color(0xff999999),
                    fontSize: 14.0),
              ),
              icon: _currentIndex == 0
                  // ? Image.asset(
                  //     'images/2x/messageSel@2x.png',
                  //     width: 32.0,
                  //     height: 28.0,
                  //   )
                  // : Image.asset(
                  //     'images/2x/messageNol@2x.png',
                  //     width: 32.0,
                  //     height: 28.0,
                  //   )),
                  ? Icon(
                      Icons.bubble_chart,
                      color: Color(0xFF03c40e),
                    )
                  : Icon(Icons.bubble_chart)),
          new BottomNavigationBarItem(
              title: new Text(
                '通讯录',
                style: TextStyle(
                    color: _currentIndex == 1
                        ? Color(0xFF03c40e)
                        : Color(0xff999999),
                    fontSize: 14.0),
              ),
              icon: _currentIndex == 1
                  // ? Image.asset(
                  //     'images/2x/messageSel@2x.png',
                  //     width: 32.0,
                  //     height: 28.0,
                  //   )
                  // : Image.asset(
                  //     'images/2x/messageNol@2x.png',
                  //     width: 32.0,
                  //     height: 28.0,
                  //   )),
                  ? Icon(
                      Icons.people,
                      color: Color(0xFF03c40e),
                    )
                  : Icon(Icons.people)),
          new BottomNavigationBarItem(
              title: new Text(
                '发现',
                style: TextStyle(
                    color: _currentIndex == 2
                        ? Color(0xFF03c40e)
                        : Color(0xff999999),
                    fontSize: 14.0),
              ),
              icon: _currentIndex == 2
                  // ? Image.asset(
                  //     'images/2x/messageSel@2x.png',
                  //     width: 32.0,
                  //     height: 28.0,
                  //   )
                  // : Image.asset(
                  //     'images/2x/messageNol@2x.png',
                  //     width: 32.0,
                  //     height: 28.0,
                  //   )),
                  ? Icon(
                      Icons.add_circle,
                      color: Color(0xFF03c40e),
                    )
                  : Icon(Icons.add_circle)),
          new BottomNavigationBarItem(
              title: new Text(
                '我的',
                style: TextStyle(
                    color: _currentIndex == 3
                        ? Color(0xFF03c40e)
                        : Color(0xff999999),
                    fontSize: 14.0),
              ),
              icon: _currentIndex == 3
                  // ? Image.asset(
                  //     'images/2x/messageSel@2x.png',
                  //     width: 32.0,
                  //     height: 28.0,
                  //   )
                  // : Image.asset(
                  //     'images/2x/messageNol@2x.png',
                  //     width: 32.0,
                  //     height: 28.0,
                  //   ))
                  ? Icon(
                      Icons.person,
                      color: Color(0xFF03c40e),
                    )
                  : Icon(Icons.person_outline)),
        ],
      ),
      body: currentPage(),
    );
  }

  Widget currentPage() {
    return Text("this is page " + (this._currentIndex + 1).toString());
  }
}
