import 'package:flutter/material.dart';

void main() {
  runApp(MaterialApp(
    home: Scaffold(
        // appBar: AppBar(
        //   title: Text('Demo'),
        // ),
        body: StatusDialog()),
  ));
}

class StatusDialog extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return StatusDialogState();
  }
}

class StatusDialogState extends State<StatusDialog> {
  String _selectedText = "SDD";

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      title: Text("Status Update"),
      content: new DropdownButton<String>(
        hint: Text("Status"),
        value: _selectedText,
        items: <String>['SDD', 'Meeting', 'Home', 'Space'].map((String value) {
          return new DropdownMenuItem<String>(
            value: value,
            child: new Text(value),
          );
        }).toList(),
        onChanged: (String val) {
          _selectedText = val;
          setState(() {
            _selectedText = val;
          });
        },
      ),
      actions: <Widget>[
        FlatButton(
          child: Text("UPDATE"),
          onPressed: () {
            //.....
          },
        ),
      ],
    );
  }
}

// void _buildStatusDialog(String documentID) {
//   showDialog<void>(
//       context: context,
//       builder: (BuildContext context) {
//         return StatusDialog();
//       });
// }
